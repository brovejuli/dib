<?php include("common/doctype-and-head.php"); ?>
<body>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->

<?php include("common/header.php"); ?>

<!-- bage header start -->
<div class="container ">
    <div class="page-header">
        <h1>Contenido actualizado en tu página web </h1>
        <ol class="breadcrumb">
            <li><a href="index.php">Inicio</a></li>
            <li><a href="#">Redes</a></li>
            <li class="active">Código embebido</li>

        </ol>
    </div>
</div>
<!-- bage header end -->
<!-- data start -->

<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-11 col-sm-11">
            <div class="row">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse-1"><span class="ion-code icon danger"></span> Código para poner en tu página web  </a></h4>
                        </div>
                        <div id="collapse-1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row" style="overflow-x: scroll;">
                                    <xmp class="iframe-text"><iframe src="<?=HTTP_WEBSITE?>embed_content.php" style="border:1px #b0b0b0 solid;" name="" scrolling="auto" align="bottom" height="200" width="100%"></iframe></xmp>
                                </div>
                                <br>
                                <button type="button" class="btn btn-primary pull-right copy-text">Copiar texto!</button>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse-4"><span class="ion-ios7-help icon danger"></span> ¿Cómo funciona? </a></h4>
                        </div>
                        <div id="collapse-4" class="panel-collapse collapse">
                            <div class="panel-body"> El código embebido en tu web te permite ver en tiempo real los principales títulos que publiquemos desde Agencia DIB. Cuando cambiamos el contenido, se refleja en tu sitio de forma automática.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse-5"><span class="ion-ios7-help icon danger"></span> Tengo el código ¿Cómo lo implemento en mi web?  </a> </h4>
                        </div>
                        <div id="collapse-5" class="panel-collapse collapse">
                            <div class="panel-body"> Copie el código que se encuentra en la parte superior y pegue el mismo en la sección de su página en que desee visualizar nuestras notas. Puede modificar el alto cambiando la variable height="200" </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- left sec End -->



        <div class="col-md-4 col-sm-5 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>

        </div>


    </div>
    <!-- left sec end -->
    <!-- redes -->


</div>
<!-- data end -->
<?php include("common/footer.php"); ?>

</body>
</html>