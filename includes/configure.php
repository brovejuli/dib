<?php

// Define the webserver and path parameters
// * DIR_FS_* = Filesystem directories (local/physical)
// * DIR_WS_* = Webserver directories (virtual/URL)
define('HTTP_SERVER', 'http://http://localhost/powerbyte/blog_manager/dib/'); //http://www.centromedicosion.com/
define('HTTP_WWW', 'http://http://localhost/powerbyte/blog_manager/dib/'); // eg, http://localhost - should not be empty for productive servers
define('HTTP_WEBSITE', 'http://http://localhost/powerbyte/blog_manager/dib/'); // eg, http://localhost - should not be empty for productive servers
define('HTTPS_SERVER', ''); // eg, https://localhost - should not be empty for productive servers
define('ENABLE_SSL', false); // secure webserver for checkout procedure?
define('HTTP_COOKIE_DOMAIN', '');
define('HTTPS_COOKIE_DOMAIN', '');
define('HTTPS_COOKIE_PATH', '');
define('DIR_WS_HTTPS_CATALOG', '');
define('DIR_WS_IMAGES', HTTP_SERVER . 'images/');
define('DIR_WS_IMAGES_WEBSITE', HTTP_WEBSITE . 'images/');
define('DIR_WS_IMAGES_GALERIA_THUMB', DIR_WS_IMAGES . 'blogmanagement/obras/thumb/');
define('DIR_WS_IMAGES_RSS', DIR_WS_IMAGES . 'gestion/blogmanagement/noticias/thumb/');
define('DIR_WS_CSS', HTTP_SERVER . 'css/');
define('DIR_WS_JS', HTTP_SERVER . 'js/');
define('DIR_WS_JS_PLUGINS', HTTP_SERVER . 'js/plugins/');
define('DIR_WS_BOWER_COMPONENTS', HTTP_SERVER . 'vendor/bower_components/');
define('DIR_WS_COMMON', 'common/');
define('DIR_WS_INCLUDES', 'includes/');
define('DIR_WS_CLASS', 'class/');
define('TITLE', 'Gestion blogs');

// define our database connection
define('DB_SERVER', 'localhost'); // eg, localhost - should not be empty for productive servers
define('DB_SERVER_USERNAME', 'root'); //
define('DB_SERVER_PASSWORD', ''); //
define('DB_DATABASE', 'power_dib'); //
define('USE_PCONNECT', 'false'); // use persistent connections?
define('STORE_SESSIONS', ''); // leave empty '' for default handler or set to 'mysql'
?>