<?php
require '../gestion/class/mailer/class.phpmailer.php';
require '../gestion/class/mailer/class.smtp.php';
include_once '../gestion/includes/configure.php';
include_once '../gestion/class/Configuration.class.php';
//include_once '../gestion/class/Email.class.php';
include_once '../gestion/class/DatabaseManager.class.php';

$dbManager = new DatabaseManager();
$objConfiguration = new Configuration();
$configuration = $objConfiguration->getConfigurationValues();

$mail_from = $configuration['Mail_From'];
$mail_pass = $configuration['Mail_Password'];
$mail_name = $configuration['Mail_From_Name'];

$mail = new PHPMailer;
if ($mail_from) {
    $mail->SMTPDebug = 0;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
//    $mail->Host = $configuration['MailHost'];  // Specify main and backup SMTP servers
    $mail->Host = $configuration['MailHost'];  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $mail_from;                 // SMTP username
    $mail->From = $mail_from;                 // SMTP username
    $mail->Password = $mail_pass;                          // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $mail->setFrom($mail_from, 'DIB:' . $_REQUEST['titulo']);
    $mail->addAddress($_REQUEST['email'], $_REQUEST['email']);     // Add a recipient
    $mail->addReplyTo('no-reply@powerbyte.com.ar', $_POST['name']);
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = utf8_decode($_REQUEST['titulo']);
    $message = '<a href="' . $_REQUEST['link'] . '">Ir a la nota</a>';
    $message .= $_REQUEST['nota'];
    $mail->Body = utf8_decode($message);
    $mail->AltBody = utf8_decode(strip_tags($message));

    if (!$mail->send()) {
        echo json_encode(['status' => false, 'msg' => 'Ocurrio un error al enviar el mensaje, por favor intente nuevamente mas tarde.']);
    } else {
        echo json_encode(['status' => true, 'msg' => 'Mensaje Enviado!']);
    }
} else
    echo json_encode(['status' => false, 'msg' => 'Ocurrio un error al enviar el mensaje, por favor intente nuevamente mas tarde.']);