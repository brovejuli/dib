<?php
error_reporting(0);
session_start();

include_once '../includes/configure.php';
include_once '../gestion/class/Login.class.php';
include_once '../gestion/class/Configuration.class.php';

$configuration = new Configuration();
$configuration = $configuration->getConfigurationValues();
$obj = new Login($_REQUEST['user'], $_REQUEST['password'], '');
$obj->remove_timed_out_session_id();
if ($abonado = $obj->checkUserAbonado()) {
//    if ($obj->check_session_count($abonado['Id']) >= $configuration['Session_Users'])
//        echo json_encode(['status' => false, 'msg' => 'El usuario ya se encuentra logeado en otro dispositivo.']);
//    else {
        $_SESSION['abonado'] = $abonado;
        $obj->set_session_id($abonado['Id'], session_id());
        echo json_encode(['status' => true, 'msg' => 'Sesion iniciada.']);
//    }
} else
    echo json_encode(['status' => false, 'msg' => 'Usuario o contraseña incorrectos.']);


?>