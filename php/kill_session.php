<?php
error_reporting(0);

session_start();

include_once '../includes/configure.php';
include_once '../gestion/class/Login.class.php';

$obj = new Login('', '', '');

$obj->remove_session_id(session_id());
session_regenerate_id();
session_unset();
session_destroy();
echo json_encode(['status' => true]);
?>