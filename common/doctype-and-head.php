<?php
ini_set('date.timezone', 'America/Argentina/Buenos_Aires');
session_start();
include_once 'gestion/class/DatabaseManager.class.php';
include_once 'gestion/class/Login.class.php';
include_once 'gestion/class/Configuration.class.php';
$dbManager = new DatabaseManager();
$configuration = new Configuration();
$configuration = $configuration->getConfigurationValues();
$login = new Login('', '', '');
$login->remove_timed_out_session_id();
//var_dump($_SESSION['abonado']);
if ($_SESSION['abonado'])
    if ($login->check_session_time(session_id()) == 'expired') {
        $login->remove_session_id(session_id());
        session_regenerate_id();
        session_unset();
        session_destroy();
    } else
        $login->set_session_expire_time(session_id())

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="600">
    <title>Diarios Bonaerenses</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- bootstrap styles-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- google font -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">     <!-- ionicons font -->
    <link href="https://fonts.googleapis.com/css?family=Neucha" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
    <!-- ionicons font -->
    <link href="css/ionicons.min.css" rel="stylesheet">
    <!-- animation styles -->
    <link rel="stylesheet" href="css/animate.css" />
    <!-- custom styles -->
    <link href="css/custom-blue.css" rel="stylesheet" id="style">
    <link href="css/custom-notes.css" rel="stylesheet" id="style">
    <!-- owl carousel styles-->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- magnific popup styles -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>