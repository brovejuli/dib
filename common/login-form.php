<div id="log-in" class="white-popup mfp-with-anim mfp-hide">
    <form role="form" id="login-form" method="POST">
        <h3>Inciar sesión</h3>
        <hr>
        <div class="form-group">
            <input type="text" name="user" id="user" class="form-control" placeholder="Usuario"
                   tabindex="3">
        </div>
        <div class="form-group">
            <input type="password" name="password" id="password" class="form-control " placeholder="Contraseña"
                   tabindex="4">
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-16">
                <input type="submit" value="Ingresar" class="btn btn-danger btn-block btn-lg bete" tabindex="7">
            </div>
        </div>
    </form>
    <div class="servicio">
        <p>Si aún no es usuario y desea acceder a este servicio,
            por favor, comuniquese con agencia DIB de Lunes a
            Viernes de 13:00 a 20:00 horas al <span>(0221) 422-0084</span>
            o por correo electrónico a <span>ventas@dib.com.ar </span></p>
    </div>
</div>
