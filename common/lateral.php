<div class="hora">
    <script type="text/javascript"> var d = new Date();
        document.write(d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear(), ', ' + d.getHours(), ':' + d.getMinutes()); </script>
</div>
<br>
<div class="bordered top-margin">
    <div class="row ">
        <div class="col-sm-16 bt-spac wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="150">
            <div class="table-responsive">
                <table class="table table-bordered social social2">
                    <tbody>
                    <tr>
                        <td><a class="twitter2" href="<?= $configuration['Url_Twitter'] ?>" target="_blank">
                                <p><span class="ion-social-twitter"></span>
                                </p>
                            </a></td>
                        <td><a class="facebook2" href="<?= $configuration['Url_Facebook'] ?>" target="_blank">
                                <p><span class="ion-social-facebook"></span>
                                </p>
                            </a></td>
                        <td><a class="youtube2" href="<?= $configuration['Url_Youtube'] ?>" target="_blank">
                                <p><span class="ion-social-youtube"></span>
                                </p>
                            </a></td>
                        <td><a class="rss2" href="rss.php" target="_blank">

                                <p><span class="ion-code"></span>
                                </p>
                            </a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- fin redes-->
        <div class="espacio"></div>
        <?php $publicidad = $dbManager->getPublicidad(2) ?>
        <?php if ($publicidad['archivo']): ?>
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50">
                <img class="img-responsive"
                     src="gestion/archivos/publicidades/<?= utf8_encode($publicidad['archivo']) ?>"
                     width="336" height="280" alt=""/>
            </div>
        <?php endif; ?>
        <!-- mapa-->
        <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"><a href=""> <img
                    class="img-responsive" src="images/general/mapa.gif" width="336" height="280" alt=""/> </a> <a
                href="#" class="sponsored"><span class="ion-ios7-play icon"></span>Ver video </a></div>
        <!-- fin mapa-->

        <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="130">
            <ul class="nav nav-tabs nav-justified " role="tablist">
                <li class="active"><a href="#popular" role="tab" data-toggle="tab">Fijas</a></li>
                <li><a href="#recent" role="tab" data-toggle="tab">Más visitadas</a></li>
            </ul>
            <!-- notas fijas -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="popular">
                    <ul class="list-unstyled">
                        <?php foreach ($dbManager->getNotasFijas() as $nota_fija): ?>
                            <?php $nota_fija_data = $dbManager->getNotaFija($nota_fija['nota'], strtolower($nota_fija['tipo'])) ?>
                            <?php if ($nota_fija_data): ?>
                                <li>
                                    <a href="<?= $dbManager->links[strtolower($nota_fija['tipo'])] . $nota_fija_data['Id'] ?>">
                                        <div class="row">
                                            <div class="col-sm-5 col-md-4">
                                                <img class="img-thumbnail pull-left"
                                                     src="<?= utf8_encode($nota_fija_data['imagen']) ?>" width="164"
                                                     height="152" alt=""/>
                                            </div>
                                            <div class="col-sm-11 col-md-12">
                                                <h5> <?= utf8_encode($nota_fija_data['titulo']) ?></h5>
                                                <div class="text-danger sub-info">
                                                    <div class="time"><span class="ion-android-data icon"></span>
                                                        <?= $nota_fija_data['fecha'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane" id="recent">
                    <ul class="list-unstyled">
                        <?php foreach ($dbManager->getMasVisitados() as $mas_visitado): ?>
                            <li>
                                <a href="noticia.php?noticia=<?= $mas_visitado['Id'] ?>">
                                    <div class="row">

                                        <div class="col-sm-5 col-md-4">
                                            <?php if ($portada = $dbManager->getNoticiaImagenes($mas_visitado['Id'])[0]['imagen']): ?>
                                                <img class="img-thumbnail pull-left"
                                                     src="gestion/images/blogmanagement/noticias/thumb/<?= $portada ?>"
                                                     width="164"
                                                     height="152" alt=""/>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-sm-11 col-md-12">
                                            <h5> <?= utf8_encode($mas_visitado['titulo']) ?></h5>
                                            <div class="text-danger sub-info">
                                                <div class="time">
                                                    <span class="ion-android-data icon"></span>
                                                    <?= $mas_visitado['hora'] ?>
                                                    -
                                                    <?= $mas_visitado['fecha'] ?>
                                                </div>
                                                <!--                                                <div class="pull-right"><a-->
                                                <!--                                                        href="noticia.php?noticia=-->
                                                <? //= $mas_visitado['Id'] ?><!--"> -->
                                                <? //= utf8_encode($mas_visitado['categoria']) ?><!-- </a>-->
                                                <!--                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <!-- fin notas fijas-->
        </div>
        <!-- radio start -->
        <?php $diario_extra = $dbManager->select(TABLE_DIARIO_EXTRA, 'Id', 'DESC', 1)[0] ?>
        <div class="col-md-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="100">
            <a href="gestion/archivos/diario_extra/<?= utf8_encode($diario_extra['archivo']) ?>" target="_blank"><img
                    src="gestion/images/blogmanagement/diario_extra/big/<?= utf8_encode($diario_extra['portada']) ?>"
                    style="width: 100%"></a>
            <a href="gestion/archivos/diario_extra/<?= utf8_encode($diario_extra['archivo']) ?>"
               download="Diario Extra">Descargar pdf</a>
        </div>
        <!-- radio end -->
        <?php if ($publicidad = $dbManager->getPublicidad(3)['archivo']): ?>
            <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"><img
                    class="img-responsive" src="gestion/archivos/publicidades/<?= utf8_encode($publicidad) ?>"
                    width="336" height="280" alt=""/></div>
        <?php endif; ?>

        <div class="col-md-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="130">
            <!-- Nav tabs -->

            <!-- flicker imgs start -->
            <div>
                <div class="main-title-outer pull-left">
                    <div class="main-title">Blog</div>
                </div>
                <ul class="list-unstyled">
                    <?php foreach ($dbManager->usuarios_blog() as $usuario): ?>
                        <?php $ultima_nota = $dbManager->getUltimaNotaUsuario($usuario['Id']) ?>
                        <li>
                            <a href="#">
                                <div class="row">
                                    <div class="col-sm-5  col-md-6 ">
                                        <?php if ($usuario['foto_perfil']): ?>
                                            <img class="img-thumbnail pull-left"
                                                 src="gestion/images/blogmanagement/perfil/<?= $usuario['foto_perfil'] ?>"
                                                 width="164"
                                                 height="152">
                                        <?php else: ?>
                                            <img class="img-thumbnail pull-left"
                                                 src="gestion/images/default-user.png"
                                                 width="164"
                                                 height="152">
                                        <?php endif; ?>
                                    </div>
                                    <div class="col-sm-11 col-md-10">
                                        <a href="nota-blog.php?nota=<?= $ultima_nota['Id'] ?>" target="_blank">
                                            <h6><?= utf8_encode($ultima_nota['titulo']) ?></h6>
                                            <a href="blog.php?user=<?= $usuario['Id'] ?>">
                                                <div class="time text-danger">
                                                    <span class="ion-compose icon"></span>
                                                    <?= utf8_encode($usuario['name']) ?>
                                                </div>
                                            </a>
                                        </a>
                                    </div>
                                </div>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- flicker imgs end -->
            <?php if ($publicidad = $dbManager->getPublicidad(4)['archivo']): ?>
                <div class="col-sm-16 bt-space wow fadeInUp animated" data-wow-delay="1s" data-wow-offset="50"><img
                        class="img-responsive" src="gestion/archivos/publicidades/<?= utf8_encode($publicidad) ?>"
                        width="336" height="280" alt=""/></div>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- right sec end -->
</div>