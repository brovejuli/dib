<!-- wrapper start -->
<div class="wrapper">


    <!-- sticky header start -->
    <div class="sticky-header">
        <!-- header start -->
        <div class="container header">
            <div class="row col-md-8 col-sm-8 col-xs-16">
                <div class="wow fadeInUpLeft animated"><a class="navbar-brand" href="index.php"></a></div>
            </div>
            <div class="span-outer col-md-8 col-sm-8 col-xs-16">
                <span class="pull-right text-danger last-update">
                    <p style="text-align: right"> Abonarse al servicio</p>
                    <span class="ion-ios7-telephone icon"></span><?= $configuration['Empresa_Telefono'] ?>
                </span>
            </div>

        </div>
        <!-- header end -->
        <!-- nav and search start -->

        <div class="nav-search-outer">
            <!-- nav start -->

            <nav class="navbar navbar-inverse" role="navigation">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-16">
                            <a href="javascript:;" class="toggle-search pull-right"><span
                                    class="ion-ios7-search"></span></a>
                            <?php if ($_SESSION['abonado']): ?>
                                <ul id="inline-popups" class="list-inline pull-right iniciar">
                                    <li><a class="open-popup-link" href="#log-in" data-effect="mfp-zoom-in"><span
                                                class="ion-person"></span> <?= utf8_encode($_SESSION['abonado']['user']) ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="open-popup-link kill-session"
                                           data-effect="mfp-zoom-in">
                                            Salir
                                        </a>
                                    </li>
                                </ul>
                            <?php else: ?>
                                <ul id="inline-popups" class="list-inline pull-right iniciar">
                                    <li><a class="open-popup-link" href="#log-in" data-effect="mfp-zoom-in"><span
                                                class="ion-person"></span> Iniciar sesión </a></li>
                                </ul>
                            <?php endif; ?>
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar-collapse"><span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                                        class="icon-bar"></span></button>
                            </div>
                            <div class="collapse navbar-collapse" id="navbar-collapse">
                                <ul class="nav navbar-nav  main-nav ">
                                    <li class="active text-uppercase"><a href="index.php">Inicio</a></li>
                                    <li><a href="grupo.php" class=" text-uppercase">Grupo DIB</a></li>
                                    <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle text-uppercase"
                                                            data-toggle="dropdown">Noticias<span
                                                class="ion-ios7-arrow-down nav-icn"></span></a>
                                        <ul class="dropdown-menu " role="menu">
                                            <?php foreach ($dbManager->categorias_noticias() as $categoria): ?>
                                                <li>
                                                    <a href="seccion_municipales.php?categoria=<?= $categoria['Id'] ?>">
                                                        <span class="ion-ios7-arrow-right nav-sub-icn"></span>
                                                        <?= utf8_encode($categoria['categoria']) ?>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle text-uppercase"
                                                            data-toggle="dropdown">Suplementos<span
                                                class="ion-ios7-arrow-down nav-icn"></span></a>
                                        <ul class="dropdown-menu " role="menu">
                                            <?php foreach ($dbManager->categorias_suplementos() as $categoria): ?>
                                                <li>
                                                    <a href="<?= $categoria['link'] ?>?categoria=<?= $categoria['Id'] ?>">
                                                        <span class="ion-ios7-arrow-right nav-sub-icn"></span>
                                                        <?= utf8_encode($categoria['concepto']) ?>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                            <li>
                                                <a href="diario-extra.php">
                                                    <span class="ion-ios7-arrow-right nav-sub-icn"></span>
                                                    Diario extra
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="fotografia.php" class=" text-uppercase">Fotografías</a></li>
                                    <li class="dropdown"><a href="javascript:void(0)" class="dropdown-toggle text-uppercase"
                                                            data-toggle="dropdown">Blog<span
                                                class="ion-ios7-arrow-down nav-icn"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <?php foreach ($dbManager->usuarios_blog() as $usuario): ?>
                                                <li><a href="blog.php?user=<?= $usuario['Id'] ?>"><span
                                                            class="ion-ios7-arrow-right nav-sub-icn"></span><?= utf8_encode($usuario['name']) ?>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                    <li><a href="diarios.php" class=" text-uppercase">Diarios</a></li>
                                    <li><a href="contacto.php" class=" text-uppercase">Contacto</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- nav end -->
                <!-- search start -->
                <div class="search-container ">
                    <div class="container">
                        <form action="resultados.php" method="POST" role="search">
                            <input id="search-bar" placeholder="Buscar.." autocomplete="off" name="busqueda" required>
                        </form>
                    </div>
                </div>
                <!-- search end -->
            </nav>
            <!--nav end-->
        </div>
        <!-- nav and search end-->
    </div>
    <!-- sticky header end -->
    <!-- top sec start -->
<?php include_once 'common/login-form.php' ?>