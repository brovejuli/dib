<!-- Footer start -->
<footer>
    <div class="top-sec">
        <div class="container ">
            <div class="row match-height-container">
                <div class="col-lg-6 subscribe-info wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">
                    <div class="row">
                        <div class="col-sm-16">
                            <div class="f-title">ABONO DE SERVICIO DE NOTICIAS</div>
                            <p>Solo para medios de comunicación, empresas, organismos oficiales, instituciones,
                                entidades intermedias y ONG.
                                Para abonarse al servicio de noticias debe llamar al (0221) 422-0084 </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 subscribe-info wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">

                    <div class="f-title">Obtener código para mi web</div>
                    <p> Obtené un código para incorporar en tu web. Los principales títulos del día serán cargados desde Agencia DIB y se actualizarán automáticamente en tu página web. <a href="rss.php" target="_blank" class="text-danger"> Obtener código > </a></p>

                </div>
                <div class="col-sm-4 recent-posts  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="40">
                    <div class="col-sm-16 f-social  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="10">
                        <ul class="list-inline">
                            <li><a href="<?= $configuration['Url_Twitter'] ?>"><span class="ion-social-twitter"></span></a>
                            </li>
                            <li><a href="<?= $configuration['Url_Facebook'] ?>"><span
                                        class="ion-social-facebook"></span></a></li>
                            <li><a href="<?= $configuration['Url_Youtube'] ?>"><span class="ion-social-youtube"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="btm-sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-16">
                    <div class="row">
                        <div class="col-lg-8 col-xs-16 copyrights text-left wow fadeInDown animated"
                             data-wow-delay="0.5s" data-wow-offset="10"><span>© 2017 Diarios Bonaerenses S.A. </span> -
                            Todos los derechos reservados
                        </div>

                        <div class="col-lg-8 col-xs-16 copyrights text-right wow fadeInDown animated"
                             data-wow-delay="0.5s" data-wow-offset="10">
                            <span>Diseño y desarrollo</span>
                            <a href="http://estudioumo.com.ar/" target="_blank"><img src="images/general/umo.png"></a>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->
<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!--login-->
<script src="js/login.js"></script>
<!--sharer-->
<script src="js/sharer.js"></script>
<script src="js/custom-notes.js"></script>
<script src="js/copy_text.js"></script>
<!--mailer-->
<script src="js/send_mail.js"></script>
<!--jQuery easing-->
<script src="js/jquery.easing.1.3.js"></script>
<!-- bootstrab js -->
<script src="js/bootstrap.js"></script>
<!--style switcher-->
<script src="js/style-switcher.js"></script> <!--wow animation-->
<script src="js/wow.min.js"></script>
<!-- time and date -->
<script src="js/moment.min.js"></script>
<!--news ticker-->
<script src="js/jquery.ticker.js"></script>
<!-- owl carousel -->
<script src="js/owl.carousel.js"></script>
<!-- magnific popup -->
<script src="js/jquery.magnific-popup.js"></script>
<!-- weather -->
<script src="js/jquery.simpleWeather.min.js"></script>
<!-- calendar-->
<script src="js/jquery.pickmeup.js"></script>
<!-- go to top -->
<script src="js/jquery.scrollUp.js"></script>
<!-- scroll bar -->
<script src="js/jquery.nicescroll.js"></script>
<script src="js/jquery.nicescroll.plus.js"></script>
<!--masonry-->
<script src="js/masonry.pkgd.js"></script>
<!--media queries to js-->
<script src="js/enquire.js"></script>
<!--custom functions-->
<script src="js/custom-fun.js"></script>
