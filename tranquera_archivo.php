<?php include_once 'common/doctype-and-head.php' ?>
<body>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->

<?php include("common/header.php"); ?>

<!-- bage header Start -->
<div class="container">
    <div class="page-header">
        <h1 class="cat-data7 tranquera">
            <span class="ion-cloud"></span>
            Archivo Suplemento Tranquera </h1>
        <ol class="breadcrumb">
            <li><a href="suplemento_tranquera.php">Tranquera</a></li>
            <li class="active">Archivo</li>
        </ol>
    </div>
</div>
<!-- bage header End -->
<!-- data start -->
<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12 col-sm-11">
            <div class="row">
                <!-- business start -->

                <div class="col-md-16 business  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="50">

                    <?php $i = 0; ?>
                    <?php foreach ($dbManager->getSuplementosArchivo($_REQUEST['categoria']) as $suplemento): ?>
                        <div class="col-md-4 col-sm-8 col-xs-8">
                            <a href="gestion/archivos/suplementos/<?= $suplemento['archivo'] ?>" target="_blank">
                                <img class="img-thumbnail img-responsive"
                                     src="gestion/images/blogmanagement/suplementos/big/<?= $suplemento['portada'] ?>"
                                     width="100%">
                                <div class="main-title-outer pull-left">
                                    <div class="main-title_tranquera">Edición Nº<?= $suplemento['edicion'] ?></div>
                                </div>
                                <div class="text-danger sub-info-bordered tranquera">
                                    <div class="time">
                                        <span class="ion-calendar icon"></span>
                                        Del
                                        <?= explode('-', $suplemento['fecha_desde'])[2] ?>
                                        al
                                        <?= explode('-', $suplemento['fecha_hasta'])[2] ?>
                                        de
                                        <?= $dbManager->meses[intval(explode('-', $suplemento['fecha_hasta'])[1]) - 1] ?>
                                        de
                                        <?= explode('-', $suplemento['fecha_hasta'])[0] ?>
                                    </div>
                                </div>
                        </div>
                        <?php $i++; ?>
                        <?php if ($i == 4): ?>
                            <div class="clearfix"></div>
                            <?php $i = 0; ?>
                        <?php endif ?>
                    <?php endforeach; ?>

                </div>
                <!-- business end -->
                <!-- Pagination Start -->
                <div class="col-sm-16">
                    <?php $page_break = 20 ?>
                    <?php $total = $dbManager->getTotalPagesEdiciones() ?>
                    <?php $offset = ceil($total / $page_break) ?>
                    <ul class="pagination">
                        <li class="<?= !isset($_REQUEST['page']) ? 'disabled' : '' ?>"><a
                                href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= ($_REQUEST['page'] - 1) ?>&categoria=<?=$_REQUEST['categoria']?>">&laquo;</a>
                        </li>
                        <?php for ($i = 1; $i <= $offset; $i++): ?>
                            <?php
                            if ($i == $_REQUEST['page'] || (!isset($_REQUEST['page']) && $i == 1))
                                $active = 'active';
                            else
                                $active = '';
                            ?>
                            <li class="<?= $active ?>"><a
                                    href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= $i ?>&categoria=<?=$_REQUEST['categoria']?>"><?= $i ?></a></li>
                        <?php endfor; ?>
                        <li class="<?= !isset($_REQUEST['page']) || $offset == $_REQUEST['page'] ? 'disabled' : '' ?>">
                            <a href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= ($_REQUEST['page'] + 1) ?>&categoria=<?=$_REQUEST['categoria']?>">&raquo;</a>
                        </li>
                    </ul>
                </div>
                <!-- Pagination End -->
            </div>
        </div>
        <!-- left sec end -->
        <!-- redes -->
        <div class="col-sm-5 col-md-4 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>

        </div>
    </div>
</div>
<!-- data end -->
<?php include("common/footer.php"); ?>

</div>
<!-- wrapper end -->
</body>
</html>