<?php include("common/doctype-and-head.php"); ?>
<?php $page_break = 4 ?>
<body>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->
<?php include("common/header.php"); ?>
<!-- data start -->
<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12 col-sm-11">
            </iframe>
            <div class="row">
                <!-- business start -->
                <div class="col-sm-16 business  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="50">

                    <div class="row">
                        <?php $noticia_principal = $dbManager->getNoticiasIndex(1, 0); ?>
                        <?php $noticia_lateral_superior = $dbManager->getNoticiasIndex(2, 0); ?>
                        <?php $noticia_lateral_inferior = $dbManager->getNoticiasIndex(3, 0); ?>
                        <div class="col-md-16 col-sm-16">
                            <div class="row">
                                <?php if ($noticia_principal): ?>
                                    <div class="col-md-11 col-sm-9 col-xs-16">
                                        <div class="topic">
                                            <?php if ($_SESSION['abonado']): ?>
                                            <a class="popup-img"
                                               href="gestion/images/blogmanagement/noticias/big/<?= $noticia_principal['imagen'] ?>">
                                                <div class="thumb-box"><span class="ion-arrow-expand"></span>
                                                    <?php endif; ?>
                                                    <img
                                                        class="img-thumbnail"
                                                        src="gestion/images/blogmanagement/noticias/thumb/<?= $noticia_principal['imagen'] ?>"
                                                        width="1600" height="972" alt=""/>
                                                    <?php if ($_SESSION['abonado']): ?>
                                                </div>
                                            </a>
                                        <?php endif; ?>
                                            <a href="noticia.php?noticia=<?= $noticia_principal['Id'] ?>">
                                                <h2><?= utf8_encode($noticia_principal['titulo']) ?> </h2>
                                            </a>
                                            <p><?= substr(strip_tags($noticia_principal['cuerpo']), 0, 400) ?></p>
                                            <a href="noticia.php?noticia=<?= $noticia_principal['Id'] ?>"
                                               class="text-danger"> Leer nota > </a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="col-md-5 col-sm-7 col-xs-16">
                                    <ul class="list-unstyled">
                                        <?php if ($noticia_lateral_superior): ?>
                                            <li>
                                                <div class="row">
                                                    <div class="topic col-sm-16">
                                                        <?php if ($_SESSION['abonado']): ?>
                                                        <a class="popup-img"
                                                           href="gestion/images/blogmanagement/noticias/big/<?= $noticia_lateral_superior['imagen'] ?>">
                                                            <div class="thumb-box"><span
                                                                    class="ion-arrow-expand"></span>
                                                                <?php endif; ?>
                                                                <img
                                                                    class="img-thumbnail"
                                                                    src="gestion/images/blogmanagement/noticias/big/<?= $noticia_lateral_superior['imagen'] ?>"
                                                                    width="1600"
                                                                    height="972" alt=""/>
                                                                <?php if ($_SESSION['abonado']): ?>
                                                            </div>
                                                        </a>
                                                    <?php endif; ?>
                                                        <a href="noticia.php?noticia=<?= $noticia_lateral_superior['Id'] ?>">
                                                            <h4><?= utf8_encode($noticia_lateral_superior['titulo']) ?></h4>
                                                        </a>
                                                        <a href="noticia.php?noticia=<?= $noticia_lateral_superior['Id'] ?>"
                                                           class="text-danger"> Leer nota > </a>

                                                    </div>
                                                </div>
                                            </li>
                                        <?php endif; ?>
                                        <?php if ($noticia_lateral_inferior): ?>
                                            <li>
                                                <div class="row">
                                                    <div class="topic col-sm-16">
                                                        <?php if ($_SESSION['abonado']): ?>
                                                        <a class="popup-img"
                                                           href="gestion/images/blogmanagement/noticias/big/<?= $noticia_lateral_inferior['imagen'] ?>">
                                                            <div class="thumb-box"><span
                                                                    class="ion-arrow-expand"></span>
                                                                <?php endif; ?>
                                                                <img
                                                                    class="img-thumbnail"
                                                                    src="gestion/images/blogmanagement/noticias/big/<?= $noticia_lateral_inferior['imagen'] ?>"
                                                                    width="1600"
                                                                    height="972" alt=""/>
                                                                <?php if ($_SESSION['abonado']): ?>
                                                            </div>
                                                        </a>
                                                    <?php endif; ?>
                                                        <a href="noticia.php?noticia=<?= $noticia_lateral_inferior['Id'] ?>">
                                                            <h4><?= utf8_encode($noticia_lateral_inferior['titulo']) ?></h4>
                                                        </a>
                                                        <a href="noticia.php?noticia=<?= $noticia_lateral_inferior['Id'] ?>"
                                                           class="text-danger"> Leer nota > </a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php $publicidad = $dbManager->getPublicidad(1) ?>
                        <?php if ($publicidad['archivo']): ?>
                            <div class="col-md-16">
                                <img class="img-thumbnail img-responsive"
                                     src="gestion/archivos/publicidades/<?= utf8_encode($publicidad['archivo']) ?>">

                            </div>
                        <?php endif; ?>
                    </div>
                    <br>
                    <div class="main-title-outer pull-left">
                        <div class="main-title">TODAS LAS NOTICIAS</div>
                    </div>
                    <?php foreach ($dbManager->getNoticias(null, $_REQUEST['page'] ? $_REQUEST['page'] : 1, $page_break) as $noticia): ?>
                        <div class="topic col-sm-16">
                            <div class="text-danger sub-info-bordered">
                                <div class="pull-right"><a href=""> <?= utf8_encode($noticia['categoria']) ?> </a></div>
                                <div class="time"><span class="ion-android-data icon"></span><?= $noticia['fecha'] ?>
                                </div>
                            </div>

                            <div class="col-md-15 col-sm-15 col-xs-15"><h5> <?= utf8_encode($noticia['titulo']) ?> </h5>
                            </div>
                            <?php if ($dbManager->getNoticiaImagenes($noticia['Id'])): ?>
                                <div class="camara col-md-1 col-sm-1 col-xs-1"><span class="ion-camera"></span></div>
                            <?php endif; ?>
                            <div class="col-md-16">
                                <p>
                                <p class="noticia-cuerpo"><?= (substr(trim(strip_tags($noticia['cuerpo'])), 0, 400)) ?></p>
                                <a href="noticia.php?noticia=<?= $noticia['Id'] ?> " class="text-danger">
                                    Leer nota >
                                </a>
                                </p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <!-- Pagination Start -->
                    <div class="col-sm-16">
                        <?php $total = $dbManager->getTotalPages(TABLE_NOTICIAS) ?>
                        <?php $offset = ceil($total / $page_break) ?>
                        <?php $dots_end = $dots_start = false ?>
                        <ul class="pagination" id="pagination" data-total="<?= $total ?>">
                            <li class="<?= !isset($_REQUEST['page']) ? 'disabled' : '' ?>"><a
                                    href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= ($_REQUEST['page'] - 1) ?>">&laquo;</a>
                            </li>
                            <?php for ($i = 1; $i <= $offset; $i++): ?>
                                <?php
                                if ($i == $_REQUEST['page'] || (!isset($_REQUEST['page']) && $i == 1))
                                    $active = 'active';
                                else
                                    $active = '';
                                ?>

                                <?php if ($i >= $_REQUEST['page'] && $i <= $_REQUEST['page'] + 9 && $_REQUEST['page'] + 9 <= $total): ?>
                                    <li class="<?= $active ?>"><a
                                            href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= $i ?>"><?= $i ?></a></li>
                                <?php else: ?>
                                    <?php if ($i <= 3): ?>
                                        <li class="<?= $active ?>"><a
                                                href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= $i ?>"><?= $i ?></a></li>
                                        <?php if ($i == 3): ?>
                                            <?php if (!$dots_start): ?>
                                                <li><a>...</a></li>
                                                <?php $dots_start = true ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if ($i >= ($offset - 3)): ?>
                                        <?php if (!$dots_end): ?>
                                            <li><a>...</a></li>
                                            <?php $dots_end = true ?>
                                        <?php endif; ?>
                                        <li class="<?= $active ?>"><a
                                                href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= $i ?>"><?= $i ?></a></li>
                                    <?php endif; ?>
                                <?php endif; ?>

                            <?php endfor; ?>
                            <li class="<?= !isset($_REQUEST['page']) || $offset == $_REQUEST['page'] ? 'disabled' : '' ?>">
                                <a href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= ($_REQUEST['page'] + 1) ?>">&raquo;</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Pagination End -->
                </div>
                <!-- business end -->


                <!-- lifestyle start-->
                <div class="col-sm-16 wow fadeInUp animated " data-wow-delay="0.5s" data-wow-offset="100">
                    <br>
                    <div class="main-title-outer pull-left">
                        <div class="main-title">Últimas fotos</div>
                    </div>

                    <div class="row">
                        <div id="owl-lifestyle" class="owl-carousel owl-theme lifestyle pull-left">
                            <?php foreach ($dbManager->getUltimasFotos(6) as $foto): ?>
                                <div class="item topic">
                                    <?php if ($_SESSION['abonado']): ?>
                                    <a class="popup-img"
                                       href="gestion/images/blogmanagement/noticias/big/<?= utf8_encode($foto['imagen']) ?>">
                                        <div class="thumb-box"><span class="ion-arrow-expand"></span>
                                            <?php endif; ?>
                                            <img
                                                class="img-thumbnail"
                                                src="gestion/images/blogmanagement/noticias/thumb/<?= utf8_encode($foto['imagen']) ?>"
                                                width="1600"
                                                height="972" alt=""/>
                                            <?php if ($_SESSION['abonado']): ?>
                                        </div>
                                    </a>
                                <?php endif; ?>
                                    <p><span
                                            class="ion-ios7-arrow-right icon"></span> <?= utf8_encode($foto['epigrafe']) ?>
                                    </p>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <!-- lifestyle end -->


                <!-- banner outer thumb start -->
                <div class="col-xs-16 banner-outer-thumb  pull-left  wow fadeInLeft animated" data-wow-delay="0.23s"
                     data-wow-offset="50">
                    <br>
                    <div class="main-title-outer pull-left">
                        <div class="main-title">Suplementos</div>
                    </div>
                    <div class="row">
                        <div id="banner-thumbs" class="owl-carousel">
                            <?php foreach ($dbManager->select(TABLE_CATEGORIAS_SUPLEMENTOS, 'concepto', 'ASC', 1) as $categoria): ?>
                                <?php $edicion = $dbManager->getSuplementosArchivo($categoria['Id'])[0] ?>
                                <?php if ($edicion): ?>
                                    <div class="item topic">
                                        <a href="<?= $categoria['link'] ?>?categoria=<?= $categoria['Id'] ?>"
                                           target="_blank">
                                            <div class="box">
                                                <div class="carousel-caption">
                                                    <p><?= substr(utf8_encode($categoria['resenia']), 0, 150) ?></p>
                                                </div>
                                                <img class="img-responsive"
                                                     src="gestion/images/blogmanagement/suplementos/big/<?= utf8_encode($edicion['portada']) ?>"
                                                     width="1600" height="972" alt=""/>
                                                <div class="overlay"></div>
                                            </div>
                                        </a>
                                        <div class="<?= $dbManager->suplementos_title_clases[$categoria['Id']] ?>">
                                            <h6 class="<?= $dbManager->suplementos_clases[$categoria['Id']] ?>">
                                                <span class="<?= utf8_encode($categoria['icon']) ?>"></span>
                                                <?= utf8_encode($categoria['concepto']) ?></h6>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <!-- banner outer thumb end -->

            </div>
            <?php if ($publicidad = $dbManager->getPublicidad(5)['archivo']): ?>
                <div class="col-md-8">
                    <img class="img-responsive" src="gestion/archivos/publicidades/<?= utf8_encode($publicidad) ?>">
                </div>
            <?php endif; ?>
            <?php if ($publicidad = $dbManager->getPublicidad(6)['archivo']): ?>
                <div class="col-md-8">
                    <img class="img-responsive" src="gestion/archivos/publicidades/<?= utf8_encode($publicidad) ?>">
                </div>
            <?php endif; ?>
        </div>
        <!-- left sec end -->
        <!-- redes -->
        <div class="col-sm-5 col-md-4 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>
        </div>
    </div>
    <!-- data end -->
    <?php include("common/footer.php"); ?>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                ...
            </div>
        </div>
    </div>

    <?php if ($publicidad = $dbManager->getPublicidad(7)['archivo']): ?>
        <!-- Modal -->
        <div class="modal fade" id="modal-publicidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
                    </div>
                    <div class="modal-body text-center">
                        <div class="row">
                            <img class="img-responsive"
                                 src="gestion/archivos/publicidades/<?= utf8_encode($publicidad) ?>"
                                 style="margin:0 auto;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('#modal-publicidad').modal('show');
        </script>
    <?php endif; ?>
</div>
</body>
</html>