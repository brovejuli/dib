<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Diarios Bonaerenses - Nota Blog</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- bootstrap styles-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- google font -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Delius" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">     <!-- ionicons font -->
    <link href="https://fonts.googleapis.com/css?family=Neucha" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">

    <!-- ionicons font -->
    <link href="css/ionicons.min.css" rel="stylesheet">
    <!-- animation styles -->
    <link rel="stylesheet" href="css/animate.css" />
    <!-- custom styles -->
    <link href="css/custom-blue.css" rel="stylesheet" id="style">
    <!-- owl carousel styles-->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- magnific popup styles -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->

<?php include("common/header.php"); ?>

<!-- bage header start -->
<div class="container ">
    <div class="page-header">
        <h1>En primera persona </h1>
        <ol class="breadcrumb">
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Blog</a></li>
        </ol>
    </div>
</div>
<!-- bage header end -->
<!-- data start -->

<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4 col-sm-3">
                        <div class="col-md-16"><img class="img-thumbnail" src="images/comments/com-1.jpg" width="128" height="128" alt=""/></div>
                        <div class="col-md-16 blog_perfil2">
                            <h4>Fernando <br>Delaiti</h4>
                            <ul class="list-inline">
                                <li><a href="#"><span class="ion-social-twitter"></span></a></li>
                                <li><a href="#"><span class="ion-social-facebook"></span></a></li>
                            </ul>
                            <p>Nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>
                            <br><br>
                        </div>

                    <div>


                        <div class="table-responsive col-md-15">
                            <br>
                            <h6 style="text-align: left">Compartí la nota</h6>
                            <table class="table table-bordered social social2">
                                <tbody>

                                <tr>
                                    <td><a class="twitter2" href="#">
                                            <p><span class="ion-social-twitter"></span>
                                            </p>
                                        </a></td>
                                    <td><a class="facebook2" href="#">
                                            <p><span class="ion-social-facebook"></span>
                                            </p>
                                        </a></td>
                                    <td><a class="correo" href="#">
                                            <p> <span class="ion-android-mail"></span>
                                            </p>
                                        </a></td>

                                </tbody>
                            </table>
                            <br>
                        </div>
                    </div>

                </div>
                <div class="col-md-12 col-sm-13 ">
                    <div class="row">
                        <div class="sec-topic col-sm-16  wow fadeInDown animated author-box2" data-wow-delay="0.5s">
                            <div class="row">
                                <div class="col-sm-16 sec-info">
                                    <div class="text-danger sub-info-bordered">
                                        <div class="pull-right"><span> Crisis en Venezuela </span></div>
                                        <div class="time"><span class="ion-android-data icon"></span>28/03/2017</div>
                                    </div>
                                    <h3>Nuevo ring en el que el chavismo y la oposición buscan dar el golpe de nocaut</h3>

                                    <p><span class="letter-badge">L</span>orem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.

                                        Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. <a href="#">Vivamus elementum semper</a> nisi. Aenean vulputate eleifend tellus. <br>
                                        Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.Etiam ultricies>nisi vel augue. Curabitur ullamcorper ultricies nisi.</p>
                                    <blockquote>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                    </blockquote>
                                    <p> Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.</p>
                                    <ul class="icn-list">
                                        <li>Lorem ipsum dolor sit amet</li>
                                        <li>Consectetur adipiscing elit</li>
                                        <li>Integer molestie lorem at massa</li>
                                        <li>Facilisis in pretium nisl aliquet</li>
                                        <li>Nulla volutpat aliquam velit</li>
                                    </ul>
                                    <div class="col-sm-16"> <img width="1000" height="606" alt="" src="images/sec/sec-3.jpg" class="img-thumbnail"> </div>

                                    <p>Aenean commodo ligula eget dolor. Aenean massa.
                                       Cum sociis
                                        natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.

                                        Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo</p>
                                    <hr>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>

                <div class="col-sm-16 related">
                    <div class="main-title-outer pull-left ">
                        <div class="main-title">Últimas notas</div>
                    </div>
                    <div class="row author-box">
                        <div class="item topic col-sm-5 col-xs-16"> <a href="#"> <img class="img-thumbnail" src="images/sec/sec-1.jpg" width="1000" height="606" alt=""/>
                                <div class="text-danger sub-info-bordered">
                                    <div class="pull-right"><span> Crisis en Venezuela </span></div>
                                    <div class="time"><span class="ion-android-data icon"></span>28/03/2017</div>
                                </div>
                                <h3>Etiam rhoncus. Maecenas tempus, tellus eget condimentum</h3>

                            </a> </div>
                        <div class="item topic col-sm-5 col-xs-16"> <a href="#"> <img class="img-thumbnail" src="images/sec/sec-2.jpg" width="1000" height="606" alt=""/>
                                <div class="text-danger sub-info-bordered">
                                    <div class="pull-right"><span> Crisis en Venezuela </span></div>
                                    <div class="time"><span class="ion-android-data icon"></span>28/03/2017</div>
                                </div>
                                <h3>Etiam rhoncus. Maecenas tempus, tellus eget condimentum</h3>

                            </a> </div>
                        <div class="item topic col-sm-5 col-xs-16"> <a href="#"> <img class="img-thumbnail" src="images/sec/sec-3.jpg" width="1000" height="606" alt=""/>
                                <div class="text-danger sub-info-bordered">
                                    <div class="pull-right"><span> Crisis en Venezuela </span></div>
                                    <div class="time"><span class="ion-android-data icon"></span>28/03/2017</div>
                                </div>
                                <h3>Etiam rhoncus. Maecenas tempus, tellus eget condimentum</h3>

                            </a> </div>
                    </div>
                </div>


            </div>


        </div>
        <div class="col-md-4 col-sm-5 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>

        </div>


    </div>
    <!-- left sec end -->
    <!-- redes -->


</div>
<!-- data end -->
<?php include("common/footer.php"); ?>


<div id="create-account" class="white-popup mfp-with-anim mfp-hide">
    <form role="form">
        <h3>Create Account</h3>
        <hr>
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name" tabindex="1">
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-group">
                    <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name" tabindex="2">
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="text" name="display_name" id="display_name" class="form-control" placeholder="Display Name" tabindex="3">
        </div>
        <div class="form-group">
            <input type="email" name="email" id="email" class="form-control " placeholder="Email Address" tabindex="4">
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control " placeholder="Password" tabindex="5">
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-group">
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password" tabindex="6">
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-16">
                <input type="submit" value="Register" class="btn btn-danger btn-block btn-lg" tabindex="7">
            </div>
        </div>
    </form>
</div>
<div id="log-in" class="white-popup mfp-with-anim mfp-hide">
    <form role="form">
        <h3>Inciar sesión</h3>
        <hr>
        <div class="form-group">
            <input type="text" name="access_name" id="access_name" class="form-control" placeholder="Nombre" tabindex="3">
        </div>
        <div class="form-group">
            <input type="password" name="password" id="password" class="form-control " placeholder="Contraseña" tabindex="4">
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-16">
                <input type="submit" value="Ingresar" class="btn btn-danger btn-block btn-lg" tabindex="7">
            </div>
        </div>
    </form>
</div>
</div>
<!-- wrapper end -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!--jQuery easing-->
<script src="js/jquery.easing.1.3.js"></script>
<!-- bootstrab js -->
<script src="js/bootstrap.js"></script>
<!--style switcher-->
<script src="js/style-switcher.js"></script> <!--wow animation-->
<script src="js/wow.min.js"></script>
<!-- time and date -->
<script src="js/moment.min.js"></script>
<!--news ticker-->
<script src="js/jquery.ticker.js"></script>
<!-- owl carousel -->
<script src="js/owl.carousel.js"></script>
<!-- magnific popup -->
<script src="js/jquery.magnific-popup.js"></script>
<!-- weather -->
<script src="js/jquery.simpleWeather.min.js"></script>
<!-- calendar-->
<script src="js/jquery.pickmeup.js"></script>
<!-- go to top -->
<script src="js/jquery.scrollUp.js"></script>
<!-- scroll bar -->
<script src="js/jquery.nicescroll.js"></script>
<script src="js/jquery.nicescroll.plus.js"></script>
<!--masonry-->
<script src="js/masonry.pkgd.js"></script>
<!--media queries to js-->
<script src="js/enquire.js"></script>
<!--custom functions-->
<script src="js/custom-fun.js"></script>
</body>
</html>