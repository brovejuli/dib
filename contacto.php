<?php include("common/doctype-and-head.php"); ?>
<body>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->

<?php include("common/header.php"); ?>

<!-- bage header start -->
<div class="container ">
    <div class="page-header">
        <h1>Contacto </h1>
        <ol class="breadcrumb">
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Contacto</a></li>
        </ol>
    </div>
</div>
<!-- bage header end -->
<!-- data start -->

<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12 col-sm-11">
            <div class="col-md-16">
                <div class="row">
                    <div class="col-md-6 adress">
                        <address>
                            <strong>Dirección</strong><br>
                            <p><?= utf8_encode($configuration['Empresa_Direccion']) ?></p>
                            <strong>Teléfono</strong><br>
                            <p>0221-4220054 / 4220084</p>
                        </address>
                        <address>
                            <strong>Email</strong><br>
                            Redacción <a href="mailto:#">editor@dib.com.ar</a><br>
                            Administración <a href="mailto:#">administracion@dib.com.ar</a><br>
                            Publicidad <a href="mailto:#">publicidad@dib.com.ar</a><br>
                            Deportes <a href="mailto:#">deportes@dib.com.ar</a><br>
                            Tranquera <a href="mailto:#">tranquera@dib.com.ar</a><br>
                            Plan Verde <a href="mailto:#">planverde@dib.com.ar</a><br>
                            Turismo <a href="mailto:#">turismo@dib.com.ar</a><br>
                            Vida y Salud <a href="mailto:#">vidaysalud@dib.com.ar</a><br>
                        </address>
                        <strong>Nuestras redes</strong><br>
                        <ul class="list-inline">
                            <li><a href="<?= $configuration['Url_Twitter'] ?>"><span class="ion-social-twitter"></span></a>
                            </li>
                            <li><a href="<?= $configuration['Url_Facebook'] ?>"><span
                                            class="ion-social-facebook"></span></a></li>
                            <li><a href="<?= $configuration['Url_Youtube'] ?>"><span class="ion-social-youtube"></span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <form method="post" class="comment-form" id="contact-form">
                            <div class="row">
                                <div class="form-group col-xs-16 col-sm-8 name-field">
                                    <input type="text" name="nombre" placeholder="Nombre" required=""
                                           class="form-control">
                                </div>
                                <div class="form-group col-xs-16 col-sm-8 email-field">
                                    <input type="text" name="medio" placeholder="Medio" required=""
                                           class="form-control">
                                </div>
                                <div class="form-group col-xs-16 col-sm-16 email-field">
                                    <input type="email" name="email" placeholder="Email" required=""
                                           class="form-control">
                                </div>
                                <p style="color:#41a3e7; font-size: 14px" class="col-md-16 kl-fancy-form">
                                    <strong>Asunto</strong>

                                    <select name="campo" id="cf_carlist" class="form-control">
                                        <option value="Publicidad#publicidad@dib.com.ar">Publicidad</option>
                                        <option value="Redacción#editor@dib.com.ar">Redacción</option>
                                        <option value="Administración#administracion@dib.com.ar">Administración</option>
                                        <option value="Deportes#deportes@dib.com.ar">Deportes</option>
                                        <option value="Tranquera#tranquera@dib.com.ar">Tranquera</option>
                                        <option value="Plan Verde#planverde@dib.com.ar">Plan Verde</option>
                                        <option value="Turismo#turismo@dib.com.ar">Turismo</option>
                                        <option value="Vida y Salud#vidaysalud@dib.com.ar">Vida y Salud</option>
                                    </select>
                                </p>
                                <div class="form-group col-xs-16 col-sm-16">
                                    <textarea placeholder="Tu mensaje" rows="8" class="form-control" required
                                              id="message" name="mensaje"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-danger" type="submit">Enviar mensaje</button>
                            </div>
                        </form>
                        <div class="modal fade" tabindex="-1" role="dialog" id="send-mail-modal">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Contacto</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button id="dismiss-contact-modal" type="button" class="btn btn-default hidden" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-5 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>
        </div>
    </div>
    <!-- left sec end -->
    <!-- redes -->
</div>
<!-- data end -->
<?php include("common/footer.php"); ?>

</div>
<!-- wrapper end -->

</body>
</html>