<?php include "common/doctype-and-head.php"; ?>
<body>
<script src="js/jssor.slider-23.1.6.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jssor_1_slider_init = function() {

        var jssor_1_options = {
            $AutoPlay: 1,
            $Idle: 0,
            $SlideDuration: 5000,
            $SlideEasing: $Jease$.$Linear,
            $PauseOnHover: 4,
            $SlideWidth: 160,
            $Cols: 7
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*responsive code begin*/
        /*remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 840);
                jssor_1_slider.$ScaleWidth(refSize);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        /*responsive code end*/
    };
</script>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->

<?php include("common/header.php"); ?>

<!-- bage header start -->
<div class="container ">
    <div class="page-header">
        <h1>Grupo DIB </h1>
        <ol class="breadcrumb">
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Grupo DIB</a></li>
        </ol>
    </div>
</div>
<!-- bage header end -->
<!-- data start -->

<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12 col-sm-11">
            <div class="row">
                <div class="col-md-5 col-sm-7 col-xs-16">
                    <a href="videos/Presentacion_DIB.mp4" target="_blank"> <img class="img-responsive" src="videos/video.jpg"></a>
                    <br>
                </div>
                <div class="col-md-11 col-sm-9 col-xs-16">
                    <div class="row">
                        <div class="sec-topic col-sm-16  wow fadeInDown animated" data-wow-delay="0.5s">
                            <div class="row">
                                <div class="col-sm-16 grupo">
                                    <p> <span> Diarios Bonaerenses </span> es el Grupo de medios gráficos más grande de la provincia de Buenos Aires. Fue <span>fundado en 1993 por 10 diarios.</span>

                                        Actualmente suministra sus servicios a más de <span>50 publicaciones </span>de la provincia de Buenos Aires.

                                        Estas publicaciones en conjunto representan una tirada superior a los <span>80.000 ejemplares,</span> y cubren informativamente a más de <span>3.000.000 de habitantes.</span>

                                        El mercado del interior muestra que cada 100 diarios que se venden, 85 son de las publicaciones locales y sólo 15 de las nacionales.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="espacio"></div>

            <div class="main-title-outer pull-left">
                <div class="main-title">Nuestros productos en papel</div>
            </div>
            <div class="col-md-16">
            <div class="col-md-3 col-sm-3 col-xs-16">
                <img class="img-responsive" src="images/grupo/extra.png" alt=""/>
            </div>
            <div class="sec-topic col-md-13 col-sm-13 col-xs-16 wow fadeInDown animated grupo" data-wow-delay="0.5s">
                <p> <span> Extra, el país y el mundo  </span> – Edición diaria <br> Producto especializado en información internacional, nacional, provincial y deportiva. </p>
            </div>
            </div>

            <div class="col-md-16">
            <div class="col-md-3 col-sm-3 col-xs-16">
                <img class="img-responsive" src="images/grupo/tranquera.png" alt=""/>
            </div>
            <div class="sec-topic col-md-13 col-sm-13 col-xs-16 wow fadeInDown animated grupo" data-wow-delay="0.5s">
                <p> <span> Tranquera Abierta  </span> – Edición semanal <br> Suplemento agropecuario de mayor circulación en la pampa húmeda. </p>
            </div>
            </div>

            <div class="col-md-16">
            <div class="col-md-3 col-sm-3 col-xs-16">
                <img class="img-responsive" src="images/grupo/deviaje.png" alt=""/>
            </div>
            <div class="sec-topic col-md-13 col-sm-13 col-xs-16 wow fadeInDown animated grupo" data-wow-delay="0.5s">
                <p> <span> De Viaje  </span> – Edición quincenal <br> Suplemento de turismo a todo color con destinos nacionales e internacionales. </p>
            </div>
            </div>

            <div class="col-md-16">
            <div class="col-md-3 col-sm-3 col-xs-16">
                <img class="img-responsive"  src="images/grupo/verde.png" alt=""/>
            </div>
            <div class="sec-topic col-md-13 col-sm-13 col-xs-16 wow fadeInDown animated grupo" data-wow-delay="0.5s">
                <p> <span> Plan Verde  </span> – Edición quincenal <br> Suplemento dedicado al abordaje de temas ambientales, alimentación consciente y vida sana. </p>
            </div>
            </div>

            <div class="col-md-16">
            <div class="col-md-3 col-sm-3 col-xs-16">
                <img class="img-responsive" src="images/grupo/vida.png" alt=""/>
            </div>
            <div class="sec-topic col-md-13 col-sm-13 col-xs-16 wow fadeInDown animated grupo" data-wow-delay="0.5s">
                <p> <span> Vida y Salud  </span> – Edición semanal <br> Suplemento semanal dedicado a los temas de la salud. </p>
            </div>
            </div>

            <div class="col-md-16">
            <div class="col-md-3 col-sm-3 col-xs-16">
                <img class="img-responsive" src="images/grupo/motor.png" alt=""/>
            </div>
            <div class="sec-topic col-md-13 col-sm-13 col-xs-16 wow fadeInDown animated grupo" data-wow-delay="0.5s">
                <p> <span> Mundo Motor  </span> – Edición semanal <br> Suplemento con novedades y tendencias en materia de autos, motos, 4x4 y camiones. </p>
            </div>
            </div>

            <div class="col-md-16">
            <div class="col-md-3 col-sm-3 col-xs-16">
                <img class="img-responsive" src="images/grupo/infobit.png" alt=""/>
            </div>
            <div class="sec-topic col-md-13 col-sm-13 col-xs-16 wow fadeInDown animated grupo" data-wow-delay="0.5s">
                <p> <span> Infobit  </span> – Edición semanal <br> Suplemento de informática y telecomunicaciones. </p>
            </div>
            </div>
            <div class="espacio"></div>

            <div class="main-title-outer pull-left logos">
                <div class="main-title">Servicio en más de 50 diarios del interior bonaerense </div>
            </div>

            <div id="jssor_1" class="logos" style="position:relative;margin:0 auto;top:0px;left:0px;width:600px;height:30px;overflow:hidden;visibility:hidden;">
                <!-- Loading Screen -->
                <div data-u="loading" style="position:absolute;top:0px;left:0px;background:url('img/loading.gif') no-repeat 50% 50%;background-color:rgba(0, 0, 0, 0.7);"></div>
                <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:600px;height:30px;overflow:hidden;">
                    <div>
                        <img data-u="image" src="images/diarios/lavoz.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/ecos.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/laopinion.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/elorden.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/elpopular.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/eltiempo.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/lamanana.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/9dejulio.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/lavoz.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/ecos.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/laopinion.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/elorden.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/elpopular.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/eltiempo.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/lamanana.png" />
                    </div>
                    <div>
                        <img data-u="image" src="images/diarios/9dejulio.png" />
                    </div>


                </div>
            </div>
            <script type="text/javascript">jssor_1_slider_init();</script>


        </div>



        <div class="col-md-4 col-sm-5 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>

        </div>


    </div>
    <!-- left sec end -->
    <!-- redes -->


</div>
<!-- data end -->
<?php include("common/footer.php"); ?>

</div>
<!-- wrapper end -->

</body>
</html>