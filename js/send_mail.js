$(document).ready(function () {
    $('#contact-form').submit(function (e) {
        e.preventDefault()
        $.ajax({
            async: true,
            url: 'php/send_mail.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            beforeSend: function (response) {
                $('#send-mail-modal').modal('show').find('.modal-body').html('Enviando mensaje...')
            },
            success: function (response) {
                $('#send-mail-modal').find('.modal-body').html(response.msg)
                $('#dismiss-contact-modal').removeClass('hidden');
            }
        });
    })
    $('#send-mail-modal').modal({
        keyboard: false,
        backdrop: false,
        show: false,
    })
    $('#send-mail-modal').on('hidden.bs.modal', function (e) {
        location.reload();
    })
    $('#email-share').submit(function (e) {
        e.preventDefault()
        $.ajax({
            async: true,
            url: 'php/share_mail.php',
            type: 'POST',
            data: {
                email: $(this).find('#email').val(),
                nota: $('.contenido-nota').html(),
                titulo: $('.contenido-nota').find('h2,h3').text(),
                link: window.location.href,
            },
            dataType: 'json',
            beforeSend: function (response) {
                $('#email-share').find('#email').prop('disabled', true);
                $('#email-share').find('button').prop('disabled', true).text('Enviado...');
            },
            success: function (response) {
                if (response.status) {
                    $('#email-share').find('#email').prop('disabled', false).val('');
                    $('#email-share').find('button').prop('disabled', false).text('enviar');
                    $('#myModal').modal('hide')
                }
                else{
                    $('#email-share').find('#email').val(response.msg);
                    $('#email-share').find('button').text('ERROR');
                }
            }
        });
    })

})