$(document).ready(function () {
    $('#login-form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            async: true,
            url: 'php/checkuser.php',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'json',
            beforeSend: function (response) {
            },
            success: function (response) {
                if (response.status)
                    location.reload();
                else
                    alert(response.msg)
            }
        });
    })

    $('.kill-session').click(function () {
        $.ajax({
            async: true,
            url: 'php/kill_session.php',
            type: 'POST',
            dataType: 'json',
            beforeSend: function (response) {
            },
            success: function (response) {
                if (response.status)
                    location.reload();
                else
                    alert(response.msg)
            }
        });
    })
})