/**
 * Created by radioeletrico on 07/07/16.
 */
$(document).ready(function () {

    $('.facebook-post-share').click(function (e) {
        e.preventDefault();
        facebookShare(
            $(this).data('link'),
            $(this).data('caption'),
            $(this).data('name'),
            $(this).data('description'),
            $(this).data('image'));
    });

    $('.popup').click(function (event) {
        var width = 575,
            height = 400,
            left = ($(window).width() - width) / 2,
            top = ($(window).height() - height) / 2,
            url = this.href,
            opts = 'status=1' +
                ',width=' + width +
                ',height=' + height +
                ',top=' + top +
                ',left=' + left;

        window.open(url, 'twitter', opts);

        return false;
    });

    $('.popup-post').click(function (event) {
        var width = 575,
            height = 400,
            left = ($(window).width() - width) / 2,
            top = ($(window).height() - height) / 2,
            url = $(this).data('url'),
            opts = 'status=1' +
                ',width=' + width +
                ',height=' + height +
                ',top=' + top +
                ',left=' + left;

        window.open(url, 'twitter', opts);

        return false;
    });

});
function facebookShare(link, caption, name, description, image) {
    FB.ui({
        appId: '1370941839685660',
        method: 'feed',
        link: link,
        name: name,
        caption: caption,
        picture: image,
        description: description,
        redirect_uri: link,
    }, function (response) {
    });
}