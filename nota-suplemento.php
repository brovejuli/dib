<?php include "common/doctype-and-head.php"; ?>
<body>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '1370941839685660',
            xfbml: true,
            version: 'v2.9'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->

<?php include "common/header.php"; ?>
<?php $suplemento = $dbManager->getSuplemento($_REQUEST['suplemento']) ?>
<?php $categoria = $dbManager->getCategoriaSuplementos($suplemento['categoria']) ?>
<?php $edicion = $dbManager->getSuplementosArchivo($suplemento['categoria'])[0] ?>
<!-- bage header start -->
<div class="container">
    <div class="page-header <?= $dbManager->suplementos_title_clases[$categoria['Id']] ?>">
        <h1 class="<?= $dbManager->suplementos_clases[$categoria['Id']] ?>">
            <span class="<?= $categoria['icon'] ?>"></span>
            SUPLEMENTO <?= utf8_encode($categoria['concepto']) ?> </h1>
        <ol class="breadcrumb">
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Suplementos</a></li>
            <li class="active"> <?= utf8_encode($categoria['concepto']) ?></li>
        </ol>
    </div>
</div>
<!-- bage header end -->
<!-- data start -->
<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12 col-sm-11">
            <div class="row">
                <div class="col-md-4 author-box">
                    <div class="col-xs-12 col-md-16">
                        <div class="topic col-sm-16 bajada_tranquera"><a
                                href="gestion/archivos/suplementos/<?= utf8_encode($edicion['archivo']) ?>"><img
                                    class="img-thumbnail img-responsive"
                                    src="gestion/images/blogmanagement/suplementos/big/<?= $edicion['portada'] ?>"
                                    width="100%" alt=""></a>
                        </div>
                    </div>
                    <div>
                        <!--                        <p>Compartí la nota</p>-->
                        <!--                        <ul class="list-inline blog_redes">-->
                        <!--                            <li><a href="https://twitter.com/share?text=Diarios Bonaerenses"-->
                        <!--                                   class=\"twitter"\-->
                        <!--                                   onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">-->
                        <!--                                    <span class="ion-social-twitter"></span></a></li>-->
                        <!--                            <li><a href="#">-->
                        <!--                                    <span class="ion-social-facebook facebook-post-share"-->
                        <!--                                          data-link="-->
                        <? //= HTTP_WEBSITE ?><!--nota-blog.php?nota=--><? //= $suplemento['Id'] ?><!--"-->
                        <!--                                          data-caption="-->
                        <? //= utf8_encode($suplemento['titulo']) ?><!--"-->
                        <!--                                          data-name="-->
                        <? //= utf8_encode($suplemento['titulo']) ?><!--"-->
                        <!--                                          data-description="-->
                        <? //= utf8_encode(strip_tags($suplemento['cuerpo'])) ?><!--"></span></a>-->
                        <!--                            </li>-->
                        <!--                        </ul>-->
                        <div>
                            <div class="table-responsive col-md-15">
                                <br>
                                <h6 style="text-align: left">Compartí la nota</h6>
                                <table class="table table-bordered social social2">
                                    <tbody>

                                    <tr>
                                        <td><a class="twitter2 "
                                               href="https://twitter.com/share?text=Diarios Bonaerenses"
                                               class=\"twitter"\
                                               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                                <p><span class="ion-social-twitter"></span>
                                                </p>
                                            </a></td>
                                        <td><a class="facebook2 facebook-post-share" href="#"
                                               data-link="<?= HTTP_WEBSITE ?>nota-blog.php?nota=<?= $suplemento['Id'] ?>"
                                               data-caption="<?= $suplemento['titulo'] ?>"
                                               data-name="<?= $suplemento['titulo'] ?>"
                                               data-description="<?= (strip_tags($suplemento['cuerpo'])) ?>"
                                               data-image="gestion/archivos/suplementos/<?= utf8_encode($edicion['archivo']) ?>">
                                                <p><span class=" ion-social-facebook"></span>
                                                </p>
                                            </a></td>
                                        <td><a class="correo" data-toggle="modal" data-target="#myModal" role="button">
                                                <p><span class="ion-android-mail"></span>
                                                </p>
                                            </a>
                                            <div class="modal fade" id="myModal" role="dialog">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">
                                                                <small>Compartir por E-mail</small>
                                                            </h4>
                                                        </div>
                                                        <form id="email-share">
                                                            <div class="modal-body">
                                                                <input type="email" name="email" id="email"
                                                                       class="form-control" required>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Compartir
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tbody>
                                </table>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="sec-topic col-sm-16  wow fadeInDown animated " data-wow-delay="0.5s">
                            <div class="row">
                                <div class="contenido-nota">
                                    <div class="col-sm-16 sec-info">
                                        <h2><?= utf8_encode($suplemento['titulo']) ?></h2>
                                        <p><?= ($suplemento['cuerpo']) ?></p>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-5 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>
        </div>
    </div>
    <!-- left sec end -->
    <!-- redes -->
</div>
<!-- data end -->
<?php include("common/footer.php"); ?>

</div>
<!-- wrapper end -->

</body>
</html>