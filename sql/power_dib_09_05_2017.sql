-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2017 at 08:45 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `power_dib`
--

-- --------------------------------------------------------

--
-- Table structure for table `abonados`
--

CREATE TABLE `abonados` (
  `Id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `medio` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `celular` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `habilitado` tinyint(2) DEFAULT '1',
  `alta` date DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `session_time` datetime DEFAULT NULL,
  `logins` int(11) NOT NULL,
  `borrar` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `abonados`
--

INSERT INTO `abonados` (`Id`, `nombre`, `user`, `pass`, `medio`, `telefono`, `celular`, `email`, `habilitado`, `alta`, `session_id`, `session_time`, `logins`, `borrar`) VALUES
(1, 'Javier', 'radioeletrico', 'a247b20066e72c1f10d930cda9afa27e', 'Diario la nación', '3816173860', '3816173860', 'javier.peralta666@gmail.com', 0, '2017-05-02', NULL, NULL, 0, 1),
(2, 'Javier', 'sgimeno', 'b73c718efd5316b4e79d32fae3e18b20', 'Diario la nación', '3816173860', '3816173860', 'javier.peralta666@gmail.com', 0, '2017-05-02', NULL, NULL, 0, 0),
(3, 'Javier Peralta', 'radioeletrico', '21232f297a57a5a743894a0e4a801fc3', 'Diario la nación', '3816173860', '3816173860', 'javier.peralta666@hotmail.com', 0, '2017-05-02', '', '2017-05-02 17:51:19', 0, 0),
(4, 'Carlita Gramichi', 'cargf', '81dc9bdb52d04dc20036dbd8313ed055', 'Reportera de la UNT', '3865323236', '3865323236', 'caaar.m21@gmail.com', 0, '2017-05-02', '', '2017-05-04 12:43:09', 0, 0),
(5, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '1234', '1234', 'javier.peralta666@gmail.com', 0, '2017-05-09', '', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `abonados_sesiones`
--

CREATE TABLE `abonados_sesiones` (
  `Id` int(11) NOT NULL,
  `session_id` text,
  `session_time` datetime DEFAULT NULL,
  `user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE `categorias` (
  `Id` int(11) NOT NULL,
  `categoria` varchar(255) DEFAULT NULL,
  `mostrar` tinyint(2) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`Id`, `categoria`, `mostrar`) VALUES
(1, 'Información general', 1),
(2, 'Economía', 1),
(3, 'Política', 1),
(4, 'Policiales', 1),
(5, 'Municipales', 1);

-- --------------------------------------------------------

--
-- Table structure for table `categorias_suplementos`
--

CREATE TABLE `categorias_suplementos` (
  `Id` int(11) NOT NULL,
  `concepto` varchar(255) DEFAULT NULL,
  `resenia` text,
  `mostrar` tinyint(2) DEFAULT NULL,
  `icon` varchar(255) NOT NULL,
  `borrar` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categorias_suplementos`
--

INSERT INTO `categorias_suplementos` (`Id`, `concepto`, `resenia`, `mostrar`, `icon`, `borrar`) VALUES
(3, 'Tranquera', 'El suplemento agropecuario de mayor circulación en la pampa húmeda Inserto en 23 publicaciones que se distribuyen en 50 partidos de la Provincia de Buenos Aires, Tranquera abierta se ha constituido en el suplemento dirigido al sector agropecuario, con mayor circulación en dicha región.', 1, 'ion-cloud', 0),
(4, 'Vida y salud', 'Ut nec rutrum metus. Quisque convallis interdum nulla, vel lobortis nulla tempus a. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce fermentum nisi non sapien blandit, sed commodo dolor tincidunt. Quisque nec lectus quis odio ultricies luctus non sed ex. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis elit lorem, iaculis sed tincidunt vitae, interdum vel purus.', 1, 'ion-heart', 0),
(5, 'Mundo motor', 'Nunc nec arcu purus. Fusce elementum tortor quis tincidunt fermentum. Fusce est nibh, placerat eget iaculis sed, placerat at felis. Ut non magna ipsum. Curabitur feugiat diam id orci suscipit, dignissim vestibulum augue placerat. Proin varius ornare ipsum ut dapibus. Proin tincidunt dolor nec lorem bibendum finibus. Cras facilisis elit cursus arcu dapibus, at mollis magna dignissim.', 1, 'ion-model-s', 0),
(6, 'Infobit', 'Nullam sagittis eget nisi ac laoreet. Praesent ultrices augue felis, sit amet molestie magna pellentesque ac. Maecenas vel auctor augue, sed pharetra quam. In eget leo eleifend, venenatis metus rutrum, feugiat quam. Morbi id arcu id libero consequat malesuada. Suspendisse consectetur dapibus interdum. Proin at massa finibus, ullamcorper nibh et, accumsan eros. Nunc scelerisque dictum ultricies. Proin mauris odio, vestibulum ultrices consequat id, bibendum id purus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus orci dui, condimentum sed tristique vitae, lobortis ut nibh. Sed hendrerit tortor turpis, id volutpat ipsum luctus sit amet.', 1, 'ion-laptop', 0),
(8, 'Plan verde', 'Ut nec rutrum metus. Quisque convallis interdum nulla, vel lobortis nulla tempus a. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce fermentum nisi non sapien blandit, sed commodo dolor tincidunt. Quisque nec lectus quis odio ultricies luctus non sed ex. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis elit lorem, iaculis sed tincidunt vitae, interdum vel purus.', 1, 'ion-leaf', 0),
(9, 'Extra Semanal', 'Quisque pellentesque vestibulum dignissim. Duis vel purus sapien. Nullam non libero vel odio finibus dignissim non sed lorem. Ut magna velit, aliquet sed arcu eu, porta facilisis mauris. Vestibulum vitae accumsan quam. Proin sit amet elit orci. In hac habitasse platea dictumst. Integer tempus metus libero, vitae malesuada leo dictum vel. Pellentesque ut egestas velit.', 1, 'ion-calendar', 0);

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `Id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mostrar` tinyint(2) DEFAULT NULL,
  `borrar` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `Id` int(11) NOT NULL,
  `variable` char(255) NOT NULL,
  `value` char(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`Id`, `variable`, `value`) VALUES
(1, 'MailHost', 'mail.powerbyte.com.ar'),
(2, 'SMTPAuth', 'true'),
(3, 'Mail_Port', '587'),
(4, 'Mail_Username', ''),
(5, 'Mail_Password', 'Jperalta2015'),
(6, 'Mail_From', 'jperalta@powerbyte.com.ar'),
(7, 'Mail_From_Name', 'Orsetti Propiedades'),
(8, 'Title', ''),
(11, 'Empresa_Nombre', 'Orsetti Propiedades'),
(13, 'Empresa_www', 'www.orsettipropiedades.com.ar'),
(14, 'Empresa_Telefono', '(0221) 422-0084/0054'),
(15, 'Youtube_Video_Url', ''),
(16, 'Url_Facebook', 'https://www.facebook.com/orsetti.propiedades/?fref=ts'),
(17, 'Url_Twitter', ''),
(18, 'Url_Pinterest', 'https://www.pinterest.com/'),
(19, 'Url_Instagram', ''),
(20, 'Url_Linkedin', ''),
(21, 'Metatags_Description', ''),
(22, 'Metatags_Keywords', ''),
(23, 'Metatags_Author', ''),
(24, 'Empresa_Direccion', '44 Nº 4579 e/ 186 y 187 Lisandro Olmos'),
(25, 'Empresa_Localidad', ''),
(26, 'Empresa_Movil', '0221 156518778'),
(27, 'Url_Google', ''),
(28, 'Pagination_Limit', '15'),
(29, 'Url_Youtube', 'https://www.youtube.com/channel/UC6TYiaitpgPQkeLoqEQ-uzg');

-- --------------------------------------------------------

--
-- Table structure for table `diarios`
--

CREATE TABLE `diarios` (
  `Id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `enlace` text,
  `borrar` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `diarios`
--

INSERT INTO `diarios` (`Id`, `nombre`, `enlace`, `borrar`) VALUES
(1, 'nombre', 'enlace', 0),
(2, 'http://www.diarioactualidad.com/', 'Actualidad - General Villegas', 0),
(3, 'Actualidad', 'http://www.diarioactualidad.com/', 0);

-- --------------------------------------------------------

--
-- Table structure for table `diario_extra`
--

CREATE TABLE `diario_extra` (
  `Id` int(11) NOT NULL,
  `archivo` text,
  `portada` text,
  `borrar` tinyint(2) DEFAULT '0',
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `diario_extra`
--

INSERT INTO `diario_extra` (`Id`, `archivo`, `portada`, `borrar`, `fecha`) VALUES
(2, 'test.pdf', '5910e8fcdc18a.jpg', 0, '2017-05-08'),
(3, 'test.pdf', '5910eaa818b64.jpg', 0, '2017-05-08');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `Id` int(11) NOT NULL,
  `level` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`Id`, `level`) VALUES
(0, 'Aministrador general'),
(1, 'Periodista nivel 1'),
(2, 'Periodista nivel 2');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `Id` char(10) NOT NULL,
  `Nombre` char(30) NOT NULL,
  `Grupo` char(50) NOT NULL,
  `Titulo` char(50) NOT NULL,
  `Link` char(30) NOT NULL,
  `iconCls` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`Id`, `Nombre`, `Grupo`, `Titulo`, `Link`, `iconCls`) VALUES
('0', 'Noticias', 'Noticias', 'Noticias', 'noticias', 'fa-home'),
('1', 'Suplementos', 'Suplementos', 'Suplementos', 'suplementos', 'fa-file-text-o'),
('2', 'Notas', 'Notas', 'Notas', 'notas', 'fa-file-text-o'),
('4', 'Diarios', 'Diarios', 'Diarios', 'diarios', 'fa-newspaper-o'),
('5', 'Diario extra', 'Diario extra', 'Diario extra', 'diario_extra_listar', 'fa-user'),
('6', 'Principales', 'Principales', 'Principales', 'principales', 'fa-star'),
('7', 'Categorias', 'Categorias', 'Categorias', '', 'fa-list'),
('7.1', 'Noticias', 'Categorias', 'Noticias', 'categorias_noticias', 'fa-circle-o'),
('7.2', 'Suplementos', 'Categorias', 'Suplementos', 'categorias_suplementos', 'fa-circle-o'),
('8', 'Usuarios', 'Usuarios', 'Usuarios', '', 'fa-user'),
('8.1', 'Usuarios', 'Usuarios', 'Usuarios', 'users', 'fa-circle-o'),
('8.2', 'Abonados', 'Usuarios', 'Abonados', 'abonados', 'fa-circle-o'),
('9', 'Configuracion', 'Configuracion', 'Configuracion', 'configuration', 'fa-cogs');

-- --------------------------------------------------------

--
-- Table structure for table `menu_levels`
--

CREATE TABLE `menu_levels` (
  `menu` char(10) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_levels`
--

INSERT INTO `menu_levels` (`menu`, `level`) VALUES
('0', 1),
('1', 1),
('1', 2),
('2', 1),
('2', 2),
('5', 1),
('5', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notas`
--

CREATE TABLE `notas` (
  `Id` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `volanta` varchar(255) DEFAULT NULL,
  `cuerpo` text,
  `borrar` tinyint(2) DEFAULT '0',
  `publicar` tinyint(2) DEFAULT '1',
  `usuario` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notas`
--

INSERT INTO `notas` (`Id`, `fecha`, `titulo`, `volanta`, `cuerpo`, `borrar`, `publicar`, `usuario`) VALUES
(11, '2017-05-08 13:26:00', 'TItulo nota prueba', 'vol an ta', '<p>kaljasf</p><p>asñfjalksf</p><p>asñlfañsfa</p><p>lñasfñajsf</p><p>kasjflkahsf</p><p>kñasfhlkahsflasf</p>', 0, 1, '27'),
(12, '2017-05-08 13:43:00', 'safasf', 'a a a a a', '<p>asfa sf</p>', 0, 0, '27'),
(13, '2017-05-20 23:58:00', 'xda', 'z z z z z z ', '<p>xxx</p><p>&nbsp; xxx</p>', 0, 1, '27'),
(14, '2017-05-08 14:44:00', 'Titullo usuario 1', 'alks ndalsk jdlka', '<p>as dfsad fasd</p><p>sad fsad&nbsp;</p><p>sdf asd f</p>', 0, 1, '26'),
(15, '2017-05-02 15:15:00', 'Titulo nota nueva', 'Crisis en Italia', '<h3 style="font-family: Neucha, sans-serif; font-weight: bold; color: rgb(52, 52, 52); margin-top: 10px; font-size: 30px; background-color: rgb(253, 253, 253); border-radius: 0px !important;">Nuevo ring en el que el chavismo y la oposición buscan dar el golpe de nocaut</h3><p style="color: rgb(58, 61, 63); font-size: 15px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;"><span class="letter-badge" style="border-radius: 0px !important;">L</span>orem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.&nbsp;<a href="http://dib.dev/nota-blog.php#" style="background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(66, 139, 202); border-radius: 0px !important;">Vivamus elementum semper</a>&nbsp;nisi. Aenean vulputate eleifend tellus.&nbsp;<br style="margin: 5px 0px; border-radius: 0px !important;">Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.Etiam ultricies&gt;nisi vel augue. Curabitur ullamcorper ultricies nisi.</p><blockquote style="border-color: rgb(65, 163, 231); color: rgb(141, 154, 165); font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;"><p style="color: rgb(65, 163, 231); font-size: 15px; border-radius: 0px !important;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p></blockquote><p style="color: rgb(58, 61, 63); font-size: 15px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;">Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.</p><ul class="icn-list" style="color: rgb(141, 154, 165); font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;"><li style="border-radius: 0px !important;">Lorem ipsum dolor sit amet</li><li style="border-radius: 0px !important;">Consectetur adipiscing elit</li><li style="border-radius: 0px !important;">Integer molestie lorem at massa</li><li style="border-radius: 0px !important;">Facilisis in pretium nisl aliquet</li><li style="border-radius: 0px !important;">Nulla volutpat aliquam velit</li></ul><div class="col-sm-16" style="position: relative; min-height: 1px; padding-left: 5px; padding-right: 5px; float: left; width: 636.875px; color: rgb(141, 154, 165); font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;"><img width="1000" height="606" alt="" src="http://dib.dev/images/sec/sec-3.jpg" class="img-thumbnail" style="border-color: rgb(237, 237, 237); line-height: 1.42857; background-color: rgb(248, 248, 248);"></div><p style="color: rgb(58, 61, 63); font-size: 15px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;">Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo</p><div><br></div>', 0, 1, '27'),
(16, '2017-05-08 16:01:00', 'Tercera nota', 'Volanta tercera', '<p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">Los rumores sobre la llegada de un tercer sistema operativo de Google llevan tiempo produciéndose. Esa teórica mezcla de Android y de Chrome OS se llama Fuchsia, y hay una diferencia importante con esas dos plataformas: que&nbsp;<span style="font-weight: 600;">no estará basado en el kernel Linux</span>.</p><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">El nuevo kernel se llama Magenta y&nbsp;<span style="font-weight: 600;">eso permitirá a Google ''librarse'' de esa dependencia y de la GPL</span>. Google no esconde el desarrollo de Fuchsia aunque no ha confirmado públicamente la existencia de esta plataforma, y ahora aparecen nuevas imágenes de una interfaz que apunta maneras pero a la que le queda aún mucho recorrido.</p><h2 style="margin: 47.04px auto 19.04px; font-size: 32px; color: rgb(17, 17, 17); font-family: Tofino, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen, Ubuntu, Cantarell, &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, sans-serif; font-weight: 600; line-height: 40px; max-width: 696px;">La interfaz de usuario de Fuchsia tiene nombre: Armadillo</h2><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">Como indican en Ars technica, ese kernel llamado Magenta está orientado según la documentación del desarrollo a "teléfonos modernos y ordenadores modernos con procesadores rápidos, cantidades de RAM no triviales y periféricos de todo tipo en entornos de computación abierta". La definición no es especialmente concreta, pero&nbsp;<span style="font-weight: 600;">parece apuntar al futuro de móviles y tabletas convertibles</span>&nbsp;que ahora están gobernadas por Android.</p><div class="article-asset-image article-asset-normal" style="margin-left: auto; margin-right: auto; max-width: 696px; text-align: center; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; font-family: Tofino, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen, Ubuntu, Cantarell, &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, sans-serif; font-size: 16px; line-height: 28px; color: rgb(51, 51, 51);"><div class="asset-content" style="-webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; line-height: 28px; overflow: hidden;"><div class="caption-img  " style="color: rgb(136, 136, 136); line-height: 28px; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;"><img class="centro_sinmarco" sf-srcset="https://i.blogs.es/a39c6e/fuchsialogo/450_1000.png 450w, https://i.blogs.es/a39c6e/fuchsialogo/650_1200.png 681w, https://i.blogs.es/a39c6e/fuchsialogo/1024_2000.png 1024w, https://i.blogs.es/a39c6e/fuchsialogo/1366_2000.png 1366w" sf-src="https://i.blogs.es/a39c6e/fuchsialogo/450_1000.png" alt="Fuchsialogo" srcset="https://i.blogs.es/a39c6e/fuchsialogo/450_1000.png 450w, https://i.blogs.es/a39c6e/fuchsialogo/650_1200.png 681w, https://i.blogs.es/a39c6e/fuchsialogo/1024_2000.png 1024w, https://i.blogs.es/a39c6e/fuchsialogo/1366_2000.png 1366w" src="https://i.blogs.es/a39c6e/fuchsialogo/450_1000.png" style="border-width: initial; border-style: none; display: block; width: 696px; max-width: 100%; height: auto; margin: 28px auto 0px;"><span style="font-size: 14px; line-height: 20px; color: rgb(119, 119, 119); display: block; border-bottom: 1px solid rgb(205, 225, 194); padding: 14px 0px; margin-bottom: 28px; text-align: left; max-width: 696px; margin-left: auto; margin-right: auto;">Este es el logotipo de Fuchsia</span></div></div></div><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">Uno de los últimos componentes en conocerse&nbsp;<span style="font-weight: 600;">es Armadillo, la interfaz de usuario de Fuchsia, que está basada en el SDK Flutter</span>&nbsp;que hace tiempo que muestra sus ventajas en desarrollos abiertos y que ahora también cobrará vida en la interfaz visual del sistema operativo de Google.</p><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">Es posible compilar el proyecto y lograr "ver cómo queda todo", pero&nbsp;<span style="font-weight: 600;">de momento la usabilidad de Fuchsia es muy limitada</span>. En su estado actual nos encontramos con una pantalla de inicio con un teclado, un botón de inicio y algo similar a un gestor de ventanas.</p><div class="article-asset-image article-asset-normal" style="margin-left: auto; margin-right: auto; max-width: 696px; text-align: center; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; font-family: Tofino, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen, Ubuntu, Cantarell, &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, sans-serif; font-size: 16px; line-height: 28px; color: rgb(51, 51, 51);"><div class="asset-content" style="-webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; line-height: 28px; overflow: hidden;"><img class="centro_sinmarco" sf-srcset="https://i.blogs.es/0fe3f9/fuchsia2/450_1000.jpg 450w, https://i.blogs.es/0fe3f9/fuchsia2/650_1200.jpg 681w, https://i.blogs.es/0fe3f9/fuchsia2/1024_2000.jpg 1024w, https://i.blogs.es/0fe3f9/fuchsia2/1366_2000.jpg 1366w" sf-src="https://i.blogs.es/0fe3f9/fuchsia2/450_1000.jpg" alt="Fuchsia2" srcset="https://i.blogs.es/0fe3f9/fuchsia2/450_1000.jpg 450w, https://i.blogs.es/0fe3f9/fuchsia2/650_1200.jpg 681w, https://i.blogs.es/0fe3f9/fuchsia2/1024_2000.jpg 1024w, https://i.blogs.es/0fe3f9/fuchsia2/1366_2000.jpg 1366w" src="https://i.blogs.es/0fe3f9/fuchsia2/450_1000.jpg" style="border-width: initial; border-style: none; display: block; width: 696px; max-width: 100%; height: auto; margin: 28px auto;"></div></div><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">En esa pantalla de inicio el comportamiento es el de una gran lista en la que hacemos scroll vertical, con algunos datos en el centro. La interfaz presenta&nbsp;<span style="font-weight: 600;">una lista de aplicaciones recientes en forma de tarjetas</span>&nbsp;?similares a las que presenta la multitarea actual en Android? mientras que debajo están una lista de sugerencias tipo "Google Now".</p><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">El funcionamiento, como indican quienes lo han probado, es aún errático y muy limitado, pero en Google se están tomando muy en serio este desarrollo, tal y como confirmaba uno de los responsables del desarrollo. "<span style="font-weight: 600;">No es un juguete ni un proyecto del 20% [del tiempo]</span>", comentaba.</p><div class="article-asset-image article-asset-normal" style="margin-left: auto; margin-right: auto; max-width: 696px; text-align: center; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; font-family: Tofino, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen, Ubuntu, Cantarell, &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, sans-serif; font-size: 16px; line-height: 28px; color: rgb(51, 51, 51);"><div class="asset-content" style="-webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; line-height: 28px; overflow: hidden;"><img class="centro_sinmarco" sf-srcset="https://i.blogs.es/d89d6a/fuchsia3/450_1000.png 450w, https://i.blogs.es/d89d6a/fuchsia3/650_1200.png 681w, https://i.blogs.es/d89d6a/fuchsia3/1024_2000.png 1024w, https://i.blogs.es/d89d6a/fuchsia3/1366_2000.png 1366w" sf-src="https://i.blogs.es/d89d6a/fuchsia3/450_1000.png" alt="Fuchsia3" srcset="https://i.blogs.es/d89d6a/fuchsia3/450_1000.png 450w, https://i.blogs.es/d89d6a/fuchsia3/650_1200.png 681w, https://i.blogs.es/d89d6a/fuchsia3/1024_2000.png 1024w, https://i.blogs.es/d89d6a/fuchsia3/1366_2000.png 1366w" src="https://i.blogs.es/d89d6a/fuchsia3/450_1000.png" style="border-width: initial; border-style: none; display: block; width: 696px; max-width: 100%; height: auto; margin: 28px auto;"></div></div><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">Todo apunta a que Fuchsia es algo así como&nbsp;<span style="font-weight: 600;">un Android rediseñado desde cero para coger lo mejor que tiene esta plataforma</span>&nbsp;pero aprovechando ideas que no podrían implementarse (al menos, no fácilmente) allí. Puede que Google comience a hablar por fin de este proyecto en Google I/O, pero lo que parece estar claro es que aún queda bastante tiempo para que Fuchsia pueda competir con sus hermanos mayores.</p>', 0, 1, '27'),
(17, '2017-05-08 16:04:00', 'Nota cuarta desplazadora', 'aslk djaslk jd', '<p style="margin-right: auto; margin-bottom: 28px; margin-left: auto; font-size: 20px; max-width: 696px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; background-color: rgb(251, 251, 251);">Para todos aquellos que hemos crecido rodeados de cine, y que estamos dedicando nuestra vida al séptimo arte de un modo u otro, la acción de seleccionar un título, comprar una entrada, y sumergirnos en la acogedora oscuridad que ofrece una sala de proyecciones&nbsp;<span style="font-weight: 700;">trasciende el estatus de consumo de ocio para convertirse en un auténtico ritual</span>.</p><p style="margin-right: auto; margin-bottom: 28px; margin-left: auto; font-size: 20px; max-width: 696px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; background-color: rgb(251, 251, 251);">Acceder al recinto, pedirle al taquillero una entrada ?dos si tenemos la suerte de ir acompañados? centrada, de la mitad para atrás ?porque hay que leer bien los planos, aunque esto va a gusto del consumidor?, sentarse en la butaca y disfrutar en la penumbra de un largo de nuestro género favorito es una práctica que, durante tres días, repetidos de manera periódica, puede convertirse en&nbsp;<span style="font-weight: 700;">un auténtico infierno en la tierra</span>&nbsp;conocido como la&nbsp;<a href="https://www.blogdecine.com/tag/fiesta-del-cine" style="color: rgb(191, 81, 0); text-decoration-line: underline;">"Fiesta del Cine"</a>.</p><h3 style="margin: 28px auto 14px; font-size: 27px; font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; font-weight: 700; line-height: 30.8px; max-width: 696px; color: rgb(51, 51, 51); background-color: rgb(251, 251, 251);">El terror sí tiene forma</h3><div class="article-asset-image article-asset-normal" style="margin-left: auto; margin-right: auto; max-width: 696px; text-align: center; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; font-size: 20px; background-color: rgb(251, 251, 251);"><div class="asset-content" style="overflow: hidden;"><img class="centro_sinmarco" sf-srcset="https://i.blogs.es/a9e4a0/scream-2/450_1000.jpg 450w, https://i.blogs.es/a9e4a0/scream-2/650_1200.jpg 681w, https://i.blogs.es/a9e4a0/scream-2/1024_2000.jpg 1024w, https://i.blogs.es/a9e4a0/scream-2/1366_2000.jpg 1366w" sf-src="https://i.blogs.es/a9e4a0/scream-2/450_1000.jpg" alt="Scream 2" srcset="https://i.blogs.es/a9e4a0/scream-2/450_1000.jpg 450w, https://i.blogs.es/a9e4a0/scream-2/650_1200.jpg 681w, https://i.blogs.es/a9e4a0/scream-2/1024_2000.jpg 1024w, https://i.blogs.es/a9e4a0/scream-2/1366_2000.jpg 1366w" src="https://i.blogs.es/a9e4a0/scream-2/450_1000.jpg" style="border-width: initial; border-style: none; display: block; width: 626.5px; max-width: 100%; height: auto; margin: 28px auto;"></div></div><p style="margin-right: auto; margin-bottom: 28px; margin-left: auto; font-size: 20px; max-width: 696px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; background-color: rgb(251, 251, 251);">Acercarse a nuestro cine de cabecera durante&nbsp;<span style="font-weight: 700;">?la fiesta del cine?</span>&nbsp;se convierte en&nbsp;<span style="font-weight: 700;">una experiencia aterradora</span>&nbsp;en la que infinidad de personas bloquean el acceso, formando&nbsp;<span style="font-weight: 700;">una cola tan inmensa y estremecedora como el tentáculo de un monstruo lovecraftiano</span>. Una vez superado este primer escollo, y ya en el interior, deberemos enfrentarnos a hordas de cinéfagos eventuales batallando por una necesaria dosis de&nbsp;<span style="font-weight: 700;">palomitas y refrescos</span>, que, por el bien de la humanidad, esperemos que no tengan los mismos ingredientes que el Soylent Green.</p><p style="margin-right: auto; margin-bottom: 28px; margin-left: auto; font-size: 20px; max-width: 696px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; background-color: rgb(251, 251, 251);">Abandonar el vestíbulo y acceder a la sala no hace mejorar la situación en absoluto. La&nbsp;<span style="font-weight: 700;">saturación de público</span>&nbsp;?siempre bienvenida y fantástica para la industria, todo sea dicho? te hace batallar con los ácratas de turno que se han sentado en tu sitio hasta perder la extenuación y decidir que, por no discutir más,&nbsp;<span style="font-weight: 700;">tampoco pasa nada por ver la película desde un lateral</span>. Cosas de la vida y de pertenecer a una especie gregaria.</p><div class="article-asset-image article-asset-normal" style="margin-left: auto; margin-right: auto; max-width: 696px; text-align: center; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; font-size: 20px; background-color: rgb(251, 251, 251);"><div class="asset-content" style="overflow: hidden;"><img class="centro_sinmarco" sf-srcset="https://i.blogs.es/f2bc84/en-la-boca-del-miedo/450_1000.jpg 450w, https://i.blogs.es/f2bc84/en-la-boca-del-miedo/650_1200.jpg 681w, https://i.blogs.es/f2bc84/en-la-boca-del-miedo/1024_2000.jpg 1024w, https://i.blogs.es/f2bc84/en-la-boca-del-miedo/1366_2000.jpg 1366w" sf-src="https://i.blogs.es/f2bc84/en-la-boca-del-miedo/450_1000.jpg" alt="En La Boca Del Miedo" srcset="https://i.blogs.es/f2bc84/en-la-boca-del-miedo/450_1000.jpg 450w, https://i.blogs.es/f2bc84/en-la-boca-del-miedo/650_1200.jpg 681w, https://i.blogs.es/f2bc84/en-la-boca-del-miedo/1024_2000.jpg 1024w, https://i.blogs.es/f2bc84/en-la-boca-del-miedo/1366_2000.jpg 1366w" src="https://i.blogs.es/f2bc84/en-la-boca-del-miedo/450_1000.jpg" style="border-width: initial; border-style: none; display: block; width: 696px; max-width: 100%; height: auto; margin: 28px auto;"></div></div><p style="margin-right: auto; margin-bottom: 28px; margin-left: auto; font-size: 20px; max-width: 696px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; background-color: rgb(251, 251, 251);">Arranca la proyección, y esperas que el jolgorio que reina durante los tres tráilers y setenta y cinco anuncios de telefonía se diluya una vez empiece la película. Iluso de ti. La habitación se queda completamente a oscuras durante unos instantes, hasta que&nbsp;<span style="font-weight: 700;">el patio de butacas empieza a iluminarse como un campo bucólico infestado de luciérnagas</span>. Luciérnagas con pantalla HD, cámara de 16 megapíxeles y el WhatsApp instalado.</p><p style="margin-right: auto; margin-bottom: 28px; margin-left: auto; font-size: 20px; max-width: 696px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; background-color: rgb(251, 251, 251);">Para completar la experiencia audiovisual definitiva mientras intentas enterarte del argumento de la película, el grupo de adolescentes ácratas que te usurparon tu butaca centrada, de la mitad para atrás ?repito, siempre a gusto del consumidor? comentan la película en voz alta, luchando contra la familia que tienes en la fila de delante, cuyos tres hijos y sus gritos salidos del mismísimo averno&nbsp;<span style="font-weight: 700;">ponen a prueba la potencia del maravilloso sistema de sonido Dolby Atmos de la sala</span>. Una auténtica maravilla, vamos.</p><h3 style="margin: 28px auto 14px; font-size: 27px; font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; font-weight: 700; line-height: 30.8px; max-width: 696px; color: rgb(51, 51, 51); background-color: rgb(251, 251, 251);">¿Denostando la cultura?</h3><div class="article-asset-image article-asset-normal" style="margin-left: auto; margin-right: auto; max-width: 696px; text-align: center; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; font-size: 20px; background-color: rgb(251, 251, 251);"><div class="asset-content" style="overflow: hidden;"><img class="centro_sinmarco" sf-srcset="https://i.blogs.es/817ace/donnie-darko/450_1000.jpg 450w, https://i.blogs.es/817ace/donnie-darko/650_1200.jpg 681w, https://i.blogs.es/817ace/donnie-darko/1024_2000.jpg 1024w, https://i.blogs.es/817ace/donnie-darko/1366_2000.jpg 1366w" sf-src="https://i.blogs.es/817ace/donnie-darko/450_1000.jpg" alt="Donnie Darko" srcset="https://i.blogs.es/817ace/donnie-darko/450_1000.jpg 450w, https://i.blogs.es/817ace/donnie-darko/650_1200.jpg 681w, https://i.blogs.es/817ace/donnie-darko/1024_2000.jpg 1024w, https://i.blogs.es/817ace/donnie-darko/1366_2000.jpg 1366w" src="https://i.blogs.es/817ace/donnie-darko/450_1000.jpg" style="border-width: initial; border-style: none; display: block; width: 696px; max-width: 100%; height: auto; margin: 28px auto;"></div></div><p style="margin-right: auto; margin-bottom: 28px; margin-left: auto; font-size: 20px; max-width: 696px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; background-color: rgb(251, 251, 251);">Lo expuesto en los anteriores párrafos no es más que una dramatización ?o no? sobre los efectos de un evento que, por suerte ?y a la vez, por desgracia?, puede hacer llegar el séptimo arte, en el formato que merece ser disfrutado, a muchos aficionados cuya economía no les permite acudir religiosamente cada semana a ver los estrenos que bombardean la cartelera a lo largo y ancho del país. Una&nbsp;<span style="font-weight: 700;">?fiesta del cine?</span>&nbsp;que, rebajando el precio de las entradas a menos de tres euros,&nbsp;<span style="font-weight: 700;">desborda los cines adheridos a la promoción convocatoria tras convocatoria</span>.</p><p style="margin-right: auto; margin-bottom: 28px; margin-left: auto; font-size: 20px; max-width: 696px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; background-color: rgb(251, 251, 251);">Por supuesto, no todo el público que acude a la cita, celebrada por muchos, se comporta como una horda de neandertales ya de por si presente en cualquier sesión regular de fin de semana en la que las entradas de más de nueve euros están a la orden del día. Aún queda gente con dos dedos de frente que&nbsp;<span style="font-weight: 700;">aprecia la experiencia de disfrutar de un largometraje en condiciones óptimas, sin denostarla por el mero hecho de estar rebajada hasta el precio de una cerveza en una terraza del centro</span>.</p><div class="article-asset-image article-asset-normal" style="margin-left: auto; margin-right: auto; max-width: 696px; text-align: center; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; font-size: 20px; background-color: rgb(251, 251, 251);"><div class="asset-content" style="overflow: hidden;"><img class="centro_sinmarco" sf-srcset="https://i.blogs.es/ebc7f9/sinister/450_1000.jpg 450w, https://i.blogs.es/ebc7f9/sinister/650_1200.jpg 681w, https://i.blogs.es/ebc7f9/sinister/1024_2000.jpg 1024w, https://i.blogs.es/ebc7f9/sinister/1366_2000.jpg 1366w" sf-src="https://i.blogs.es/ebc7f9/sinister/450_1000.jpg" alt="Sinister" srcset="https://i.blogs.es/ebc7f9/sinister/450_1000.jpg 450w, https://i.blogs.es/ebc7f9/sinister/650_1200.jpg 681w, https://i.blogs.es/ebc7f9/sinister/1024_2000.jpg 1024w, https://i.blogs.es/ebc7f9/sinister/1366_2000.jpg 1366w" src="https://i.blogs.es/ebc7f9/sinister/450_1000.jpg" style="border-width: initial; border-style: none; display: block; width: 696px; max-width: 100%; height: auto; margin: 28px auto;"></div></div><p style="margin-right: auto; margin-bottom: 28px; margin-left: auto; font-size: 20px; max-width: 696px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; background-color: rgb(251, 251, 251);"><span style="font-weight: 700;">Ir al cine, más que un ritual, es una fiesta</span>. Una celebración colectiva en la que, hace años, una afluencia masiva no estaba relacionada en lo más mínimo con una experiencia caótica, desagradable e irrespetuosa tanto hacia el público, como hacia la película a proyectar. Es por esto que&nbsp;<span style="font-weight: 700;">duele especialmente ver desvirtuado un espectáculo tan enorme como el cine</span>&nbsp;que, como el resto de opciones culturales, se van devaluando progresivamente con el paso de los años.</p><p style="margin-right: auto; margin-bottom: 28px; margin-left: auto; font-size: 20px; max-width: 696px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; background-color: rgb(251, 251, 251);">Mi mala experiencia con la&nbsp;<span style="font-weight: 700;">?fiesta del cine?</span>&nbsp;no está relacionada con la perspectiva de un esnob ermitaño que pretende estar sólo, o rodeado por un selecto grupo de gente durante una proyección comportándose como si estuviesen en misa de doce ?pocas cosas me gustan más que un pase desmadrado en el festival de&nbsp;<span style="font-weight: 700;">Sitges</span>?. Mi mala experiencia viene derivada de unos espectadores ?eventuales o no? que parecen haber olvidado, como decía Garci en su programa, lo grande que es el cine, ayudados por los gritos que claman rebajas descomunales,&nbsp;<span style="font-weight: 700;">denigrando una vez más la cultura y fomentando conductas irrespetuosas</span>.</p><p style="margin-right: auto; margin-bottom: 28px; margin-left: auto; font-size: 20px; max-width: 696px; color: rgb(51, 51, 51); font-family: &quot;Source Sans Pro&quot;, &quot;Open Sans&quot;, &quot;San Francisco&quot;, &quot;Segoe UI&quot;, &quot;Lucida Grande&quot;, Helvetica, Arial, sans-serif; background-color: rgb(251, 251, 251);">Para todos aquellos que decidáis ir al cine durante estos días&nbsp;<span style="font-weight: 700;">8, 9 y 10 de mayo</span>, disfrutad enormemente de los grandes títulos que tendréis la suerte de poder ver en pantalla grande. Por mi parte, creo que optaré por la sana ?para mis nervios? alternativa de&nbsp;<span style="font-weight: 700;">sofá, manta</span>&nbsp;y&nbsp;<a href="https://www.blogdecine.com/tag/netflix" style="color: rgb(191, 81, 0); text-decoration-line: underline;">Netflix</a>&nbsp;en vena, porque en mi salón no hay nadie con el valor suficiente para usurpar mi asiento.</p>', 0, 1, '27'),
(18, '2017-05-02 23:00:00', 'Cómo conseguir el nuevo explorador de archivos', 'Mundo tecnológico', '<p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">Redstone 3 va camino de convertirse en una actualización muy importante para Windows 10; tal vez incluso mas que la reciente Creators Update.</p><div id="singlePostContentAdvertising3" style="max-height: 1e+06px; width: 728px; height: 90px; margin: 10px auto; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">s cambios consistirán en algo más que un par de nuevas apps; en vez de eso, el aspecto del escritorio cambiará completamente, con un nuevo diseño más moderno.<br></div><h2 style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; font-size: 24px; line-height: 35px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif;">El explorador de Windows, el alérgico al cambio</h2><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">Ya hemos empezado a ver algunos indicativos de estos cambios en la interfaz; algunas apps ya siguen las nuevas líneas de diseño de Project Neon, con muchas transparencias y cambios en la disposición de los elementos.</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;"><img class="aligncenter size-full wp-image-180722" src="http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-8.jpg" alt="" width="733" height="550" srcset="http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-8.jpg 733w, http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-8-300x225.jpg 300w, http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-8-586x440.jpg 586w" sizes="(max-width: 733px) 100vw, 733px" style="max-height: 1e+06px; max-width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto; margin-bottom: 10px;"></p><div id="singlePostContentAdvertising" style="max-height: 1e+06px; width: 773.219px; float: left; margin: 0px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;"></div><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">Pero eso no será lo único que cambiará. Sorprendentemente, parece que<strong style="max-height: 1e+06px;">&nbsp;el explorador de archivos también cambiará</strong>, adaptándose al nuevo estilo. Y digo ?sorprendentemente?, porque Microsoft nunca ha sido muy dada a tocar el explorador de archivos.</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">Desde hace ya varias generaciones, el programa que usamos para navegar por las carpetas de nuestro navegador no ha cambiado mucho. De hecho, en su momento tal vez el mayor cambio fue el ?ribbon?, la barra superior que imita a la disponible en Office.</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">Por lo demás,<strong style="max-height: 1e+06px;">&nbsp;el diseño del explorador de archivos&nbsp;es prácticamente el mismo desde Windows 95</strong>. Eso está a punto de cambiar con el nuevo explorador de archivos de Windows 10.</p><h2 style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; font-size: 24px; line-height: 35px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif;">Cómo conseguir el nuevo explorador de archivos de Windows 10</h2><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">Esta en realidad es una pequeña ?sorpresa? que Microsoft ha dejado en el sistema. Así que no necesitas ser miembro de Windows Insider ni usar una versión beta de Windows 10; para usar este explorador sólo necesitas tener instalada la Creators Update.</p><ul style="max-height: 1e+06px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;"><li style="max-height: 1e+06px; list-style-position: inside;">Haz click derecho sobre el escritorio o en una carpeta y selecciona ?Nuevo? -&gt; ?Acceso directo?.<br style="max-height: 1e+06px;"><img class="aligncenter size-full wp-image-180723" src="http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-1.jpg" alt="" width="802" height="549" srcset="http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-1.jpg 802w, http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-1-300x205.jpg 300w, http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-1-768x526.jpg 768w, http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-1-643x440.jpg 643w" sizes="(max-width: 802px) 100vw, 802px" style="max-height: 1e+06px; max-width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto; margin-bottom: 10px;"></li><li style="max-height: 1e+06px; list-style-position: inside;">En la ventana que se abrirá, pega esta ruta sin las comillas:&nbsp;<strong style="max-height: 1e+06px;">?explorer shell:AppsFolder\\c5e2524a-ea46-4f67-841f-6a9465d9d515_cw5n1h2txyewy!App?<br style="max-height: 1e+06px;"><img class="aligncenter size-full wp-image-180724" src="http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-2.jpg" alt="" width="789" height="550" srcset="http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-2.jpg 789w, http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-2-300x209.jpg 300w, http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-2-768x535.jpg 768w, http://omicrono.elespanol.com/wp-content/uploads/2017/05/explorador-archivos-nuevo-2-631x440.jpg 631w" sizes="(max-width: 789px) 100vw, 789px" style="max-height: 1e+06px; max-width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto; margin-bottom: 10px;"><br style="max-height: 1e+06px;"></strong></li><li style="max-height: 1e+06px; list-style-position: inside;">Ponle el nombre que quieras.</li><li style="max-height: 1e+06px; list-style-position: inside;">El icono habrá cambiado al del explorador, y cuando lo inicies se abrirá el nuevo explorador.<br style="max-height: 1e+06px;"></li></ul><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">Este explorador es más simple y directo que el habitual. Y con esto quiero decir que&nbsp;<strong style="max-height: 1e+06px;">puede que eches en falta algunas funcionalidades</strong>; lo se porque me ha pasado. En su estado actual, parece más un explorador orientado a tablets o dispositivos móviles, y no tanto a ordenadores.</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">Eso no quiere decir que funcione mal. Si quieres<strong style="max-height: 1e+06px;">&nbsp;quitarte todas las distracciones y sólo quieres algo sencillo</strong>&nbsp;para navegar por tus carpetas, es una buena opción.</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;"></p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">El diseño recuerda mucho a otras apps que han dado el salto a Project Neon, con un menú lateral en el que podemos acceder a los dispositivios conectados y un menú inferior en el que están la mayoría de funciones.</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;"></p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">Este es un menú contextual, y por lo tanto cambiará si seleccionamos una carpeta o un archivo; podemos pegar y copiar archivos, acceder a la búsqueda o cambiar la vista a iconos, entre otras cosas.</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;"></p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 18px;">Es de imaginar que Microsoft seguirá trabajando en este explorador de cara a Redstone 3; o también es posible que mantenga dos exploradores, uno para ordenador y otro para tablets, por ejemplo. Sea como sea, es una buena curiosidad como mínimo.</p>', 0, 1, '37'),
(19, '2017-05-08 19:03:00', 'Nota de prueba last', 'Volanta 2', '<p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);">La evolución de los botones del frontal en los móviles ha sufrido tres pasos importantes. De los botones físicos con pulsación pasamos a los&nbsp;<strong style="max-height: 1e+06px;">botones capacitivos o táctiles</strong>; después a los botones virtuales impresos en pantalla. Sobre gustos no hay nada escrito, ya se sabe, pero seguro que sí te gusta saber cómo funcionan las cosas. Por ejemplo? Los botones capacitivos.</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);">Este tipo de botones son físicos porque disponen de su propio espacio en el frontal de la pantalla, pero&nbsp;<strong style="max-height: 1e+06px;">carecen de recorrido en la pulsación</strong>&nbsp;como sí tienen los botones de volumen o el de encendido. Quizá pienses que poseen una circuitería propia o un sensor dedicado a detectar las pulsaciones concretas por botón,&nbsp;pero nada más lejos de la realidad. Como me explicó un día&nbsp;<a href="https://twitter.com/Ivan_EscuderC" target="_blank" style="max-height: 1e+06px; color: rgb(255, 255, 255); font-weight: bold; background-color: rgb(33, 150, 243); padding: 0px 5px; display: inline-block;">Iván Escuder</a>,&nbsp;<a href="https://www.bq.com/es/" target="_blank" style="max-height: 1e+06px; color: rgb(255, 255, 255); font-weight: bold; background-color: rgb(33, 150, 243); padding: 0px 5px; display: inline-block;">de BQ</a>, el funcionamiento de los botones capacitivos es más simple de lo que parece.</p><h2 style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; line-height: 35px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(245, 245, 245);">El panel táctil que cubre la pantalla es el encargado de la magia</h2><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);"><img class="alignnone size-full" src="https://elandroidelibre.elespanol.com/wp-content/uploads/2016/10/pantalla.png" width="1002" height="502" style="max-height: 1e+06px; max-width: 100%; width: 50%;"></p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);">Los botones táctiles&nbsp;situados en el frontal<strong style="max-height: 1e+06px;">&nbsp;no poseen un circuito propio ni sensores dedicados a capturar&nbsp;el toque del dedo</strong>:&nbsp;forman parte del panel digitalizador que cubre el frontal del smartphone. Dicho panel es el encargado de registrar los toques que realizamos sobre la pantalla&nbsp;transmitiéndolos al sistema en forma de eventos; que desembocarán en una acción concreta (ir al home, retroceso?).</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);">La pantalla de un smartphone es una especie de sándwich formado por varias capas. Está el cristal protector que tocamos con el dedo,&nbsp;una red invisible a modo de digitalizador que captura la posición y número de toques y luego está el propio display, ya sea LCD u OLED. Esta disposición&nbsp;<strong style="max-height: 1e+06px;">se traslada a toda la pantalla cubriendo la zona inferior</strong>&nbsp;en aquellos smartphones que poseen botones capacitivos.</p><div id="singlePostContentAdvertising" style="max-height: 1e+06px; width: 773.219px; float: left; margin: 0px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);"></div><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);">Como el digitalizador captura los toques supone un ahorro&nbsp;extenderlo hasta el final del frontal para dotar a los botones capacitivos&nbsp;de su reconocimiento&nbsp;táctil. El sistema identifica a la perfección&nbsp;los toques porque sabe qué zona concreta del frontal corresponde con los botones o con la propia pantalla. Mucho más barato que dotar a los botones de su propia circuitería&nbsp;e igual de efectivo.</p><h2 style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; line-height: 35px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(245, 245, 245);">¿Qué ocurre cuando fallan los botones capacitivos?</h2><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);"><img class="aligncenter wp-image-305309 size-full" title="Así funcionan los botones táctiles en el frontal de los móviles" src="https://elandroidelibre.elespanol.com/wp-content/uploads/2017/04/asus-zenfone-3-deluxe-2.jpg" alt="Así funcionan los botones táctiles en el frontal de los móviles" width="1050" height="592" srcset="https://elandroidelibre.elespanol.com/wp-content/uploads/2017/04/asus-zenfone-3-deluxe-2.jpg 1050w, https://elandroidelibre.elespanol.com/wp-content/uploads/2017/04/asus-zenfone-3-deluxe-2-450x254.jpg 450w, https://elandroidelibre.elespanol.com/wp-content/uploads/2017/04/asus-zenfone-3-deluxe-2-768x433.jpg 768w, https://elandroidelibre.elespanol.com/wp-content/uploads/2017/04/asus-zenfone-3-deluxe-2-750x423.jpg 750w" sizes="(max-width: 1050px) 100vw, 1050px" style="max-height: 1e+06px; max-width: 100%; height: auto; display: block; margin-left: auto; margin-right: auto; margin-bottom: 10px;"></p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);">Ya hemos visto que es el panel digitalizador el encargado de transformar las pulsaciones en eventos reconocibles por el sistema. Y este panel corresponde con la pantalla, luego&nbsp;<strong style="max-height: 1e+06px;">es imposible&nbsp;cambiar solo la zona de los botones si alguno deja de funcionar</strong>.</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);">Por lo general suele ser problema de software, no es demasiado común que una zona del digitalizador pierda la capacidad táctil. Así que, en&nbsp;este caso, hay que esperar a que el fabricante actualice el firmware corrigiendo el problema.</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);">Si por desgracia el fallo es físico, lo único que puedes hacer es acudir al fabricante y&nbsp;<strong style="max-height: 1e+06px;">hacer uso de la garantía</strong>. Es un problema de fabricación, por lo que deberían arreglártelo. Si no tuviera garantía la solución más viable sería cambiar la pantalla al completo, una reparación que suele tener un coste medio de 90 euros (dependiendo del modelo).</p><p style="max-height: 1e+06px; width: 757.75px; margin-left: 7.71875px; color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 17px; background-color: rgb(245, 245, 245);">Para paliar el problema de&nbsp;la falta de sensibilidad en los botones capacitivos también puedes optar por un parche:<strong style="max-height: 1e+06px;">&nbsp;colocar botones virtuales de navegación</strong>. De esta manera seguirás usando el móvil teniendo en pantalla el control necesario para administrarlo.</p>', 0, 1, '37'),
(20, '2017-05-09 16:20:00', 'asffafas f', 'asfafsafs', '<p>asf</p>', 0, 1, '37'),
(21, '2017-05-09 16:20:00', 'asfafs', 'fasf', '<p>afs</p>', 0, 1, '37'),
(22, '2017-05-09 16:20:00', 'asfas', 'asff', '<p>asffsa</p>', 0, 1, '37');

-- --------------------------------------------------------

--
-- Table structure for table `notas_fijas`
--

CREATE TABLE `notas_fijas` (
  `Id` int(11) NOT NULL,
  `nota` int(11) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notas_fijas`
--

INSERT INTO `notas_fijas` (`Id`, `nota`, `tipo`, `titulo`) VALUES
(1, 21, 'suplemento', 'guardado y editado con ubacion e imagen'),
(2, 6, 'noticia', 'Noticia principal prueba'),
(3, 11, 'nota', 'TItulo nota prueba');

-- --------------------------------------------------------

--
-- Table structure for table `notas_imagenes`
--

CREATE TABLE `notas_imagenes` (
  `Id` int(11) NOT NULL,
  `nota` int(11) DEFAULT NULL,
  `imagen` text,
  `predeterminar` tinyint(2) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notas_imagenes`
--

INSERT INTO `notas_imagenes` (`Id`, `nota`, `imagen`, `predeterminar`) VALUES
(2, 12, '5910a04c07e8b.jpg', 1),
(18, 13, '5910a6f7b1e7c.jpg', 1),
(19, 11, '5910a702d7b15.jpg', 1),
(20, 14, '5910aea297e64.jpg', 1),
(21, 15, '5910b6150d692.jpg', 1),
(22, 16, '5910c0c2728ce.jpg', 1),
(23, 17, '5910c1864a070.jpg', 1),
(26, 19, '5910eb9eee3fe.jpg', 1),
(31, 18, '5912165d7de15.jpg', 1),
(32, 20, '59121679bc5ca.jpg', 1),
(33, 21, '59121681cc4ea.jpg', 1),
(34, 22, '591216888ad97.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE `noticias` (
  `Id` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `categoria` int(11) DEFAULT NULL,
  `cuerpo` text,
  `audio` text,
  `video` text,
  `usuario` varchar(255) DEFAULT NULL,
  `visitas` int(11) DEFAULT '0',
  `publicar` tinyint(2) DEFAULT '1',
  `borrar` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`Id`, `fecha`, `titulo`, `categoria`, `cuerpo`, `audio`, `video`, `usuario`, `visitas`, `publicar`, `borrar`) VALUES
(1, '2017-05-05 23:54:00', 'asd asd asd', 1, '<p>asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das asd as das&nbsp;<br></p>', NULL, NULL, '29', 0, 1, 1),
(2, '2017-05-06 00:06:00', 'asd asddas ASd', 4, '<p>a a fasd fsda fAS D</p>', NULL, NULL, '29', 0, 1, 0),
(3, '2017-05-06 00:10:00', 'Noticia 3', 2, '<p>asdas dgsda gsd asd gasd gsadg&nbsp;</p>', NULL, NULL, '29', 6, 1, 0),
(4, '2017-05-06 00:11:00', 'Noticia 5', 5, '<p>asf ad sadg sadg&nbsp;</p>', NULL, NULL, '29', 0, 1, 0),
(5, '2017-05-06 00:13:00', 'asfsaasdg', 1, '<p>adsg asdg sad gsadg&nbsp;</p>', NULL, NULL, '29', 1, 1, 0),
(6, '2017-05-06 00:14:00', 'Noticia principal prueba', 4, '<p>da FA SAD FSDA&nbsp;</p>', NULL, NULL, '29', 6, 1, 1),
(7, '2017-05-01 00:43:00', 'Cuerpo largo editado', 5, '<p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Sed sed tincidunt lorem, eget ullamcorper felis. Aliquam mollis libero elementum eros commodo lobortis. Sed molestie tellus nec risus molestie fermentum. In hac habitasse platea dictumst. Aenean scelerisque tortor purus, et efficitur nisi tincidunt sed. Vivamus bibendum urna a risus luctus, id dapibus nunc vulputate. Quisque massa neque, consequat ac ligula vitae, varius bibendum lorem. Donec tristique dolor augue, ut semper justo pharetra in. Cras dignissim, est quis dapibus ultricies, eros libero ultrices neque, at ornare massa nisi non purus. Aenean nunc quam, commodo et nunc quis, dapibus suscipit est. Vivamus risus massa, laoreet non dignissim ut, lacinia eget quam. Morbi id mi eleifend, rutrum turpis eu, egestas risus. Pellentesque egestas, enim eu suscipit aliquet, purus metus rhoncus orci, euismod vehicula diam sapien et dolor. Nullam consequat condimentum tellus, a vulputate orci eleifend at. Suspendisse eleifend felis urna, ut scelerisque elit bibendum vel. Donec facilisis ac ipsum ac lobortis.</span></p><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Sed sed tincidunt lorem, eget ullamcorper felis. Aliquam mollis libero elementum eros commodo lobortis. Sed molestie tellus nec risus molestie fermentum. In hac habitasse platea dictumst. Aenean scelerisque tortor purus, et efficitur nisi tincidunt sed. Vivamus bibendum urna a risus luctus, id dapibus nunc vulputate. Quisque massa neque, consequat ac ligula vitae, varius bibendum lorem. Donec tristique dolor augue, ut semper justo pharetra in. Cras dignissim, est quis dapibus ultricies, eros libero ultrices neque, at ornare massa nisi non purus. Aenean nunc quam, commodo et nunc quis, dapibus suscipit est. Vivamus risus massa, laoreet non dignissim ut, lacinia eget quam. Morbi id mi eleifend, rutrum turpis eu, egestas risus. Pellentesque egestas, enim eu suscipit aliquet, purus metus rhoncus orci, euismod vehicula diam sapien et dolor. Nullam consequat condimentum tellus, a vulputate orci eleifend at. Suspendisse eleifend felis urna, ut scelerisque elit bibendum vel. Donec facilisis ac ipsum ac lobortis.</span></p><p style="text-align: justify; "><font face="Open Sans, Arial, sans-serif">asd</font></p><p style="text-align: justify; "><font face="Open Sans, Arial, sans-serif"><br></font></p><p style="text-align: justify; "><font face="Open Sans, Arial, sans-serif">asdasd</font></p><p style="text-align: justify; "><font face="Open Sans, Arial, sans-serif"><br></font></p><p style="text-align: justify; "><font face="Open Sans, Arial, sans-serif">asdasd</font></p><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><br></p>', NULL, NULL, '29', 9, 0, 0),
(8, '2017-05-06 00:56:00', 'x x x x x x x x', 1, '<p>af sdf asd fasd f</p>', 'undefined', '', '29', 1, 1, 0),
(9, '2017-05-06 00:57:00', 't t t t', 1, '<table><tbody><tr><td class="line-content">SELECT\r\n</td></tr><tr><td class="line-number" value="344"></td><td class="line-content">                                              n.*,\r\n</td></tr><tr><td class="line-number" value="345"></td><td class="line-content">                                              c.categoria,\r\n</td></tr><tr><td class="line-number" value="346"></td><td class="line-content">                                              DATE_FORMAT(n.fecha, ''%H:%i - %d/%m/%Y'') as fecha\r\n</td></tr><tr><td class="line-number" value="347"></td><td class="line-content">                                          FROM noticias n\r\n</td></tr><tr><td class="line-number" value="348"></td><td class="line-content">                                          JOIN categorias c ON c.Id = n.categoria\r\n</td></tr><tr><td class="line-number" value="349"></td><td class="line-content">                                          WHERE n.fecha &lt;= ''2017-05-06''\r\n</td></tr><tr><td class="line-number" value="350"></td><td class="line-content">                                          AND publicar = 1\r\n</td></tr><tr><td class="line-number" value="351"></td><td class="line-content">                                          AND n.borrar = 0\r\n</td></tr><tr><td class="line-number" value="352"></td><td class="line-content">                                          \r\n</td></tr><tr><td class="line-number" value="353"></td><td class="line-content">                                          ORDER BY n.fecha DESC&nbsp;</td></tr></tbody></table>', NULL, NULL, '29', 0, 1, 1),
(10, '2017-05-06 00:57:00', 'asf asf asf asf ', 1, '<p>as&nbsp;</p>', 'undefined', '', '29', 0, 1, 0),
(11, '2017-05-06 00:59:00', 'Noticia nueva principal', 1, '<p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">Los rumores sobre la llegada de un tercer sistema operativo de Google llevan tiempo produciéndose. Esa teórica mezcla de Android y de Chrome OS se llama Fuchsia, y hay una diferencia importante con esas dos plataformas: que&nbsp;<span style="font-weight: 600;">no estará basado en el kernel Linux</span>.</p><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;"><br></p><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">El nuevo kernel se llama Magenta y&nbsp;<span style="font-weight: 600;">eso permitirá a Google ''librarse'' de esa dependencia y de la GPL</span>. Google no esconde el desarrollo de Fuchsia aunque no ha confirmado públicamente la existencia de esta plataforma, y ahora aparecen nuevas imágenes de una interfaz que apunta maneras pero a la que le queda aún mucho recorrido.</p><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;"><br></p><h2 style="margin: 47.04px auto 19.04px; font-size: 32px; color: rgb(17, 17, 17); font-family: Tofino, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen, Ubuntu, Cantarell, &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, sans-serif; font-weight: 600; line-height: 40px; max-width: 696px;">La interfaz de usuario de Fuchsia tiene nombre: Armadillo</h2><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">Como indican en Ars technica, ese kernel llamado Magenta está orientado según la documentación del desarrollo a "teléfonos modernos y ordenadores modernos con procesadores rápidos, cantidades de RAM no triviales y periféricos de todo tipo en entornos de computación abierta". La definición no es especialmente concreta, pero&nbsp;<span style="font-weight: 600;">parece apuntar al futuro de móviles y tabletas convertibles</span>&nbsp;que ahora están gobernadas por Android.</p><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;"><br></p><div class="article-asset-image article-asset-normal" style="margin-left: auto; margin-right: auto; max-width: 696px; text-align: center; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; font-family: Tofino, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen, Ubuntu, Cantarell, &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, sans-serif; font-size: 16px; line-height: 28px; color: rgb(51, 51, 51);"><div class="asset-content" style="-webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; line-height: 28px; overflow: hidden;"><div class="caption-img  " style="color: rgb(136, 136, 136); line-height: 28px; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility;"><img class="centro_sinmarco" sf-srcset="https://i.blogs.es/a39c6e/fuchsialogo/450_1000.png 450w, https://i.blogs.es/a39c6e/fuchsialogo/650_1200.png 681w, https://i.blogs.es/a39c6e/fuchsialogo/1024_2000.png 1024w, https://i.blogs.es/a39c6e/fuchsialogo/1366_2000.png 1366w" sf-src="https://i.blogs.es/a39c6e/fuchsialogo/450_1000.png" alt="Fuchsialogo" srcset="https://i.blogs.es/a39c6e/fuchsialogo/450_1000.png 450w, https://i.blogs.es/a39c6e/fuchsialogo/650_1200.png 681w, https://i.blogs.es/a39c6e/fuchsialogo/1024_2000.png 1024w, https://i.blogs.es/a39c6e/fuchsialogo/1366_2000.png 1366w" src="https://i.blogs.es/a39c6e/fuchsialogo/450_1000.png" style="border-width: initial; border-style: none; display: block; width: 696px; max-width: 100%; height: auto; margin: 28px auto 0px;"><span style="font-size: 14px; line-height: 20px; color: rgb(119, 119, 119); display: block; border-bottom: 1px solid rgb(205, 225, 194); padding: 14px 0px; margin-bottom: 28px; text-align: left; max-width: 696px; margin-left: auto; margin-right: auto;">Este es el logotipo de Fuchsia</span></div></div></div><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">Uno de los últimos componentes en conocerse&nbsp;<span style="font-weight: 600;">es Armadillo, la interfaz de usuario de Fuchsia, que está basada en el SDK Flutter</span>&nbsp;que hace tiempo que muestra sus ventajas en desarrollos abiertos y que ahora también cobrará vida en la interfaz visual del sistema operativo de Google.</p><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">Es posible compilar el proyecto y lograr "ver cómo queda todo", pero&nbsp;<span style="font-weight: 600;">de momento la usabilidad de Fuchsia es muy limitada</span>. En su estado actual nos encontramos con una pantalla de inicio con un teclado, un botón de inicio y algo similar a un gestor de ventanas.</p><div class="article-asset-image article-asset-normal" style="margin-left: auto; margin-right: auto; max-width: 696px; text-align: center; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; font-family: Tofino, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen, Ubuntu, Cantarell, &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, sans-serif; font-size: 16px; line-height: 28px; color: rgb(51, 51, 51);"><div class="asset-content" style="-webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; line-height: 28px; overflow: hidden;"><img class="centro_sinmarco" sf-srcset="https://i.blogs.es/0fe3f9/fuchsia2/450_1000.jpg 450w, https://i.blogs.es/0fe3f9/fuchsia2/650_1200.jpg 681w, https://i.blogs.es/0fe3f9/fuchsia2/1024_2000.jpg 1024w, https://i.blogs.es/0fe3f9/fuchsia2/1366_2000.jpg 1366w" sf-src="https://i.blogs.es/0fe3f9/fuchsia2/450_1000.jpg" alt="Fuchsia2" srcset="https://i.blogs.es/0fe3f9/fuchsia2/450_1000.jpg 450w, https://i.blogs.es/0fe3f9/fuchsia2/650_1200.jpg 681w, https://i.blogs.es/0fe3f9/fuchsia2/1024_2000.jpg 1024w, https://i.blogs.es/0fe3f9/fuchsia2/1366_2000.jpg 1366w" src="https://i.blogs.es/0fe3f9/fuchsia2/450_1000.jpg" style="border-width: initial; border-style: none; display: block; width: 696px; max-width: 100%; height: auto; margin: 28px auto;"></div></div><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">En esa pantalla de inicio el comportamiento es el de una gran lista en la que hacemos scroll vertical, con algunos datos en el centro. La interfaz presenta&nbsp;<span style="font-weight: 600;">una lista de aplicaciones recientes en forma de tarjetas</span>&nbsp;?similares a las que presenta la multitarea actual en Android? mientras que debajo están una lista de sugerencias tipo "Google Now".</p><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">El funcionamiento, como indican quienes lo han probado, es aún errático y muy limitado, pero en Google se están tomando muy en serio este desarrollo, tal y como confirmaba uno de los responsables del desarrollo. "<span style="font-weight: 600;">No es un juguete ni un proyecto del 20% [del tiempo]</span>", comentaba.</p><div class="article-asset-image article-asset-normal" style="margin-left: auto; margin-right: auto; max-width: 696px; text-align: center; -webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; font-family: Tofino, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Oxygen, Ubuntu, Cantarell, &quot;Open Sans&quot;, &quot;Helvetica Neue&quot;, sans-serif; font-size: 16px; line-height: 28px; color: rgb(51, 51, 51);"><div class="asset-content" style="-webkit-font-smoothing: antialiased; text-rendering: optimizeLegibility; line-height: 28px; overflow: hidden;"><img class="centro_sinmarco" sf-srcset="https://i.blogs.es/d89d6a/fuchsia3/450_1000.png 450w, https://i.blogs.es/d89d6a/fuchsia3/650_1200.png 681w, https://i.blogs.es/d89d6a/fuchsia3/1024_2000.png 1024w, https://i.blogs.es/d89d6a/fuchsia3/1366_2000.png 1366w" sf-src="https://i.blogs.es/d89d6a/fuchsia3/450_1000.png" alt="Fuchsia3" srcset="https://i.blogs.es/d89d6a/fuchsia3/450_1000.png 450w, https://i.blogs.es/d89d6a/fuchsia3/650_1200.png 681w, https://i.blogs.es/d89d6a/fuchsia3/1024_2000.png 1024w, https://i.blogs.es/d89d6a/fuchsia3/1366_2000.png 1366w" src="https://i.blogs.es/d89d6a/fuchsia3/450_1000.png" style="border-width: initial; border-style: none; display: block; width: 696px; max-width: 100%; height: auto; margin: 28px auto;"></div></div><p style="margin-right: auto; margin-bottom: 19.04px; margin-left: auto; font-size: 21px; max-width: 696px; color: rgb(51, 51, 51); font-family: Charter, Georgia, serif;">Todo apunta a que Fuchsia es algo así como&nbsp;<span style="font-weight: 600;">un Android rediseñado desde cero para coger lo mejor que tiene esta plataforma</span>&nbsp;pero aprovechando ideas que no podrían implementarse (al menos, no fácilmente) allí. Puede que Google comience a hablar por fin de este proyecto en Google I/O, pero lo que parece estar claro es que aún queda bastante tiempo para que Fuchsia pueda competir con sus hermanos mayores.</p>', NULL, NULL, '29', 4, 0, 0),
(12, '2017-05-06 01:00:00', 'gasd gsad ags', 5, '<p>a sf asf&nbsp;</p>', NULL, NULL, '29', 0, 1, 1),
(13, '2017-05-06 00:52:00', 'as fasf as', 1, '<p>ds asd fsda&nbsp;</p>', NULL, NULL, '29', 3, 1, 0),
(14, '2017-05-06 01:07:00', 'iuiuiu', 1, '<p>sd fasd fsda&nbsp;</p>', NULL, NULL, '29', 3, 1, 1),
(15, '2017-05-08 17:15:00', 'Nueva nota principal parrafos', 1, '<p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Aliquam elit tortor, faucibus sed pulvinar at, pharetra sit amet neque. Sed et tristique purus. Curabitur luctus dolor quis augue venenatis, id finibus dolor vehicula. Duis fringilla dolor eu metus malesuada, vitae lacinia est rutrum. Praesent vitae enim et risus varius convallis. Sed vel nulla diam. Sed quis consequat turpis. Ut fringilla orci nec tortor pretium, sed molestie urna vehicula. Nunc ornare, ex sed luctus interdum, elit ante ultrices tellus, eget viverra enim purus a neque. Aliquam vel nisl et diam facilisis vehicula. Vestibulum pulvinar elit enim, non dapibus turpis dapibus quis.</span></p><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Aliquam elit tortor, faucibus sed pulvinar at, pharetra sit amet neque. Sed et tristique purus. Curabitur luctus dolor quis augue venenatis, id finibus dolor vehicula. Duis fringilla dolor eu metus malesuada, vitae lacinia est rutrum. Praesent vitae enim et risus varius convallis. Sed vel nulla diam. Sed quis consequat turpis. Ut fringilla orci nec tortor pretium, sed molestie urna vehicula. Nunc ornare, ex sed luctus interdum, elit ante ultrices tellus, eget viverra enim purus a neque. Aliquam vel nisl et diam facilisis vehicula. Vestibulum pulvinar elit enim, non dapibus turpis dapibus quis.</span></p><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Aliquam elit tortor, faucibus sed pulvinar at, pharetra sit amet neque. Sed et tristique purus. Curabitur luctus dolor quis augue venenatis, id finibus dolor vehicula. Duis fringilla dolor eu metus malesuada, vitae lacinia est rutrum. Praesent vitae enim et risus varius convallis. Sed vel nulla diam. Sed quis consequat turpis. Ut fringilla orci nec tortor pretium, sed molestie urna vehicula. Nunc ornare, ex sed luctus interdum, elit ante ultrices tellus, eget viverra enim purus a neque. Aliquam vel nisl et diam facilisis vehicula. Vestibulum pulvinar elit enim, non dapibus turpis dapibus quis.</span></p><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Aliquam elit tortor, faucibus sed pulvinar at, pharetra sit amet neque. Sed et tristique purus. Curabitur luctus dolor quis augue venenatis, id finibus dolor vehicula. Duis fringilla dolor eu metus malesuada, vitae lacinia est rutrum. Praesent vitae enim et risus varius convallis. Sed vel nulla diam. Sed quis consequat turpis. Ut fringilla orci nec tortor pretium, sed molestie urna vehicula. Nunc ornare, ex sed luctus interdum, elit ante ultrices tellus, eget viverra enim purus a neque. Aliquam vel nisl et diam facilisis vehicula. Vestibulum pulvinar elit enim, non dapibus turpis dapibus quis.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Aliquam elit tortor, faucibus sed pulvinar at, pharetra sit amet neque. Sed et tristique purus. Curabitur luctus dolor quis augue venenatis, id finibus dolor vehicula. Duis fringilla dolor eu metus malesuada, vitae lacinia est rutrum. Praesent vitae enim et risus varius convallis. Sed vel nulla diam. Sed quis consequat turpis. Ut fringilla orci nec tortor pretium, sed molestie urna vehicula. Nunc ornare, ex sed luctus interdum, elit ante ultrices tellus, eget viverra enim purus a neque. Aliquam vel nisl et diam facilisis vehicula. Vestibulum pulvinar elit enim, non dapibus turpis dapibus quis.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Aliquam elit tortor, faucibus sed pulvinar at, pharetra sit amet neque. Sed et tristique purus. Curabitur luctus dolor quis augue venenatis, id finibus dolor vehicula. Duis fringilla dolor eu metus malesuada, vitae lacinia est rutrum. Praesent vitae enim et risus varius convallis. Sed vel nulla diam. Sed quis consequat turpis. Ut fringilla orci nec tortor pretium, sed molestie urna vehicula. Nunc ornare, ex sed luctus interdum, elit ante ultrices tellus, eget viverra enim purus a neque. Aliquam vel nisl et diam facilisis vehicula. Vestibulum pulvinar elit enim, non dapibus turpis dapibus quis.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Aliquam elit tortor, faucibus sed pulvinar at, pharetra sit amet neque. Sed et tristique purus. Curabitur luctus dolor quis augue venenatis, id finibus dolor vehicula. Duis fringilla dolor eu metus malesuada, vitae lacinia est rutrum. Praesent vitae enim et risus varius convallis. Sed vel nulla diam. Sed quis consequat turpis. Ut fringilla orci nec tortor pretium, sed molestie urna vehicula. Nunc ornare, ex sed luctus interdum, elit ante ultrices tellus, eget viverra enim purus a neque. Aliquam vel nisl et diam facilisis vehicula. Vestibulum pulvinar elit enim, non dapibus turpis dapibus quis.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Aliquam elit tortor, faucibus sed pulvinar at, pharetra sit amet neque. Sed et tristique purus. Curabitur luctus dolor quis augue venenatis, id finibus dolor vehicula. Duis fringilla dolor eu metus malesuada, vitae lacinia est rutrum. Praesent vitae enim et risus varius convallis. Sed vel nulla diam. Sed quis consequat turpis. Ut fringilla orci nec tortor pretium, sed molestie urna vehicula. Nunc ornare, ex sed luctus interdum, elit ante ultrices tellus, eget viverra enim purus a neque. Aliquam vel nisl et diam facilisis vehicula. Vestibulum pulvinar elit enim, non dapibus turpis dapibus quis.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Aliquam elit tortor, faucibus sed pulvinar at, pharetra sit amet neque. Sed et tristique purus. Curabitur luctus dolor quis augue venenatis, id finibus dolor vehicula. Duis fringilla dolor eu metus malesuada, vitae lacinia est rutrum. Praesent vitae enim et risus varius convallis. Sed vel nulla diam. Sed quis consequat turpis. Ut fringilla orci nec tortor pretium, sed molestie urna vehicula. Nunc ornare, ex sed luctus interdum, elit ante ultrices tellus, eget viverra enim purus a neque. Aliquam vel nisl et diam facilisis vehicula. Vestibulum pulvinar elit enim, non dapibus turpis dapibus quis.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><br></p>', NULL, NULL, '29', 7, 1, 0),
(16, '2017-05-09 11:35:00', 'x x x x ', 1, '<p>as fasf</p>', '02 - Las Pastillas Del Abuelo - Amar Y Envejecer (Acústico).Mp3', 'video', '29', 0, 0, 0),
(17, '2017-05-09 11:35:00', 'x x x x  x aaaa', 1, '<p>as fasf</p>', '02 - Las Pastillas Del Abuelo - Amar Y Envejecer (Acústico).Mp3', 'video', '29', 0, 1, 0),
(18, '2017-05-09 11:35:00', 'baba', 1, '<p>as fasf</p>', '02 - Las Pastillas Del Abuelo - Amar Y Envejecer (Acústico).Mp3', 'video', '29', 0, 1, 0),
(19, '2017-05-09 11:35:00', 'babax', 1, '<p>as fasf</p>', '02 - Las Pastillas Del Abuelo - Amar Y Envejecer (Acústico).Mp3', 'video', '29', 0, 1, 0),
(20, '2017-05-09 11:35:00', 'babaxs', 1, '<p>as fasf</p>', '02 - Las Pastillas Del Abuelo - Amar Y Envejecer (Acústico).Mp3', 'video', '29', 0, 0, 0),
(21, '2017-05-09 11:35:00', 'babaxss', 1, '<p>as fasf</p>', '02 - Las Pastillas Del Abuelo - Amar Y Envejecer (Acústico).Mp3', 'video', '29', 0, 1, 0),
(22, '2017-05-09 15:56:00', 'babaxss', 1, '<p>as fasf</p>', 'undefined', '', '29', 0, 1, 0),
(23, '2017-05-09 15:57:00', 'babaxss aaaa', 1, '<p>as fasf</p>', 'undefined', '', '29', 1, 1, 0),
(24, '2017-05-09 15:57:00', 'babaxss aaaa nenene', 1, '<p>as fasf</p>', 'undefined', '', '29', 0, 1, 0),
(25, '2017-05-09 15:57:00', 'babaxss aaaa nenene ve', 1, '<p>as fasf</p>', 'undefined', '', '29', 0, 1, 0),
(26, '2017-05-09 15:55:00', 'as dfasf', 1, '<p>as das das d</p>', 'undefined', '', '29', 0, 1, 0),
(27, '2017-05-09 11:58:00', 'asdf asf asf ', 1, '<p>as fasf&nbsp;</p>', 'undefined', 'as fas fasf ', '29', 0, 0, 0),
(28, '2017-05-09 15:55:00', 'as faasf', 1, '<p>asf as f</p>', 'undefined', 'asfasfasf', '29', 0, 1, 0),
(29, '2017-05-09 19:38:00', 'asd fsad fsda f', 1, '<p>sda fsa sd&nbsp;</p>', 'undefined', 'sadfsdaf', '37', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `noticias_imagenes`
--

CREATE TABLE `noticias_imagenes` (
  `Id` int(11) NOT NULL,
  `noticia` int(11) DEFAULT NULL,
  `imagen` text,
  `epigrafe` varchar(255) DEFAULT NULL,
  `predeterminar` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `noticias_imagenes`
--

INSERT INTO `noticias_imagenes` (`Id`, `noticia`, `imagen`, `epigrafe`, `predeterminar`) VALUES
(1, 1, '3.PNG', NULL, 0),
(2, 1, '2.PNG', NULL, 0),
(3, 1, '4.PNG', NULL, 0),
(4, 1, '5.PNG', NULL, 0),
(5, 1, '6.PNG', NULL, 0),
(6, 1, '7.PNG', NULL, 0),
(7, 1, '9.PNG', NULL, 0),
(8, 1, '8.PNG', NULL, 0),
(9, 1, '10.PNG', NULL, 0),
(10, 1, '11.PNG', NULL, 0),
(11, 1, '13.PNG', NULL, 0),
(12, 1, '12.PNG', NULL, 0),
(13, 1, '14.PNG', NULL, 0),
(14, 1, '15.PNG', NULL, 0),
(15, 1, '16.PNG', NULL, 0),
(16, 1, '17.PNG', NULL, 0),
(17, 2, '10.PNG', NULL, 0),
(18, 2, '9.PNG', NULL, 0),
(19, 2, '11.PNG', NULL, 0),
(20, 2, '12.PNG', NULL, 0),
(21, 2, '13.PNG', NULL, 0),
(22, 2, '14.PNG', NULL, 0),
(23, 2, '16.PNG', NULL, 0),
(24, 2, '15.PNG', NULL, 0),
(25, 2, '17.PNG', NULL, 0),
(26, 3, '16.PNG', NULL, 0),
(27, 3, '17.PNG', NULL, 0),
(28, 3, '19.PNG', NULL, 0),
(29, 3, '18.PNG', NULL, 0),
(30, 4, '18.PNG', NULL, 0),
(31, 4, '17.PNG', NULL, 0),
(32, 4, '19.PNG', NULL, 0),
(33, 5, '17.PNG', NULL, 1),
(34, 5, '16.PNG', NULL, 0),
(35, 5, '18.PNG', NULL, 0),
(36, 6, 'slide-img-1.jpg', NULL, 0),
(37, 6, '3CA.jpg', NULL, 0),
(38, 6, 'seba.PNG', NULL, 0),
(39, 6, 'Nico.PNG', NULL, 0),
(40, 6, 'sec-3.jpg', NULL, 0),
(41, 6, 'D32.jpg', NULL, 0),
(42, 6, '5B6.jpg', NULL, 1),
(43, 6, '555.jpg', NULL, 0),
(65, 9, 'the_mountains_of_scotland-wallpaper-3840x1600.jpg', '', 0),
(66, 9, 'HNszoYe.jpg', '', 0),
(67, 9, 'jVsXW3B.jpg', '', 0),
(73, 12, 'kqVYD4x.jpg', 'asddas', 0),
(74, 12, 'HGLDPL0.jpg', 'asf', 0),
(75, 12, 'bB8BIam.jpg', 'as fas f', 0),
(76, 12, '5V7zOfa.jpg', 'as fas fasf', 0),
(77, 13, '6ogww0K.jpg', '', 0),
(78, 13, 'ltrnMib.jpg', '', 0),
(79, 13, 'KDihNdo.jpg', '', 0),
(80, 13, 'BkRL3o4.jpg', '', 0),
(81, 13, 'RfDLZgP.jpg', '', 0),
(82, 13, '9vWwUDY.jpg', '', 0),
(83, 13, '8JbWCF0.jpg', '', 0),
(84, 13, 'We0xMsE.jpg', '', 0),
(85, 14, 'qLmaL6X.png', '44242', 0),
(86, 14, 'ZKa9jm1.jpg', '', 0),
(87, 14, 'xYnqbAL.jpg', '', 0),
(88, 14, 'bNinOXk.jpg', '', 0),
(89, 14, 'kitty_12-wallpaper-1440x900.jpg', '1234', 1),
(147, 7, 'HGLDPL0.jpg', '2', 0),
(148, 7, 'the_mountains_of_scotland-wallpaper-3840x1600.jpg', '1', 1),
(163, 15, '2.PNG', '', 0),
(164, 15, '3.PNG', '', 0),
(165, 15, '4.PNG', '', 0),
(166, 15, '5.PNG', '', 0),
(167, 11, 'mp0Vxyv.png', '', 0),
(168, 16, '004.jpg', '', 0),
(169, 17, '004.jpg', '', 0),
(170, 18, '004.jpg', '', 0),
(171, 19, '004.jpg', '', 0),
(172, 20, '004.jpg', '', 0),
(173, 21, '004.jpg', '', 0),
(175, 23, '004.jpg', '', 0),
(176, 24, '004.jpg', '', 0),
(177, 25, '004.jpg', '', 0),
(178, 28, '01D.jpg', '', 0),
(179, 22, '004.jpg', '', 0),
(180, 22, '003.jpg', '', 1),
(181, 22, '01D.jpg', '', 0),
(182, 22, '019.jpg', '', 0),
(183, 10, 'spacecat_2-wallpaper-1920x1080.jpg', '', 0),
(184, 10, 'LLHbUfD.jpg', '', 1),
(185, 8, 'kqVYD4x.jpg', 'ep', 0),
(186, 8, '5V7zOfa.jpg', 'igrafe', 1),
(187, 29, '003.jpg', 'aaa', 0);

-- --------------------------------------------------------

--
-- Table structure for table `noticias_secciones`
--

CREATE TABLE `noticias_secciones` (
  `Id` int(11) NOT NULL,
  `noticia` int(11) NOT NULL,
  `seccion` int(11) DEFAULT NULL,
  `suplemento` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `noticias_secciones`
--

INSERT INTO `noticias_secciones` (`Id`, `noticia`, `seccion`, `suplemento`) VALUES
(1, 3, 1, 0),
(2, 29, 2, 0),
(3, 26, 3, 0),
(4, 26, 1, 5),
(5, 21, 2, 5),
(6, 25, 3, 5),
(7, 17, 1, 9),
(8, 38, 2, 9),
(9, 16, 3, 9),
(10, 11, 1, 3),
(11, 37, 2, 3),
(12, 37, 3, 3),
(13, 0, 1, 4),
(14, 0, 2, 4),
(15, 0, 3, 4),
(16, 0, 1, 6),
(17, 0, 2, 6),
(18, 0, 3, 6),
(19, 0, 1, 7),
(20, 0, 2, 7),
(21, 0, 3, 7),
(22, 0, 1, 8),
(23, 0, 2, 8),
(24, 0, 3, 8),
(25, 0, 1, 10),
(26, 0, 2, 10),
(27, 0, 3, 10);

-- --------------------------------------------------------

--
-- Table structure for table `publicidades`
--

CREATE TABLE `publicidades` (
  `Id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `archivo` text,
  `borrar` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `publicidades`
--

INSERT INTO `publicidades` (`Id`, `nombre`, `archivo`, `borrar`) VALUES
(1, 'Central 850x150 px', '01.jpg', 0),
(2, 'Suplementos especiales', 'anuncios.jpg', 0),
(3, 'Lateral superior 243x300 px', 'publicidad02.jpg', 0),
(4, 'Lateral inferior 243x300px', 'defensorDib.gif', 0),
(5, 'Inferior izquierda 415x120px', 'publicidad03.jpg', 0),
(6, 'Inferior derecha 415x120 px', 'publicidad03.jpg', 0),
(7, 'Popup', 'publicidad03.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `secciones`
--

CREATE TABLE `secciones` (
  `Id` int(11) NOT NULL,
  `seccion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `secciones`
--

INSERT INTO `secciones` (`Id`, `seccion`) VALUES
(1, 'Principal'),
(2, 'Lateral superior'),
(3, 'Lateral inferior');

-- --------------------------------------------------------

--
-- Table structure for table `suplementos`
--

CREATE TABLE `suplementos` (
  `Id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `categoria` int(11) DEFAULT NULL,
  `cuerpo` text,
  `usuario` varchar(255) DEFAULT NULL,
  `borrar` tinyint(2) DEFAULT '0',
  `publicar` tinyint(2) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suplementos`
--

INSERT INTO `suplementos` (`Id`, `fecha`, `titulo`, `categoria`, `cuerpo`, `usuario`, `borrar`, `publicar`) VALUES
(5, '2017-05-03', 'afsasadf', 5, '<p>sdf sadf sad f</p>', 'admin', 1, 1),
(6, '2017-05-03', 'Suplemento', 6, '<p>sadf asd sad f</p>', 'admin', 1, 1),
(7, '2017-05-03', 'Suplemento de tranquera', 3, '<p>as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa as das dsa&nbsp;<br></p>', 'admin', 1, 0),
(8, '2017-05-03', 'a ssdg sda g', 9, '<p>sda sd</p>', 'admin', 1, 0),
(9, '2017-05-03', 'aaaaaaaaaaaaa a a a a a a', 3, '<p>sdg asdg sda sdag&nbsp;</p>', 'admin', 0, 0),
(10, '2017-05-03', 'suplemento de tranqyera kajdñlasm, dgasd ', 8, '<p>sadf asd fsad f</p>', 'admin', 0, 0),
(11, '2017-05-03', 'Tranquera loca', 3, '<p>a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad a fasd sad fsad&nbsp;<br></p>', 'admin', 0, 0),
(13, '2017-05-04', 'asdsdasd', 9, '<p>dfsg dsfg df gdfsg&nbsp;</p>', '29', 0, 1),
(14, '2017-05-04', 'asdsdasda', 9, '<p>dfsg dsfg df gdfsg&nbsp;</p>', '29', 0, 1),
(16, '2017-05-04', 'ffasfasf', 9, '<p>s s dfsg sdfg s s dfsg sdfg s s dfsg sdfg s s dfsg sdfg&nbsp;<br></p>', '29', 0, 1),
(17, '2017-05-04', 'z z z z z', 9, '<p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nunc rutrum ipsum vitae est fermentum, ut luctus sem gravida. Donec mollis augue felis, in aliquam orci maximus in. Proin neque justo, venenatis ut ultricies vel, volutpat et ante. Mauris bibendum at purus id suscipit. Aliquam sit amet placerat odio. Nam accumsan sit amet arcu et hendrerit. Sed nec suscipit ante, at viverra lorem. Nunc accumsan urna quis sapien ullamcorper pellentesque. Nunc at molestie ex, non dictum neque. Integer malesuada dolor bibendum pharetra mollis. Nam vehicula vehicula molestie. Nunc in tempor orci. Nunc nunc ligula, posuere vitae sapien eget, accumsan congue nunc. Nulla tempus varius nulla, eget elementum nisl sodales nec. Morbi nibh sapien, pharetra nec fermentum eget, accumsan nec est. In hac habitasse platea dictumst.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nunc rutrum ipsum vitae est fermentum, ut luctus sem gravida. Donec mollis augue felis, in aliquam orci maximus in. Proin neque justo, venenatis ut ultricies vel, volutpat et ante. Mauris bibendum at purus id suscipit. Aliquam sit amet placerat odio. Nam accumsan sit amet arcu et hendrerit. Sed nec suscipit ante, at viverra lorem. Nunc accumsan urna quis sapien ullamcorper pellentesque. Nunc at molestie ex, non dictum neque. Integer malesuada dolor bibendum pharetra mollis. Nam vehicula vehicula molestie. Nunc in tempor orci. Nunc nunc ligula, posuere vitae sapien eget, accumsan congue nunc. Nulla tempus varius nulla, eget elementum nisl sodales nec. Morbi nibh sapien, pharetra nec fermentum eget, accumsan nec est. In hac habitasse platea dictumst.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nunc rutrum ipsum vitae est fermentum, ut luctus sem gravida. Donec mollis augue felis, in aliquam orci maximus in. Proin neque justo, venenatis ut ultricies vel, volutpat et ante. Mauris bibendum at purus id suscipit. Aliquam sit amet placerat odio. Nam accumsan sit amet arcu et hendrerit. Sed nec suscipit ante, at viverra lorem. Nunc accumsan urna quis sapien ullamcorper pellentesque. Nunc at molestie ex, non dictum neque. Integer malesuada dolor bibendum pharetra mollis. Nam vehicula vehicula molestie. Nunc in tempor orci. Nunc nunc ligula, posuere vitae sapien eget, accumsan congue nunc. Nulla tempus varius nulla, eget elementum nisl sodales nec. Morbi nibh sapien, pharetra nec fermentum eget, accumsan nec est. In hac habitasse platea dictumst.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nunc rutrum ipsum vitae est fermentum, ut luctus sem gravida. Donec mollis augue felis, in aliquam orci maximus in. Proin neque justo, venenatis ut ultricies vel, volutpat et ante. Mauris bibendum at purus id suscipit. Aliquam sit amet placerat odio. Nam accumsan sit amet arcu et hendrerit. Sed nec suscipit ante, at viverra lorem. Nunc accumsan urna quis sapien ullamcorper pellentesque. Nunc at molestie ex, non dictum neque. Integer malesuada dolor bibendum pharetra mollis. Nam vehicula vehicula molestie. Nunc in tempor orci. Nunc nunc ligula, posuere vitae sapien eget, accumsan congue nunc. Nulla tempus varius nulla, eget elementum nisl sodales nec. Morbi nibh sapien, pharetra nec fermentum eget, accumsan nec est. In hac habitasse platea dictumst.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nunc rutrum ipsum vitae est fermentum, ut luctus sem gravida. Donec mollis augue felis, in aliquam orci maximus in. Proin neque justo, venenatis ut ultricies vel, volutpat et ante. Mauris bibendum at purus id suscipit. Aliquam sit amet placerat odio. Nam accumsan sit amet arcu et hendrerit. Sed nec suscipit ante, at viverra lorem. Nunc accumsan urna quis sapien ullamcorper pellentesque. Nunc at molestie ex, non dictum neque. Integer malesuada dolor bibendum pharetra mollis. Nam vehicula vehicula molestie. Nunc in tempor orci. Nunc nunc ligula, posuere vitae sapien eget, accumsan congue nunc. Nulla tempus varius nulla, eget elementum nisl sodales nec. Morbi nibh sapien, pharetra nec fermentum eget, accumsan nec est. In hac habitasse platea dictumst.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nunc rutrum ipsum vitae est fermentum, ut luctus sem gravida. Donec mollis augue felis, in aliquam orci maximus in. Proin neque justo, venenatis ut ultricies vel, volutpat et ante. Mauris bibendum at purus id suscipit. Aliquam sit amet placerat odio. Nam accumsan sit amet arcu et hendrerit. Sed nec suscipit ante, at viverra lorem. Nunc accumsan urna quis sapien ullamcorper pellentesque. Nunc at molestie ex, non dictum neque. Integer malesuada dolor bibendum pharetra mollis. Nam vehicula vehicula molestie. Nunc in tempor orci. Nunc nunc ligula, posuere vitae sapien eget, accumsan congue nunc. Nulla tempus varius nulla, eget elementum nisl sodales nec. Morbi nibh sapien, pharetra nec fermentum eget, accumsan nec est. In hac habitasse platea dictumst.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><br></p>', '29', 0, 1),
(18, '2017-05-04', 'xxx', 9, '<p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nam faucibus maximus orci quis dictum. Pellentesque pharetra tincidunt vulputate. Proin consequat metus eget leo tincidunt, at imperdiet erat pharetra. Morbi mollis urna quis ante pellentesque egestas. Ut consectetur sapien sapien, ut feugiat ligula auctor ut. Suspendisse tincidunt in magna fringilla mollis. Morbi non tincidunt augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ante elit, lobortis ac nibh pretium, pellentesque egestas mauris. Nulla non est est.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nam faucibus maximus orci quis dictum. Pellentesque pharetra tincidunt vulputate. Proin consequat metus eget leo tincidunt, at imperdiet erat pharetra. Morbi mollis urna quis ante pellentesque egestas. Ut consectetur sapien sapien, ut feugiat ligula auctor ut. Suspendisse tincidunt in magna fringilla mollis. Morbi non tincidunt augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ante elit, lobortis ac nibh pretium, pellentesque egestas mauris. Nulla non est est.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nam faucibus maximus orci quis dictum. Pellentesque pharetra tincidunt vulputate. Proin consequat metus eget leo tincidunt, at imperdiet erat pharetra. Morbi mollis urna quis ante pellentesque egestas. Ut consectetur sapien sapien, ut feugiat ligula auctor ut. Suspendisse tincidunt in magna fringilla mollis. Morbi non tincidunt augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ante elit, lobortis ac nibh pretium, pellentesque egestas mauris. Nulla non est est.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nam faucibus maximus orci quis dictum. Pellentesque pharetra tincidunt vulputate. Proin consequat metus eget leo tincidunt, at imperdiet erat pharetra. Morbi mollis urna quis ante pellentesque egestas. Ut consectetur sapien sapien, ut feugiat ligula auctor ut. Suspendisse tincidunt in magna fringilla mollis. Morbi non tincidunt augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ante elit, lobortis ac nibh pretium, pellentesque egestas mauris. Nulla non est est.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nam faucibus maximus orci quis dictum. Pellentesque pharetra tincidunt vulputate. Proin consequat metus eget leo tincidunt, at imperdiet erat pharetra. Morbi mollis urna quis ante pellentesque egestas. Ut consectetur sapien sapien, ut feugiat ligula auctor ut. Suspendisse tincidunt in magna fringilla mollis. Morbi non tincidunt augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ante elit, lobortis ac nibh pretium, pellentesque egestas mauris. Nulla non est est.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;">Nam faucibus maximus orci quis dictum. Pellentesque pharetra tincidunt vulputate. Proin consequat metus eget leo tincidunt, at imperdiet erat pharetra. Morbi mollis urna quis ante pellentesque egestas. Ut consectetur sapien sapien, ut feugiat ligula auctor ut. Suspendisse tincidunt in magna fringilla mollis. Morbi non tincidunt augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ante elit, lobortis ac nibh pretium, pellentesque egestas mauris. Nulla non est est.</span></p><hr><p><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><span style="font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;"><br></span><br></p>', '29', 0, 1),
(19, '2017-05-06', 'ytytyt', 4, '<p>a gasg sd dsag</p>', '29', 0, 0),
(20, '2017-05-08', 'x a x a x a xa ', 9, '<p>as fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as f</p><p>as fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as fas fas as f</p><hr><p>as fas as f</p><hr><p>as fas as f</p><hr><p>as fas as f</p><hr><p>as fas as f</p><hr><p>as fas as f</p><hr><p><br></p>', '29', 0, 1),
(21, '2017-05-09', 'guardado y editado con ubacion e imagen', 5, '<p>a x a</p><p>xxxxxxxxxxxxxxxxxxxxxxxxxxxx</p>', '27', 0, 1),
(22, '0000-00-00', 'Guardado nuevo y editado', 5, '<p>a sdg asd g</p><p>aa</p><p>aaa</p><p>xxx</p>', '29', 0, 1),
(23, '2017-05-08', 'Publicado', 8, '<p>gas sdag sad gs g</p>', '29', 0, 1),
(24, '2017-05-08', 'titulo con seccion', 9, '<p><br></p><hr><p>as das das&nbsp;</p><hr><p>as das das&nbsp;</p><hr><p>as das das&nbsp;</p><hr><p>as das das&nbsp;</p><hr><p>as das das&nbsp;</p><hr><p>as das das&nbsp;</p><hr><p>as das das&nbsp;</p><hr><p><br></p>', '29', 0, 1),
(25, '2017-05-08', 'Prueba seccion otra', 5, '<p>as dasd as das das d<br>asdas d s</p><hr><p>asasfas</p><hr><p>asfasfsf</p><hr><p>asasfsafasf</p>', '27', 0, 1),
(26, '2017-05-08', 'aaaaaaaaaaaaaaaaaaaa a a a', 5, '<p>sad fs fasd&nbsp;</p><p>sadf asd f</p><p>sdaf asd f</p><p>sda fasd f</p>', '29', 0, 1),
(27, '2017-05-08', 'Guardado nuevo', 5, '<p>a sdg asd g</p>', '29', 0, 0),
(28, '2017-05-08', 'guardado', 5, '<p>sd asafd&nbsp;<br>ffdf<br>fasfaf</p><p>aasfasf</p><p>asfasfasf</p><p>asasf</p>', '29', 0, 1),
(29, '2017-05-08', 'guardado', 5, '<p>sd asafd&nbsp;<br>ffdf<br>fasfaf</p><p>aasfasf</p><p>asfasfasf</p><p>asasf</p>', '29', 0, 1),
(30, '2017-05-08', 'guardado', 5, '<p>sd asafd&nbsp;<br>ffdf<br>fasfaf</p><p>aasfasf</p><p>asfasfasf</p><p>asasf</p>', '29', 0, 1),
(31, '2017-05-08', 'guardado', 5, '<p>sd asafd&nbsp;<br>ffdf<br>fasfaf</p><p>aasfasf</p><p>asfasfasf</p><p>asasf</p>', '29', 0, 1),
(32, '2017-05-08', 'guardado', 5, '<p>sd asafd&nbsp;<br>ffdf<br>fasfaf</p><p>aasfasf</p><p>asfasfasf</p><p>asasf</p>', '29', 0, 1),
(33, '2017-05-08', 'guardado', 5, '<p>sd asafd&nbsp;<br>ffdf<br>fasfaf</p><p>aasfasf</p><p>asfasfasf</p><p>asasf</p>', '29', 0, 1),
(34, '2017-05-08', 'guardado', 5, '<p>sd asafd&nbsp;<br>ffdf<br>fasfaf</p><p>aasfasf</p><p>asfasfasf</p><p>asasf</p>', '29', 0, 1),
(35, '2017-05-08', 'guardado', 5, '<p>a</p>', '29', 0, 1),
(36, '2017-05-08', 'guardado y editado con ubacion e imagen', 5, '<p>sd asafd&nbsp;<br>asd&nbsp;</p><p>asdasd</p><p>asgsladghoasdg</p><p>ñksjadglksadg</p>', '29', 0, 1),
(37, '2017-05-08', 'Tranquera principal', 3, '<h2 style="font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(52, 52, 52); margin-top: 10px; font-size: 25px; background-color: rgb(253, 253, 253); border-radius: 0px !important;">Nuevo ring en el que el chavismo y la oposición buscan dar el golpe de nocaut</h2><p style="color: rgb(58, 61, 63); font-size: 15px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;"><span class="letter-badge" style="border-radius: 0px !important;">L</span>orem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.&nbsp;<a href="http://localhost/Code/dib_base/nota-suplemento.php#" style="background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(66, 139, 202); border-radius: 0px !important;">Vivamus elementum semper</a>&nbsp;nisi. Aenean vulputate eleifend tellus.&nbsp;<br style="margin: 5px 0px; border-radius: 0px !important;">Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.Etiam ultricies&gt;nisi vel augue. Curabitur ullamcorper ultricies nisi.</p><blockquote style="border-color: rgb(65, 163, 231); color: rgb(141, 154, 165); font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;"><p style="color: rgb(65, 163, 231); font-size: 15px; border-radius: 0px !important;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p></blockquote><p style="color: rgb(58, 61, 63); font-size: 15px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;">Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.</p><ul class="icn-list" style="color: rgb(141, 154, 165); font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;"><li style="border-radius: 0px !important;">Lorem ipsum dolor sit amet</li><li style="border-radius: 0px !important;">Consectetur adipiscing elit</li><li style="border-radius: 0px !important;">Integer molestie lorem at massa</li><li style="border-radius: 0px !important;">Facilisis in pretium nisl aliquet</li><li style="border-radius: 0px !important;">Nulla volutpat aliquam velit</li></ul><p><br style="margin: 5px 0px; color: rgb(141, 154, 165); font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;"><img src="http://localhost/Code/dib_base/images/sec/sec-3.jpg" class="img-thumbnail" style="border-color: rgb(237, 237, 237); line-height: 1.42857; background-color: rgb(248, 248, 248); color: rgb(141, 154, 165); font-family: &quot;Open Sans&quot;, sans-serif;"><span style="color: rgb(141, 154, 165); font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253);"></span><br style="margin: 5px 0px; color: rgb(141, 154, 165); font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;"></p><p style="color: rgb(58, 61, 63); font-size: 15px; font-family: &quot;Open Sans&quot;, sans-serif; background-color: rgb(253, 253, 253); border-radius: 0px !important;">Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo</p>', '29', 0, 1),
(38, '2017-05-09', 'asfafdfsdaf', 9, '<p>sa fsad afsd&nbsp;</p>', '37', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `suplementos_ediciones`
--

CREATE TABLE `suplementos_ediciones` (
  `Id` int(11) NOT NULL,
  `categoria` int(11) DEFAULT NULL,
  `edicion` int(11) DEFAULT NULL,
  `portada` text,
  `archivo` text,
  `fecha_desde` date DEFAULT NULL,
  `fecha_hasta` date DEFAULT NULL,
  `borrar` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suplementos_ediciones`
--

INSERT INTO `suplementos_ediciones` (`Id`, `categoria`, `edicion`, `portada`, `archivo`, `fecha_desde`, `fecha_hasta`, `borrar`) VALUES
(6, 9, 442141, '590d5ef40b83c.jpg', 'test.pdf', '2017-05-06', '2017-05-06', 0),
(7, 3, 17, '5910c7d72a4b2.jpg', 'test.pdf', '2017-05-08', '2017-05-08', 1),
(8, 5, 100, '5910ccf021010.jpg', 'test.pdf', '2017-05-08', '2017-05-08', 0),
(9, 7, 100, '5910d3549d918.jpg', 'test.pdf', '2017-05-08', '2017-05-08', 0),
(10, 3, 29, '5910d4381566b.jpg', 'test.pdf', '2017-05-08', '2017-05-31', 0),
(11, 8, 100, '5910d4c3db57f.jpg', 'test.pdf', '2017-05-08', '2017-06-10', 0),
(12, 4, 1241, '5910d50832b4d.jpg', 'test.pdf', '2017-05-08', '2017-05-08', 0),
(13, 6, 1209, '5910d562f1036.jpg', 'test.pdf', '2017-05-08', '2017-05-08', 0),
(14, 6, 1209, '5910d58d6d51f.jpg', 'test.pdf', '2017-05-08', '2017-05-08', 1),
(15, 0, 0, '5910e7023d979.jpg', 'test.pdf', '0000-00-00', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `suplementos_imagenes`
--

CREATE TABLE `suplementos_imagenes` (
  `Id` int(11) NOT NULL,
  `suplemento` int(11) DEFAULT NULL,
  `imagen` text,
  `predeterminar` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suplementos_imagenes`
--

INSERT INTO `suplementos_imagenes` (`Id`, `suplemento`, `imagen`, `predeterminar`) VALUES
(1, 9, '590a0c3708eef.jpg', 1),
(2, 10, '590a0ec513384.jpg', 1),
(3, 11, '590a255897df7.jpg', 1),
(4, 11, '590a25589978a.jpg', 0),
(19, 19, '590d609a2b084.jpg', 1),
(23, 23, '5910787a4f47b.jpg', 1),
(24, 24, '59107eb4f16c0.jpg', 1),
(27, 27, '591080aad5b37.jpg', 1),
(28, 28, '591080d73284f.jpg', 1),
(29, 29, '591080e615985.jpg', 1),
(30, 30, '59108110f3480.jpg', 1),
(31, 31, '5910813da25e1.jpg', 1),
(32, 32, '591081692b44d.jpg', 1),
(33, 33, '5910818525bae.jpg', 1),
(39, 22, '5910838aed80f.jpg', 1),
(45, 25, '5910a60d5d127.jpg', 1),
(48, 21, '5910a761c3318.jpg', 1),
(49, 37, '5910ca01e5229.jpg', 1),
(50, 26, '5910ccbb95e06.jpg', 1),
(51, 13, '591212bc85ba5.jpg', 1),
(52, 20, '591212c3a0b87.jpg', 1),
(53, 14, '591212ca40e69.jpg', 1),
(54, 17, '591212d42e493.jpg', 1),
(55, 16, '591212db47f76.jpg', 1),
(56, 18, '591212e2cfa42.jpg', 1),
(57, 38, '5912447e25036.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Id` bigint(32) NOT NULL,
  `name` varchar(100) NOT NULL,
  `descripcion` text,
  `user` char(40) NOT NULL,
  `pass` text NOT NULL,
  `level` int(1) NOT NULL,
  `foto_perfil` text,
  `perfil_twitter` text,
  `perfil_facebook` text,
  `borrar` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id`, `name`, `descripcion`, `user`, `pass`, `level`, `foto_perfil`, `perfil_twitter`, `perfil_facebook`, `borrar`) VALUES
(9, 'Federico1', '', 'fede', '7832756692e6f1a77da5b29e458a7422', 0, '590b771ea6417.jpg', '', '', 0),
(26, 'Periodista Nivel 1', 'asd', 'nivel1', '81dc9bdb52d04dc20036dbd8313ed055', 1, '5910aee17d298.jpg', '', '', 0),
(27, 'Periodista Nivel 2 updated', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ultricies neque vel elit pharetra posuere. Curabitur a justo tellus. Quisque ut neque in magna egestas iaculis quis et eros. Integer ut suscipit orci. Mauris volutpat ex non urna luctus, non varius quam laoreet. Aliquam sollicitudin lectus sit amet augue ultrices tempus. Sed rhoncus non augue vel gravida. Maecenas posuere nibh ut dui lacinia lobortis quis eget ante. Cras rhoncus gravida felis, non suscipit augue tincidunt ultrices.', 'nivel2', '81dc9bdb52d04dc20036dbd8313ed055', 2, '5910b2ff2a74e.jpg', 'https://www.twitter.com/', 'https://www.facebook.com/', 0),
(29, 'admin', '', 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, '590b7800df70d.jpg', '', '', 0),
(30, ' Lic. Sylvia Gimeno', NULL, 'sgimeno', '81dc9bdb52d04dc20036dbd8313ed055', 0, NULL, NULL, NULL, 1),
(31, ' Lic. Sylvia Gimeno', 'Nam faucibus maximus orci quis dictum. Pellentesque pharetra tincidunt vulputate. Proin consequat metus eget leo tincidunt, at imperdiet erat pharetra. Morbi mollis urna quis ante pellentesque egestas. Ut consectetur sapien sapien, ut feugiat ligula auctor ut. Suspendisse tincidunt in magna fringilla mollis. Morbi non tincidunt augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ante elit, lobortis ac nibh pretium, pellentesque egestas mauris. Nulla non est est.', 'usernotas1', '81dc9bdb52d04dc20036dbd8313ed055', 1, '590b7a5be4458.jpg', '', '', 0),
(32, 'asd', 'asd', 'asd', '7815696ecbf1c96e6894b779456d330e', 2, '590b786cbec5c.jpg', '', '', 0),
(33, 'Usuario cargador de notas nive 2', 'Nam faucibus maximus orci quis dictum. Pellentesque pharetra tincidunt vulputate. Proin consequat metus eget leo tincidunt, at imperdiet erat pharetra. Morbi mollis urna quis ante pellentesque egestas. Ut consectetur sapien sapien, ut feugiat ligula auctor ut. Suspendisse tincidunt in magna fringilla mollis. Morbi non tincidunt augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ante elit, lobortis ac nibh pretium, pellentesque egestas mauris. Nulla non est est.', 'usuarionivel2', '81dc9bdb52d04dc20036dbd8313ed055', 2, '590b7ac088111.jpg', 'https://www.twitter.com/', 'https://www.facebook.com/', 0),
(34, 'av', 'ab', 'ab', '187ef4436122d1cc2f40dc2b92f0eba0', 1, '590b777db3182.jpg', 'ab', 'ab', 0),
(35, 'ffasf', 'asfas fas fas ', 'asfasf', '0a040ec34abbfb7f3030345244a913c9', 2, '590b78a6e64d5.jpg', 'asfasf', 'asfasf', 0),
(36, 'a a a a ', 'a a a a', 'a a a a a', '47bce5c74f589f4867dbd57e9ca9f808', 2, '590b7b6af20d8.jpg', 'aa', 'asaa', 1),
(37, 'Javier Alejandro Peralta', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec mauris leo, eleifend ut cursus ut, aliquam vel tortor. Praesent mattis dui posuere ultrices interdum. Nunc sed suscipit libero. Aenean vitae eros semper felis porttitor scelerisque sed nec arcu. Maecenas fringilla congue urna quis aliquam. Suspendisse molestie arcu tellus. Ut luctus eu nulla et facilisis. Donec non convallis diam, ut venenatis nisl. Nullam sit amet dapibus turpis. Sed varius eros ac libero placerat mattis. Maecenas sed risus porta, pretium nulla eget, semper nisi. Donec sed imperdiet ligula, quis molestie augue. Sed et metus sed nulla iaculis rhoncus. Cras lorem massa, cursus in hendrerit ut, dignissim at dolor. Quisque fermentum tortor ut sagittis viverra. Nulla dignissim urna libero, nec dictum orci faucibus ac.', 'radioeletrico', 'ee8fd64a1b55855cc963987749258d67', 1, '5910c261eba34.jpg', 'https://www.twitter.com/', 'https://www.facebook.com/', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abonados`
--
ALTER TABLE `abonados`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `abonados_sesiones`
--
ALTER TABLE `abonados_sesiones`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `categorias_suplementos`
--
ALTER TABLE `categorias_suplementos`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `variable` (`variable`);

--
-- Indexes for table `diarios`
--
ALTER TABLE `diarios`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `diario_extra`
--
ALTER TABLE `diario_extra`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `menu_levels`
--
ALTER TABLE `menu_levels`
  ADD PRIMARY KEY (`menu`,`level`);

--
-- Indexes for table `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `notas_fijas`
--
ALTER TABLE `notas_fijas`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `notas_imagenes`
--
ALTER TABLE `notas_imagenes`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `noticias_categorias_Id_fk` (`categoria`);

--
-- Indexes for table `noticias_imagenes`
--
ALTER TABLE `noticias_imagenes`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `noticias_imagenes_noticias_Id_fk` (`noticia`);

--
-- Indexes for table `noticias_secciones`
--
ALTER TABLE `noticias_secciones`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `publicidades`
--
ALTER TABLE `publicidades`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `secciones`
--
ALTER TABLE `secciones`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `suplementos`
--
ALTER TABLE `suplementos`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `suplementos_categorias_suplementos_Id_fk` (`categoria`);

--
-- Indexes for table `suplementos_ediciones`
--
ALTER TABLE `suplementos_ediciones`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `suplementos_imagenes`
--
ALTER TABLE `suplementos_imagenes`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `suplementos_imagenes_suplementos_Id_fk` (`suplemento`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `user` (`user`),
  ADD KEY `users_levels_Id_fk` (`level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abonados`
--
ALTER TABLE `abonados`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `abonados_sesiones`
--
ALTER TABLE `abonados_sesiones`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `categorias_suplementos`
--
ALTER TABLE `categorias_suplementos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `diarios`
--
ALTER TABLE `diarios`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `diario_extra`
--
ALTER TABLE `diario_extra`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notas`
--
ALTER TABLE `notas`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `notas_fijas`
--
ALTER TABLE `notas_fijas`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `notas_imagenes`
--
ALTER TABLE `notas_imagenes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `noticias_imagenes`
--
ALTER TABLE `noticias_imagenes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT for table `noticias_secciones`
--
ALTER TABLE `noticias_secciones`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `publicidades`
--
ALTER TABLE `publicidades`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `secciones`
--
ALTER TABLE `secciones`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `suplementos`
--
ALTER TABLE `suplementos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `suplementos_ediciones`
--
ALTER TABLE `suplementos_ediciones`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `suplementos_imagenes`
--
ALTER TABLE `suplementos_imagenes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `Id` bigint(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `noticias_categorias_Id_fk` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `noticias_imagenes`
--
ALTER TABLE `noticias_imagenes`
  ADD CONSTRAINT `noticias_imagenes_noticias_Id_fk` FOREIGN KEY (`noticia`) REFERENCES `noticias` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `suplementos`
--
ALTER TABLE `suplementos`
  ADD CONSTRAINT `suplementos_categorias_suplementos_Id_fk` FOREIGN KEY (`categoria`) REFERENCES `categorias_suplementos` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `suplementos_imagenes`
--
ALTER TABLE `suplementos_imagenes`
  ADD CONSTRAINT `suplementos_imagenes_suplementos_Id_fk` FOREIGN KEY (`suplemento`) REFERENCES `suplementos` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
