-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2017 at 04:25 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `power_dib`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu_levels`
--

CREATE TABLE IF NOT EXISTS `menu_levels` (
  `menu` char(10) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_levels`
--

INSERT INTO `menu_levels` (`menu`, `level`) VALUES
('0', 1),
('0.1', 1),
('1', 1),
('1', 2),
('1.1', 1),
('1.1', 2),
('1.2', 1),
('1.2', 2),
('2', 1),
('2', 2),
('5', 1),
('5', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu_levels`
--
ALTER TABLE `menu_levels`
  ADD PRIMARY KEY (`menu`,`level`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
