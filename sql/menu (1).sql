-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2017 at 04:25 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `power_dib`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `Id` char(10) NOT NULL,
  `Nombre` char(30) NOT NULL,
  `Grupo` char(50) NOT NULL,
  `Titulo` char(50) NOT NULL,
  `Link` char(30) NOT NULL,
  `iconCls` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`Id`, `Nombre`, `Grupo`, `Titulo`, `Link`, `iconCls`) VALUES
('0', 'Noticias', 'Noticias', 'Noticias', '', 'fa-home'),
('0.1', 'Noticias', 'Noticias', 'Noticias', 'noticias', 'fa-circle-o'),
('0.2', 'Categorias', 'Noticias', 'Categorias', 'categorias_noticias', 'fa-circle-o'),
('1', 'Suplementos', 'Suplementos', 'Suplementos', '', 'fa-file-text-o'),
('1.0', 'Ediciones', 'Suplementos', 'Ediciones', 'ediciones', 'fa-circle-o'),
('1.1', 'Notas', 'Suplementos', 'Notas', 'suplementos', 'fa-circle-o'),
('1.2', 'Diario extra', 'Suplementos', 'Diario extra', 'diario_extra_listar', 'fa-circle-o'),
('1.3', 'Categorias', 'Suplementos', 'Categorias', 'categorias_suplementos', 'fa-circle-o'),
('10', 'Diario extra', 'Diario extra', 'Diario extra', 'diario_extra_listar', 'fa-user'),
('11', 'Categorias', 'Categorias', 'Categorias', '', 'fa-list'),
('2', 'Blog', 'Notas', 'Notas', 'notas', 'fa-file-text-o'),
('3', 'Principales', 'Principales', 'Principales', 'principales', 'fa-star'),
('6', 'Diarios', 'Diarios', 'Diarios', 'diarios', 'fa-newspaper-o'),
('8', 'Usuarios', 'Usuarios', 'Usuarios', '', 'fa-user'),
('8.1', 'Usuarios', 'Usuarios', 'Usuarios', 'users', 'fa-circle-o'),
('8.2', 'Abonados', 'Usuarios', 'Abonados', 'abonados', 'fa-circle-o'),
('9', 'Configuracion', 'Configuracion', 'Configuracion', 'configuration', 'fa-cogs');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
