<?php include "common/doctype-and-head.php"; ?>
<?php $page_break = 4 ?>
<body>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->
<?php include("common/header.php"); ?>
<?php $suplemento = $dbManager->select(TABLE_CATEGORIAS_SUPLEMENTOS, 'Id', 'DESC', null, $_REQUEST['categoria'])[0] ?>
<?php $categoria = $dbManager->getCategoriaSuplementos($_REQUEST['categoria']) ?>
<!-- bage header Start -->
<div class="container">
    <div class="page-header">
        <h1 class="cat-data4 deviaje">
            <span class="ion-camera"></span>
            Suplemento <?= utf8_encode($categoria['concepto']) ?> </h1>
        <ol class="breadcrumb">
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Suplementos</a></li>
            <li class="active"><?= utf8_encode($categoria['concepto']) ?></li>
        </ol>
    </div>
</div>
<!-- bage header End -->
<!-- data start -->
<?php $suplemento_data = $dbManager->getSuplementoSeccion($_REQUEST['categoria'], 1) ?>
<?php $suplemento_izquierda = $dbManager->getSuplementoSeccion($_REQUEST['categoria'], 2) ?>
<?php $suplemento_derecha = $dbManager->getSuplementoSeccion($_REQUEST['categoria'], 3) ?>
<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12 col-sm-11">
            <div class="row">
                <!-- business start -->
                <div class="col-sm-16 business  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="50">
                    <div class="row">
                        <div class="col-md-16 col-sm-16">
                            <div class="row">
                                <div class="col-md-5 col-sm-7 col-xs-16 suplemento">
                                    <ul class="list-unstyled">
                                        <li>
                                            <?php $edicion_data = $dbManager->getEdicion($_REQUEST['categoria']) ?>
                                            <a href="deviaje_archivo.php?categoria=<?= $edicion_data['categoria'] ?>">
                                                <div class="row">
                                                    <div class="main-title-outer pull-left">
                                                        <div class="main-title_deviaje">Edición
                                                            Nº<?= $edicion_data['edicion'] ?></div>
                                                    </div>
                                                    <div class="text-danger sub-info-bordered deviaje">
                                                        <div class="time">
                                                            <span class="ion-calendar icon"></span>
                                                            Del
                                                            <?= explode('-', $edicion_data['fecha_desde'])[2] ?>
                                                            al
                                                            <?= explode('-', $edicion_data['fecha_hasta'])[2] ?>
                                                            de
                                                            <?= $dbManager->meses[intval(explode('-', $edicion_data['fecha_hasta'])[1]) - 1] ?>
                                                            de
                                                            <?= explode('-', $edicion_data['fecha_hasta'])[0] ?>
                                                        </div>
                                                    </div>
                                                    <div class="topic col-sm-16 bajada_deviaje">
                                                        <a href="gestion/archivos/suplementos/<?= utf8_encode($edicion_data['archivo']) ?>"
                                                           target="_blank">
                                                            <img class="img-thumbnail img-responsive"
                                                                 src="gestion/images/blogmanagement/suplementos/big/<?= utf8_encode($edicion_data['portada']) ?>"
                                                                 width="100%">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-16 border">
                                                        <?php if ($edicion_data['archivo'] && $edicion_data['archivo'] != '' & $edicion_data['archivo'] != 'undefined'): ?>
                                                            <a href="gestion/archivos/suplementos/<?= utf8_encode($edicion_data['archivo']) ?>" download>
                                                                <span class="ion-archive"></span>
                                                                Descargar pdf - Color
                                                            </a>
                                                        <?php endif; ?>
                                                        <div class="clearfix"></div>
                                                        <?php if ($edicion_data['archivo_byn'] && $edicion_data['archivo_byn'] != '' & $edicion_data['archivo_byn'] != 'undefined'): ?>
                                                            <a href="gestion/archivos/suplementos/byn/<?= utf8_encode($edicion_data['archivo_byn']) ?>" download>
                                                                <span class="ion-archive"></span>
                                                                Descargar pdf - ByN
                                                            </a>
                                                            <div class="clearfix"></div>
                                                        <?php endif; ?>
                                                        <a class="pull-left"
                                                           href="deviaje_archivo.php?categoria=<?= $edicion_data['categoria'] ?>">
                                                            <span class="ion-filing"></span>
                                                            Ver archivo

                                                        </a>
                                                    </div>
                                                    <div class="topic col-sm-16 bajada_deviaje">
                                                        <h2><?= substr(utf8_encode($suplemento['resenia']), 0, 150) ?></h2>
                                                    </div>
                                                    <div class="col-md-16">
                                                        <a class="popup-img" href="images/alcance/deviaje.jpg">
                                                            <div class="thumb-box alcance">
                                                                Ver alcance >
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <?php if ($suplemento_data): ?>
                                    <div class="col-md-11 col-sm-9 col-xs-16 suplemento2">
                                        <div class="topic">
                                            <a href="#">
                                                <img class="img-thumbnail img-responsive"
                                                     src="gestion/images/blogmanagement/suplementos/big/<?= $suplemento_data['imagen'] ?>"
                                                     width="100%"
                                                     alt="">
                                            </a>
                                            <h2><?= utf8_encode($suplemento_data['titulo']) ?></h2>
                                            <p> <?= substr((strip_tags($suplemento_data['cuerpo'])), 0, 600) ?></p>
                                            <a href="nota-suplemento.php?suplemento=<?= $suplemento_data['Id'] ?>"
                                               class="deviaje">
                                                Leer nota >
                                            </a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <?php if ($suplemento_izquierda): ?>
                        <div class="col-md-8 col-sm-9 col-xs-16 fondo">
                            <div class="topic col-md-9"><a href="#">
                                    <img class="img-thumbnail img-responsive"
                                         src="gestion/images/blogmanagement/suplementos/big/<?= utf8_encode($suplemento_izquierda['imagen']) ?>"
                                         width="100%" alt=""/>
                                </a>
                            </div>
                            <div class="col-md-7 col-sm-9 col-xs-16">
                                <h5><?= utf8_encode($suplemento_izquierda['titulo']) ?> </h5>
                                <a href="nota-suplemento.php?suplemento=<?= $suplemento_izquierda['Id'] ?>"
                                   class="deviaje">
                                    Leer nota >
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($suplemento_derecha): ?>
                        <div class="col-md-8 col-sm-9 col-xs-16 fondo">
                            <div class="topic col-md-9"><a href="#">
                                    <img class="img-thumbnail img-responsive"
                                         src="gestion/images/blogmanagement/suplementos/big/<?= utf8_encode($suplemento_derecha['imagen']) ?>"
                                         width="100%" alt=""/>
                                </a>

                            </div>
                            <div class="col-md-7 col-sm-9 col-xs-16">
                                <h5><?= utf8_encode($suplemento_derecha['titulo']) ?></h5>
                                <a href="nota-suplemento.php?suplemento=<?= $suplemento_derecha['Id'] ?>"
                                   class="deviaje"> Leer nota > </a>

                            </div>
                        </div>
                    <?php endif; ?>
                    <br>
                </div>
                <!-- business end -->
                <div class="col-xs-16 banner-outer-thumb  pull-left  wow fadeInLeft animated" data-wow-delay="0.9s"
                     data-wow-offset="50">
                    <br>
                    <div class="main-title-outer pull-left">

                        <div class="main-title_deviaje">TODAS LAS NOTICIAS</div>
                    </div>

                    <?php foreach ($dbManager->getSuplementos($_REQUEST['categoria'], $_REQUEST['page'] ? $_REQUEST['page'] : 1, $page_break) as $suplemento): ?>
                        <div class="sec-topic col-sm-16 wow fadeInDown animated border" data-wow-delay="0.5s">
                            <div class="row">
                                <div class="col-sm-5"><img width="1000" height="606" alt=""
                                                           src="gestion/images/blogmanagement/suplementos/big/<?= utf8_encode($suplemento['imagen']) ?>"
                                                           class="img-thumbnail"></div>
                                <div class="col-sm-11"><a href="#">
                                        <div class="sec-info">
                                            <h5><?= utf8_encode($suplemento['titulo']) ?> </h5>

                                        </div>
                                    </a>
                                    <p><?= substr((strip_tags($suplemento['cuerpo'])), 0, 250) ?> </p>
                                    <a href="nota-suplemento.php?suplemento=<?= $suplemento['Id'] ?>" class="deviaje">
                                        Leer nota > </a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <!-- Pagination Start -->
                    <div class="col-sm-16">

                        <?php $total = $dbManager->getTotalPages(TABLE_SUPLEMENTOS, $_REQUEST['categoria']) ?>
                        <?php if ($total > $page_break): ?>
                            <?php $offset = ceil($total / $page_break) ?>
                            <ul class="pagination">
                                <li class="<?= !isset($_REQUEST['page']) ? 'disabled' : '' ?>"><a
                                        href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= ($_REQUEST['page'] - 1) ?>&categoria=<?= $_REQUEST['categoria'] ?>">&laquo;</a>
                                </li>
                                <?php for ($i = 1; $i <= $offset; $i++): ?>
                                    <?php
                                    if ($i == $_REQUEST['page'] || (!isset($_REQUEST['page']) && $i == 1))
                                        $active = 'active';
                                    else
                                        $active = '';
                                    ?>
                                    <li class="<?= $active ?>"><a
                                            href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= $i ?>&categoria=<?= $_REQUEST['categoria'] ?>"><?= $i ?></a>
                                    </li>
                                <?php endfor; ?>
                                <li class="<?= !isset($_REQUEST['page']) || $offset == $_REQUEST['page'] ? 'disabled' : '' ?>">
                                    <a href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= ($_REQUEST['page'] + 1) ?>&categoria=<?= $_REQUEST['categoria'] ?>">&raquo;</a>
                                </li>
                            </ul>
                        <?php endif; ?>
                    </div>
                    <!-- Pagination End -->
                </div>
                <!-- banner outer thumb start -->
                <div class="col-xs-16 banner-outer-thumb  pull-left  wow fadeInLeft animated" data-wow-delay="0.23s"
                     data-wow-offset="50">
                    <br>
                    <div class="main-title-outer pull-left">
                        <div class="main-title">Suplementos</div>
                    </div>
                    <div class="row">
                        <div id="banner-thumbs" class="owl-carousel">
                            <?php foreach ($dbManager->select(TABLE_CATEGORIAS_SUPLEMENTOS, 'concepto', 'ASC', 1) as $categoria): ?>
                                <?php $edicion = $dbManager->getSuplementosArchivo($categoria['Id'])[0] ?>
                                <?php if ($edicion): ?>
                                    <div class="item">
                                        <a href="<?= $categoria['link'] ?>?categoria=<?= $categoria['Id'] ?>"
                                           target="_blank">
                                            <div class="box">
                                                <div class="carousel-caption">
                                                    <p><?= substr(utf8_encode($categoria['resenia']), 0, 150) ?> </p>
                                                </div>
                                                <img class="img-responsive"
                                                     src="gestion/images/blogmanagement/suplementos/big/<?= utf8_encode($edicion['portada']) ?>"
                                                     width="1600" height="972" alt=""/>
                                                <div class="overlay"></div>
                                            </div>
                                        </a>
                                        <div class="<?= $dbManager->suplementos_title_clases[$categoria['Id']] ?>">
                                            <h6 class="<?= $dbManager->suplementos_clases[$categoria['Id']] ?>">
                                                <span class="<?= utf8_encode($categoria['icon']) ?>"></span>
                                                <?= utf8_encode($categoria['concepto']) ?></h6>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <!-- banner outer thumb end -->
            </div>
        </div>
        <!-- left sec end -->
        <!-- redes -->
        <div class="col-sm-5 col-md-4 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>
        </div>
    </div>
</div>
<!-- data end -->
<?php include("common/footer.php"); ?>

</body>
</html>