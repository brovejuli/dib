<?php include("common/doctype-and-head.php"); ?>
<body>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->

<?php include("common/header.php"); ?>

<!-- bage header start -->
<div class="container ">
    <div class="page-header">
        <h1>Diario Extra </h1>
        <ol class="breadcrumb">
            <li><a href="index.php">Inicio</a></li>
            <li><a href="#">Suplementos</a></li>
            <li class="active">Diario Extra</li>

        </ol>
    </div>
</div>
<!-- bage header end -->
<!-- data start -->

<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12 col-sm-11">
            <div class="row">
                <div class="col-md-6">
                    <?php $diario_extra = $dbManager->select(TABLE_DIARIO_EXTRA, 'Id', 'DESC', 1)[0] ?>
                    <a href="gestion/archivos/diario_extra/<?= utf8_encode($diario_extra['archivo']) ?>"
                       target="_blank">
                        <img
                            src="gestion/images/blogmanagement/diario_extra/big/<?= utf8_encode($diario_extra['portada']) ?>"
                            style="width: 100%">
                    </a>
                </div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="sec-topic col-sm-16  wow fadeInDown animated" data-wow-delay="0.5s">
                            <div class="row">
                                <div class="col-sm-16 grupo">
                                    <p> <span>Extra</span> es un diario que se edita en la ciudad de La Plata y acompaña
                                        las ediciones locales de diferentes diarios del interior bonaerense.
                                        Con noticias del ámbito nacional, provincial, internacional y
                                        deportivo, Extra cuenta además con una amplia cobertura de información
                                        originada con el aporte de sus periodistas y de las agencias como EFE,
                                        AFP, DPA, DYN, Télam y DIB.</p>
                                    <a class="popup-img" href="images/alcance/diarioextra.jpg">
                                        <div class="thumb-box alcance">
                                            Ver alcance >
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <img class="img-responsive" src="images/datos_extra.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="col-md-4 col-sm-5 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>

        </div>


    </div>
    <!-- left sec end -->
    <!-- redes -->


</div>
<!-- data end -->
<?php include("common/footer.php"); ?>


<div id="create-account" class="white-popup mfp-with-anim mfp-hide">
    <form role="form">
        <h3>Create Account</h3>
        <hr>
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name"
                           tabindex="1">
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-group">
                    <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name"
                           tabindex="2">
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="text" name="display_name" id="display_name" class="form-control" placeholder="Display Name"
                   tabindex="3">
        </div>
        <div class="form-group">
            <input type="email" name="email" id="email" class="form-control " placeholder="Email Address" tabindex="4">
        </div>
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control " placeholder="Password"
                           tabindex="5">
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-group">
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control"
                           placeholder="Confirm Password" tabindex="6">
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-16">
                <input type="submit" value="Register" class="btn btn-danger btn-block btn-lg" tabindex="7">
            </div>
        </div>
    </form>
</div>
<div id="log-in" class="white-popup mfp-with-anim mfp-hide">
    <form role="form">
        <h3>Inciar sesión</h3>
        <hr>
        <div class="form-group">
            <input type="text" name="access_name" id="access_name" class="form-control" placeholder="Nombre"
                   tabindex="3">
        </div>
        <div class="form-group">
            <input type="password" name="password" id="password" class="form-control " placeholder="Contraseña"
                   tabindex="4">
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-16">
                <input type="submit" value="Ingresar" class="btn btn-danger btn-block btn-lg" tabindex="7">
            </div>
        </div>
    </form>
</div>
</div>
<!-- wrapper end -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!--jQuery easing-->
<script src="js/jquery.easing.1.3.js"></script>
<!-- bootstrab js -->
<script src="js/bootstrap.js"></script>
<!--style switcher-->
<script src="js/style-switcher.js"></script> <!--wow animation-->
<script src="js/wow.min.js"></script>
<!-- time and date -->
<script src="js/moment.min.js"></script>
<!--news ticker-->
<script src="js/jquery.ticker.js"></script>
<!-- owl carousel -->
<script src="js/owl.carousel.js"></script>
<!-- magnific popup -->
<script src="js/jquery.magnific-popup.js"></script>
<!-- weather -->
<script src="js/jquery.simpleWeather.min.js"></script>
<!-- calendar-->
<script src="js/jquery.pickmeup.js"></script>
<!-- go to top -->
<script src="js/jquery.scrollUp.js"></script>
<!-- scroll bar -->
<script src="js/jquery.nicescroll.js"></script>
<script src="js/jquery.nicescroll.plus.js"></script>
<!--masonry-->
<script src="js/masonry.pkgd.js"></script>
<!--media queries to js-->
<script src="js/enquire.js"></script>
<!--custom functions-->
<script src="js/custom-fun.js"></script>
</body>
</html>