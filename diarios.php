<?php include("common/doctype-and-head.php"); ?>
<body>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->

<?php include("common/header.php"); ?>

<!-- bage header start -->
<div class="container ">
    <div class="page-header">
        <h1>Diarios de Argentina que están en Internet </h1>
        <ol class="breadcrumb">
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Diarios</a></li>
        </ol>
    </div>
</div>
<!-- bage header end -->
<!-- data start -->

<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12 col-sm-11">
            <div class="row">
                <div class="col-md-11">
                    <div class="row">
                        <div class="sec-topic col-md-16  wow fadeInDown animated " data-wow-delay="0.5s">
                            <div class="row">
                                <?php foreach ($dbManager->select(TABLE_DIARIOS, 'nombre', 'ASC', 1) as $diario): ?>
                                    <div class="sec-info">
                                        <div class="text-danger border">
                                            <a href="<?= $diario['enlace'] ?>" target="_blank">
                                                <div class="pull-right"><span> <?= $diario['enlace'] ?> </span></div>
                                                <div>
                                                    <h6><span class="ion-arrow-right-b"></span>
                                                        <?= utf8_encode($diario['nombre']) ?>
                                                    </h6>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>


                    </div>
                </div>


            </div>


        </div>
        <div class="col-md-4 col-sm-5 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>

        </div>


    </div>
    <!-- left sec end -->
    <!-- redes -->


</div>
<!-- data end -->
<?php include("common/footer.php"); ?>

</div>
<!-- wrapper end -->

</body>
</html>