-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2017 at 11:51 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `power_figueredo`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorias`
--

CREATE TABLE `categorias` (
  `Id` int(11) NOT NULL,
  `categoria` varchar(255) DEFAULT NULL,
  `mostrar` tinyint(2) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categorias`
--

INSERT INTO `categorias` (`Id`, `categoria`, `mostrar`) VALUES
(2, 'Flotantes', 1),
(3, 'Locales', 1),
(5, 'Escaleras', 1),
(6, 'Mobiliarios', 1),
(7, 'Baños', 1),
(8, 'Recorridos', 1);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `Id` int(11) NOT NULL,
  `variable` char(255) NOT NULL,
  `value` char(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`Id`, `variable`, `value`) VALUES
(1, 'MailHost', 'mail.orsettipropiedades.com.ar'),
(2, 'SMTPAuth', 'true'),
(3, 'Mail_Port', '587'),
(4, 'Mail_Username', ''),
(5, 'Mail_Password', 'muGAdu14ku'),
(6, 'Mail_From', 'contacto@orsettipropiedades.com.ar'),
(7, 'Mail_From_Name', 'Orsetti Propiedades'),
(8, 'Title', ''),
(11, 'Empresa_Nombre', 'Orsetti Propiedades'),
(13, 'Empresa_www', 'www.orsettipropiedades.com.ar'),
(14, 'Empresa_Telefono', '0221 4961524'),
(15, 'Youtube_Video_Url', ''),
(16, 'Url_Facebook', 'https://www.facebook.com/orsetti.propiedades/?fref=ts'),
(17, 'Url_Twitter', ''),
(18, 'Url_Pinterest', 'https://www.pinterest.com/'),
(19, 'Url_Instagram', ''),
(20, 'Url_Linkedin', ''),
(21, 'Metatags_Description', ''),
(22, 'Metatags_Keywords', ''),
(23, 'Metatags_Author', ''),
(24, 'Empresa_Direccion', '44 Nº 4579 e/ 186 y 187 Lisandro Olmos'),
(25, 'Empresa_Localidad', ''),
(26, 'Empresa_Movil', '0221 156518778'),
(27, 'Url_Google', '');

-- --------------------------------------------------------

--
-- Table structure for table `fotos`
--

CREATE TABLE `fotos` (
  `Id` int(11) NOT NULL,
  `categorias` text,
  `obra` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  `borrar` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fotos_galeria`
--

CREATE TABLE `fotos_galeria` (
  `Id` int(11) NOT NULL,
  `imagen` text NOT NULL,
  `id_foto` int(11) NOT NULL,
  `borrar` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `Id` int(11) NOT NULL,
  `level` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`Id`, `level`) VALUES
(0, 'Admin'),
(2, 'Blog master'),
(3, 'Cursos master');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `Id` char(10) NOT NULL,
  `Nombre` char(30) NOT NULL,
  `Grupo` char(50) NOT NULL,
  `Titulo` char(50) NOT NULL,
  `Link` char(30) NOT NULL,
  `iconCls` char(30) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`Id`, `Nombre`, `Grupo`, `Titulo`, `Link`, `iconCls`, `level`) VALUES
('0', 'Fotos', 'Fotos', 'Fotos', 'galerias', 'fa-camera', 2),
('1', 'Obras', 'Obras', 'Obras', 'Obras', 'fa-home', 2),
('2', 'Categorias', 'Categorias', 'Categorias', 'categorias', 'fa-list', 2),
('8', 'Usuarios', 'Usuarios', 'Usuarios', 'users', 'fa-user', 1),
('9', 'Configuracion', 'Configuracion', 'Configuracion', 'configuration', 'fa-cogs', 1);

-- --------------------------------------------------------

--
-- Table structure for table `obras`
--

CREATE TABLE `obras` (
  `Id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `porcentaje` double DEFAULT NULL,
  `lugar` varchar(255) DEFAULT NULL,
  `email` text,
  `mostrar` tinyint(2) DEFAULT '1',
  `borrar` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Id` bigint(32) NOT NULL,
  `name` varchar(100) NOT NULL,
  `user` char(40) NOT NULL,
  `pass` text NOT NULL,
  `level` int(1) NOT NULL,
  `borrar` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id`, `name`, `user`, `pass`, `level`, `borrar`) VALUES
(9, 'Federico', 'fede', '7832756692e6f1a77da5b29e458a7422', 0, 0),
(21, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `variable` (`variable`);

--
-- Indexes for table `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `fotos_galeria`
--
ALTER TABLE `fotos_galeria`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `obras`
--
ALTER TABLE `obras`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `user` (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `fotos`
--
ALTER TABLE `fotos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fotos_galeria`
--
ALTER TABLE `fotos_galeria`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `obras`
--
ALTER TABLE `obras`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `Id` bigint(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
