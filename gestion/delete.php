<?php
define('__ROOTWEB__', dirname(__FILE__));
require_once(__ROOTWEB__ . '/validacion.php');
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
require_once(__ROOTWEB__ . '/class/Main.class.php');

$databaseManager = new DatabaseManager();

if (!empty($_POST["action"])) {
    switch ($_POST["action"]) {

        case "user":

            if ($databaseManager->disable(TABLE_USERS, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'An error occurred, check whether the record exists try again.'));

            break;

        case "categoria":

            if ($databaseManager->remove(TABLE_CATEGORIAS, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "categoria_suplemento":

            if ($databaseManager->remove(TABLE_CATEGORIAS_SUPLEMENTOS, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "noticia":

            if ($databaseManager->disable(TABLE_NOTICIAS, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "suplemento":

            if ($databaseManager->disable(TABLE_SUPLEMENTOS, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "abonado":

            if ($databaseManager->disable(TABLE_ABONADOS, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'An error occurred, check whether the record exists try again.'));

            break;

        case "suplemento_edicion":

            if ($databaseManager->disable(TABLE_SUPLEMENTOS_EDICIONES, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "diario_extra":

            $_REQUEST['archivo'] = '';
            $_REQUEST['portada'] = '';
            if ($databaseManager->update(TABLE_DIARIO_EXTRA, $_REQUEST)) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "diario":

            if ($databaseManager->remove(TABLE_DIARIOS, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "nota":

            if ($databaseManager->disable(TABLE_NOTAS, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

        case "noticia_anterior":

            if ($databaseManager->disable(TABLE_NOTICIAS_BACKUP, $_REQUEST['Id'])) {
                echo json_encode(array('success' => true));
            } else
                echo json_encode(array('msg' => 'Ocurrio un erro en la consulta, por favor verifique e intente de nuevo.'));

            break;

    }
}

?>