<?php
define('__ROOTWEB__', dirname(__FILE__));
require_once(__ROOTWEB__ . '/validacion.php');
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
require_once(__ROOTWEB__ . '/class/Main.class.php');
require_once(__ROOTWEB__ . '/class/Email.class.php');

$databaseManager = new DatabaseManager();

if (!empty($_POST["action"])) {

    $logic = ["No" => 0, "Si" => 1];
    $logica = [1 => "Si", 0 => "No"];

    switch ($_POST["action"]) {

        case "noticia":

            include_once 'class/Notas.class.php';
            $obj = new Notas();
            $_REQUEST['usuario'] = $IDUSER;
            $_REQUEST['fecha'] = Main::changeDNormalToSql($_REQUEST['fecha']) . ' ' . $_REQUEST['hora'];
            $_REQUEST['cuerpo'] = Main::clean_richtext($_REQUEST['cuerpo']);
            $_REQUEST['cabecera'] = Main::clean_richtext($_REQUEST['cabecera']);
            $_REQUEST['titulo'] = str_replace('“', '"', $_REQUEST['titulo']);
            $_REQUEST['titulo'] = str_replace('”', '"', $_REQUEST['titulo']);
            $_REQUEST['publicar'] = $logica[$_REQUEST['publicar']];
            if ($_FILES) {
                $_REQUEST['audio'] = Main::upload_audio($_FILES, 'archivos/noticias/');
            }
            if ($last_inserted = $databaseManager->save(TABLE_NOTICIAS, $_REQUEST)) {
                if ($_REQUEST['seccion'])
                    $databaseManager->update_seccion($last_inserted, $_REQUEST['seccion'], 0);
                if ($_REQUEST['imagen']) {
                    $i = 0;
                    foreach ($_REQUEST['imagen'] as $imagen) {
                        $portada = $imagen == $_REQUEST['predeterminada'] ? 1 : 0;
                        $_REQUEST['epigrafe'][$i] = str_replace('“', '"', $_REQUEST['epigrafe'][$i]);
                        $_REQUEST['epigrafe'][$i] = str_replace('”', '"', $_REQUEST['epigrafe'][$i]);
                        $databaseManager->save(TABLE_NOTICIAS_IMAGENES, ['noticia' => $last_inserted, 'imagen' => $imagen, 'predeterminar' => $portada, 'epigrafe' => $_REQUEST['epigrafe'][$i]]);
                        $i++;
                    }
                }
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "user":

            $obj = new Main();
            $obj->save_base64_image($_POST['foto_perfil'], $imagen = uniqid() . time() . '.jpg', 'images/blogmanagement/perfil/');
            $_POST['foto_perfil'] = $imagen;
            if ($last_inserted = $databaseManager->save(TABLE_USERS, $_POST)) {
                echo json_encode(array('status' => true, 'msg' => 'Usuario insertado correctamente!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar insertar el usuario. Por favor intentalo de nuevo!'));

            break;

        case 'categoria':

            if ($databaseManager->save(TABLE_CATEGORIAS, $_POST)) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case 'categoria_suplemento':

            if ($last_inserted = $databaseManager->save(TABLE_CATEGORIAS_SUPLEMENTOS, $_POST)) {
                $databaseManager->save(TABLE_NOTICIAS_SECCIONES, ['noticia' => 0, 'seccion' => 1, 'suplemento' => $last_inserted]);
                $databaseManager->save(TABLE_NOTICIAS_SECCIONES, ['noticia' => 0, 'seccion' => 2, 'suplemento' => $last_inserted]);
                $databaseManager->save(TABLE_NOTICIAS_SECCIONES, ['noticia' => 0, 'seccion' => 3, 'suplemento' => $last_inserted]);
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case 'fotos':
            $mailer = new Email();
            $_REQUEST['categorias'] = implode(',', $_REQUEST['categorias']);
            $_REQUEST['fecha'] = Main::changeDNormalToSql($_REQUEST['fecha']);
            $_REQUEST['usuario'] = $IDUSER;
            if ($last_inserted = $databaseManager->save(TABLE_FOTOS, $_REQUEST)) {
                if (isset($_REQUEST['imagen']))
                    foreach ($_REQUEST['imagen'] as $imagen) {
                        $datos = [
                            'imagen'  => $imagen,
                            'id_foto' => $last_inserted,
                        ];
                        $databaseManager->save(TABLE_FOTOS_GALERIA, $datos);
                    }
                $obra = $databaseManager->select(TABLE_OBRAS, 'Id', 'DESC', '1', $_REQUEST['obra'])[0];
                $mailer->send_mail(explode(',', $obra['email']), $obra, 'Nueva imagen de obra. ', 'WF a subido una nueva foto del avance de obra a su perfil. ');
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "nota":

            include_once 'class/Notas.class.php';
            $obj = new Notas();

            $_REQUEST['usuario'] = $IDUSER;
            $_REQUEST['fecha'] = Main::changeDNormalToSql($_REQUEST['fecha']) . ' ' . $_REQUEST['hora'];
            $_REQUEST['publicar'] = $logica[$_REQUEST['publicar']];

            $obj->save_base64_image($_POST['portada'], $portada = uniqid() . time() . '.jpg', 'images/blogmanagement/notas/big/');

            if ($_FILES) {
                $_REQUEST['audio'] = Main::upload_audio($_FILES, 'archivos/notas/audio/');
            }

            if ($last_inserted = $databaseManager->save(TABLE_NOTAS, $_REQUEST)) {
                $databaseManager->save(TABLE_NOTAS_IMAGENES, ['nota' => $last_inserted, 'imagen' => $portada, 'predeterminar' => 1]);

                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "suplemento":

            include_once 'class/Notas.class.php';
            $obj = new Notas();
            $obj->save_base64_image($_POST['portada'], $portada = uniqid() . time() . '.jpg', 'images/blogmanagement/suplementos/big/');
            if ($_FILES) {
                $_REQUEST['audio'] = Main::upload_audio($_FILES, 'archivos/suplementos/audio/');
            }
            $_REQUEST['usuario'] = $IDUSER;
            $_REQUEST['publicar'] = $_REQUEST['publicar'] ? 'Si' : 'No';
            if ($last_inserted = $databaseManager->save(TABLE_SUPLEMENTOS, $_REQUEST)) {
                if ($_REQUEST['seccion']) {
                    $databaseManager->update_seccion($last_inserted, $_REQUEST['seccion'], $_REQUEST['categoria']);
                }

                $databaseManager->save(TABLE_SUPLEMENTOS_IMAGENES, ['suplemento' => $last_inserted, 'imagen' => trim($portada), 'predeterminar' => 1]);

                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));
            break;

        case "abonado":

            if ($databaseManager->save(TABLE_ABONADOS, $_REQUEST))
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));
            break;

        case "suplemento_edicion":

            include_once 'class/Notas.class.php';
            $obj = new Notas();
            $obj->save_base64_image($_POST['portada'], $portada = uniqid() . time() . '.jpg', 'images/blogmanagement/suplementos/big/');
            $_REQUEST['usuario'] = $IDUSER;
            $_REQUEST['archivo'] = Main::upload_pdf($_FILES, 'archivos/suplementos/');
            $_REQUEST['archivo_byn'] = Main::upload_pdf_byn($_FILES, 'archivos/suplementos/byn/');
            $_REQUEST['portada'] = $portada;
            if ($last_inserted = $databaseManager->save(TABLE_SUPLEMENTOS_EDICIONES, $_REQUEST)) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));
            break;

        case "diario_extra":

            include_once 'class/Notas.class.php';

            $obj = new Notas();

            $_REQUEST['portada'] = $portada;
            $_REQUEST['archivo'] = $_FILES['archivo']['name'];
            $_REQUEST['publicar'] = $logica[$_REQUEST['publicar']];

            $obj->save_base64_image($_POST['portada'], $portada = uniqid() . time() . '.jpg', 'images/blogmanagement/diario_extra/big/');
            $obj->save_pdf_diario_extra($_FILES);

            if ($databaseManager->save(TABLE_DIARIO_EXTRA, $_REQUEST)) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));
            break;

        case 'diario':

            if ($databaseManager->save(TABLE_DIARIOS, $_POST)) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

    }

}

?>