<?php
define('__ROOTWEB__', dirname(__FILE__));
require_once(__ROOTWEB__ . '/validacion.php');
require_once(__ROOTWEB__ . '/class/Configuration.class.php');
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
require_once(__ROOTWEB__ . '/class/Main.class.php');


$databaseManager = new DatabaseManager();

if (!empty($_REQUEST["action"])) {

    $logic = ["No" => 0, "Si" => 1];
    $logica = [1 => "Si", 0 => "No"];

    switch ($_POST["action"]) {

        case "user":

            $obj = new Main();
            $obj->save_base64_image($_REQUEST['foto_perfil_updated'], $imagen = uniqid() . time() . '.jpg', 'images/blogmanagement/perfil/');
            $_POST['foto_perfil'] = $imagen;
            if ($databaseManager->update(TABLE_USERS, $_POST)) {
                echo json_encode(array('status' => true, 'msg' => 'Usuario actualizado correctamente!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar actualizar el usuario. Por favor intentalo de nuevo!'));

            break;

        case "users_pass":
            require(__ROOTWEB__ . "/class/Users.class.php");

            $obj = new Users();
            $id = $_REQUEST["Id"];
            $pass = md5($_REQUEST["pass"]);

            if ($obj->actualizar_pass($pass, $id) == true)
                echo json_encode(array('status' => true, 'msg' => 'Categoria actualizada correctamente!'));
            else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar actualizar la categoria. Por favor intentalo de nuevo!'));

            break;


        case "configuration":

            if ($databaseManager->update(TABLE_CONFIGURATION, $_REQUEST)) {
                echo json_encode(array('status' => true, 'msg' => 'Categoria actualizada correctamente!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar actualizar la categoria. Por favor intentalo de nuevo!'));

            break;

        case "categoria":

            if ($databaseManager->update(TABLE_CATEGORIAS, $_REQUEST)) {
                echo json_encode(array('status' => true, 'msg' => 'Categoria actualizada correctamente!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar actualizar la categoria. Por favor intentalo de nuevo!'));

            break;

        case "categoria_suplemento":

            if ($databaseManager->update(TABLE_CATEGORIAS_SUPLEMENTOS, $_REQUEST)) {
                echo json_encode(array('status' => true, 'msg' => 'Categoria actualizada correctamente!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar actualizar la categoria. Por favor intentalo de nuevo!'));

            break;

        case "abonado":

            if ($databaseManager->update(TABLE_ABONADOS, $_POST)) {
                echo json_encode(array('status' => true, 'msg' => 'Usuario actualizado correctamente!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar actualizar. Por favor intentalo de nuevo!'));

            break;

        case "abonado_pass":

            require(__ROOTWEB__ . "/class/Abonados.class.php");

            $obj = new Abonados();
            $id = $_REQUEST["Id"];
            $pass = md5($_REQUEST["pass"]);

            if ($obj->actualizar_pass($pass, $id) == true)
                echo json_encode(array('status' => true, 'msg' => 'Categoria actualizada correctamente!'));
            else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error al intentar actualizar la categoria. Por favor intentalo de nuevo!'));

            break;

        case "noticia":

            include_once 'class/Notas.class.php';
            $obj = new Notas();
            $_REQUEST['usuario'] = $IDUSER;
            $_REQUEST['fecha'] = Main::changeDNormalToSql($_REQUEST['fecha']) . ' ' . $_REQUEST['hora'];
            if ($_FILES['audio']['name']) {
                $_REQUEST['audio'] = Main::upload_audio($_FILES, 'archivos/noticias/');
            } else
                $_REQUEST['audio'] = $_REQUEST['audio_cargado'];
            $_REQUEST['cuerpo'] = Main::clean_richtext($_REQUEST['cuerpo']);
            $_REQUEST['cabecera'] = Main::clean_richtext($_REQUEST['cabecera']);
            $_REQUEST['titulo'] = str_replace('“', '"', $_REQUEST['titulo']);
            $_REQUEST['titulo'] = str_replace('”', '"', $_REQUEST['titulo']);

            $_REQUEST['publicar'] = $logica[$_REQUEST['publicar']];
            if ($databaseManager->update(TABLE_NOTICIAS, $_REQUEST)) {
                if ($_REQUEST['seccion'])
                    $databaseManager->update_seccion($_REQUEST['Id'], $_REQUEST['seccion'], 0);
                $databaseManager->delete_imagenes($_REQUEST['Id']);
                if ($_REQUEST['imagen']) {
                    $i = 0;
                    foreach ($_REQUEST['imagen'] as $imagen) {
                        $portada = $imagen == $_REQUEST['predeterminada'] ? 1 : '0';
                        $_REQUEST['epigrafe'][$i] = str_replace('“', '"', $_REQUEST['epigrafe'][$i]);
                        $_REQUEST['epigrafe'][$i] = str_replace('”', '"', $_REQUEST['epigrafe'][$i]);
                        $databaseManager->save(TABLE_NOTICIAS_IMAGENES, ['noticia' => $_REQUEST['Id'], 'imagen' => trim($imagen), 'predeterminar' => $portada, 'epigrafe' => $_REQUEST['epigrafe'][$i]]);
                        $i++;
                    }
                }
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "suplemento":

            include_once 'class/Notas.class.php';
            $obj = new Notas();

            $_REQUEST['usuario'] = $IDUSER;
            $_REQUEST['publicar'] = $_REQUEST['publicar'] ? 'Si' : 'No';
            $_REQUEST['fecha'] = $obj->changeDNormalToSql($_REQUEST['fecha']);

            $obj->save_base64_image($_POST['portada'], $portada = uniqid() . time() . '.jpg', 'images/blogmanagement/suplementos/big/');

            if ($_FILES['audio']['name']) {
                $_REQUEST['audio'] = Main::upload_audio($_FILES, 'archivos/suplementos/audio/');
            } else {
                $_REQUEST['audio'] = $_REQUEST['audio_cargado'];
            }

            if ($databaseManager->update(TABLE_SUPLEMENTOS, $_REQUEST)) {
                if ($_REQUEST['seccion']) {
                    $databaseManager->update_seccion($_REQUEST['Id'], $_REQUEST['seccion'], $_REQUEST['categoria']);
                }

                $databaseManager->delete_imagenes_suplementos($_REQUEST['Id']);

                $databaseManager->save(TABLE_SUPLEMENTOS_IMAGENES, ['suplemento' => $_REQUEST['Id'], 'imagen' => trim($portada), 'predeterminar' => 1]);

                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));
            break;

        case "nota":

            include_once 'class/Notas.class.php';
            $obj = new Notas();

            $_REQUEST['usuario'] = $IDUSER;
            $_REQUEST['fecha'] = Main::changeDNormalToSql($_REQUEST['fecha']) . ' ' . $_REQUEST['hora'];
            $_REQUEST['publicar'] = $logica[$_REQUEST['publicar']];

            $obj->save_base64_image($_POST['portada'], $portada = uniqid() . time() . '.jpg', 'images/blogmanagement/notas/big/');

            if ($_FILES['audio']['name']) {
                $_REQUEST['audio'] = Main::upload_audio($_FILES, 'archivos/notas/audio/');
            } else {
                $_REQUEST['audio'] = $_REQUEST['audio_cargado'];
            }

            if ($databaseManager->update(TABLE_NOTAS, $_REQUEST)) {
                $databaseManager->delete_imagenes_notas($_REQUEST['Id']);

                $databaseManager->save(TABLE_NOTAS_IMAGENES, ['nota' => $_REQUEST['Id'], 'imagen' => $portada, 'predeterminar' => 1]);

                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else {
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));
            }

            break;

        case "seccion_noticia":
            if ($databaseManager->update_simple(TABLE_NOTICIAS_SECCIONES, 'noticia', $_REQUEST['noticia'], $_REQUEST['Id'])) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "seccion_suplemento":
            if ($databaseManager->update_simple(TABLE_NOTICIAS_SECCIONES, 'noticia', $_REQUEST['noticia'], $_REQUEST['Id'])) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "publicidad":
            Main::save_publicidad($_FILES);
            if ($databaseManager->update_simple(TABLE_PUBLICIDADES, 'archivo', $_FILES['archivo']['name'], $_REQUEST['Id'])) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "nota_fija":

            if ($databaseManager->update(TABLE_NOTAS_FIJAS, $_REQUEST)) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "limpiar_publicidad":

            if ($databaseManager->update_simple(TABLE_PUBLICIDADES, 'archivo', '', $_REQUEST['Id'])) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "diario_extra":
            if ($archivo = Main::save_diario_extra_pdf($_FILES)) {
                Main::save_image_base64($_POST['portada'], $portada = uniqid() . time() . '.jpg', 'images/blogmanagement/diario_extra/big/');

                $_REQUEST['portada'] = $portada;
                $_REQUEST['archivo'] = $archivo;
                $_REQUEST['publicar'] = $logica[$_REQUEST['publicar']];

                if ($databaseManager->update(TABLE_DIARIO_EXTRA, $_REQUEST)) {
                    echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
                } else
                    echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "suplemento_edicion":

            include_once 'class/Notas.class.php';
            $obj = new Notas();
            $obj->save_base64_image($_POST['portada'], $portada = uniqid() . time() . '.jpg', 'images/blogmanagement/suplementos/big/');
            if ($_FILES['archivo']['name']) {
                $_REQUEST['archivo'] = Main::upload_pdf($_FILES, 'archivos/suplementos/');
            } else
                $_REQUEST['archivo'] = $_REQUEST['pdf_cargado'];
            if ($_FILES['archivo_byn']['name']) {
                $_REQUEST['archivo_byn'] = Main::upload_pdf_byn($_FILES, 'archivos/suplementos/byn/');
            } else
                $_REQUEST['archivo_byn'] = $_REQUEST['pdf_cargado_byn'];
            $_REQUEST['portada'] = $portada;
            $_REQUEST['usuario'] = $IDUSER;
            $_REQUEST['fecha_desde'] = Main::changeDNormalToSql($_REQUEST['fecha_desde']);
            $_REQUEST['fecha_hasta'] = Main::changeDNormalToSql($_REQUEST['fecha_hasta']);
            if ($databaseManager->update(TABLE_SUPLEMENTOS_EDICIONES, $_REQUEST)) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));
            break;

        case "nota_rss":

            if ($databaseManager->update(TABLE_NOTAS_RSS, $_REQUEST)) {
                $databaseManager->armar_archivo_embebido();
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "limpiar_rss":

            if ($databaseManager->update_simple(TABLE_NOTAS_RSS, 'nota', '', $_REQUEST['id'])) {
                $databaseManager->update_simple(TABLE_NOTAS_RSS, 'tipo', '', $_REQUEST['id']);
                $databaseManager->update_simple(TABLE_NOTAS_RSS, 'titulo', '', $_REQUEST['id']);
                $databaseManager->armar_archivo_embebido();
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "diario":

            if ($databaseManager->update(TABLE_DIARIOS, $_REQUEST)) {
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

        case "noticia_anterior":

            include_once 'class/Notas.class.php';
            $obj = new Notas();
            $_REQUEST['usuario'] = $IDUSER;
            $_REQUEST['fecha'] = Main::changeDNormalToSql($_REQUEST['fecha']) . ' ' . $_REQUEST['hora'];
            if ($_FILES['audio']['name']) {
                $_REQUEST['audio'] = Main::upload_audio($_FILES, 'archivos/noticias/');
            } else
                $_REQUEST['audio'] = $_REQUEST['audio_cargado'];
            $_REQUEST['cuerpo'] = Main::clean_richtext($_REQUEST['cuerpo']);
            $_REQUEST['cabecera'] = Main::clean_richtext($_REQUEST['cabecera']);
            $_REQUEST['titulo'] = str_replace('“', '"', $_REQUEST['titulo']);
            $_REQUEST['titulo'] = str_replace('”', '"', $_REQUEST['titulo']);
            if ($databaseManager->update(TABLE_NOTICIAS_BACKUP, $_REQUEST)) {
                if ($_REQUEST['seccion'])
                    $databaseManager->update_seccion($_REQUEST['Id'], $_REQUEST['seccion'], 0);
                $databaseManager->delete_imagenes_anteriores($_REQUEST['Id']);
                if ($_REQUEST['imagen']) {
                    $i = 0;
                    foreach ($_REQUEST['imagen'] as $imagen) {
                        $portada = $imagen == $_REQUEST['predeterminada'] ? 1 : 0;
                        $_REQUEST['epigrafe'][$i] = str_replace('“', '"', $_REQUEST['epigrafe'][$i]);
                        $_REQUEST['epigrafe'][$i] = str_replace('”', '"', $_REQUEST['epigrafe'][$i]);
                        $databaseManager->save(TABLE_NOTICIAS_IMAGENES_BACKUP, ['noticia' => $_REQUEST['Id'], 'imagen' => $imagen, 'predeterminar' => $portada, 'epigrafe' => $_REQUEST['epigrafe'][$i]]);
                        $i++;
                    }
                }
                echo json_encode(array('status' => true, 'msg' => 'Registro cargado!'));
            } else
                echo json_encode(array('status' => false, 'msg' => 'Ocurrio un error. Por favor vuelva a intentar mas tarde!'));

            break;

    }
}

?>