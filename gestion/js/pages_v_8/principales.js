var q = '';
var campo = '';
var srt = '';
var order = '';
var page = '1';
var url;

$(document).ready(function () {
    autocompleteUserLevels();

    $('.fecha_input').datepicker({
        format: 'mm/dd/yyyy',
        language: 'en'
    });

    $('#frmSuplementos #noticia_titulo').keypress(function () {
        autocompleteSuplementos();
    })
    $('#frmNotasFijas #noticia_nombre').keypress(function () {
        autocompleteNotaFija();
    })

    $("#frm #noticia_titulo").autocomplete("autocompletar.php?act=noticias")
        .result(function (evt, data, formatted) {
            $("#frm input[name=noticia]").val(data[1]);
        });

    $('#suplemento_categoria').change(function () {
        $('.data-table-suplementos').bootstrapTable('refresh');
    })


    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            noticia_titulo: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo requerido!'
                    }
                }
            },
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $.post(url, $form.serialize(), function (result) {
            if (result.status) {
                $("#modal_form_noticias").modal("hide");
                $('.data-table').bootstrapTable('refresh');
                $('input[type=submit]').prop('disabled', false);
            }
        }, 'json');
    });

    $('#frmSuplementos').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            concepto: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo requerido!'
                    }
                }
            },
            resenia: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo requerido!'
                    }
                }
            },


        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $.post(url, $form.serialize(), function (result) {
            if (result.status) {
                $("#modal_form_suplementos").modal("hide");
                $('.data-table-suplementos').bootstrapTable('refresh');
            }
        }, 'json');
    });

    $('#frmPublicidad').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            archivo: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo requerido!'
                    }
                }
            },
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        var file_data = $('#frmPublicidad #archivo').prop('files')[0];
        var form_data = new FormData();
        form_data.append('archivo', file_data);
        var other_data = $('#frmPublicidad').serializeArray();
        $.each(other_data, function (key, input) {
            form_data.append(input.name, input.value);
        });
        $.ajax({
            async: true,
            url: url,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            data: form_data,
            dataType: 'json',
            beforeSend: function (response) {
            },
            success: function (response) {
                if (response.status) {
                    $("#modal_form_publicidades").modal("hide");
                    $('.data-table-publicidades').bootstrapTable('refresh');
                }
                else {
                    swal(response.msg)
                    $('button[type=submit]').prop('disabled', false)
                }
            }
        });
    });

    $('#frmPublicidadLimpiar').bootstrapValidator()
        .on('success.form.bv', function (e) {
            e.preventDefault();
            var $form = $(e.target);
            var bv = $form.data('bootstrapValidator');
            $.ajax({
                async: true,
                url: url,
                type: 'POST',
                data: $form.serialize(),
                dataType: 'json',
                beforeSend: function (response) {
                },
                success: function (response) {
                    if (response.status) {
                        $("#modal_form_limpiar_publicidades").modal("hide");
                        $('.data-table-publicidades').bootstrapTable('refresh');
                        $('button[type=submit]').prop('disabled', false)
                    }
                    else {
                        swal(response.msg)
                        $('button[type=submit]').prop('disabled', false)
                    }
                }
            });
        });

    $('#frmNotasFijas').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            noticia_nombre: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo requerido!'
                    }
                }
            },
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $.post(url, $form.serialize(), function (result) {
            if (result.status) {
                $("#modal_form_notas_fijas").modal("hide");
                $('.data-table-notas-fijas').bootstrapTable('refresh');
            }
        }, 'json');
    });

    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

});


function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doNew(action) {
    $('.modal-title').text('Nueva categoria');
    $(".clearval").val('');
    $('#frm #action').val(action);
    url = 'new.php';
    return false;
}

function doSave() {
    var str = $("#frm").serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.status) {
                $('#modal_form').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });

    return false;
}


function doAlertRemove(id, name) {
    $(".modal-title").text('Eliminar usuario');
    $('#confirm-delete').modal('show');
    url = 'delete.php';
    str = {
        action: 'user',
        Id: id,
    }
}

function doRemove() {
    var str = $('#frmDelete').serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('#confirm-delete-edicion').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });
}

function doRemoveEdicion() {
    var str = $('#frmDeleteEdicion').serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('#confirm-delete-edicion').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });
}

function doChangePass(id, name) {
    $(".modal-title-pass").text('Change Password ' + name);
    $('#modal_pass').modal('show');
    $('.clearval').val('')
    $('#frmPass input[name=Id]').val(id);
    url = 'update.php';
}

function doSavePass() {
    var str = $("#frmPass").serialize();
    var pass = $("input[name=pass]").val();
    var rpass = $("input[name=rpass]").val();
    if (pass == rpass) {
        $.ajax({
            async: true,
            url: url,
            type: "POST",
            data: str,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#modal_pass').modal('hide');
                    $('.data-table').bootstrapTable('refresh');
                }
                else
                    alertError(data.msg);

            }
        });
    } else {
        alertError('The password is no equals.');
    }

}

function autocompleteUserLevels() {
    $.post('autocompletar.php', {action: 'user_levels'}, function (result) {
        if (result) {
            $('#level').html(result)
        }
    }, 'json');
}

function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="edit"  href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Editar</li>',
        '<li class="divider"></li>',
        '<li><a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i>Eliminar</a></li>',
        '</ul>',
        '</div>',
    ].join('');
}

function actionFormatterSuplementos(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="edit"  href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Editar</li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar categoria seccion noticia');
        url = 'update.php';
        $.each(row, function (key, value) {
            // console.log(key + ' - ' + value);
            $('#frm #' + key).val(value);
        });
        $('#modal_form_noticias').modal('show');
    },
};

window.actionEventsSuplementos = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar categoria seccion');
        url = 'update.php';
        $.each(row, function (key, value) {
            // console.log(key + ' - ' + value);
            $('#frmSuplementos #' + key).val(value);
        });
        $('#modal_form_suplementos').modal('show');
    },
};

function actionFormatterPublicidades(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="edit"  href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Editar</li>',
        '<li><a class="clear"  href="javascript:void(0)"><i class="fa fa-trash"></i> Limpiar</li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEventsPublicidades = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar publicidad ' + row.nombre);
        url = 'update.php';
        $('#frmPublicidad').find('#Id').val(row.Id)
        $('#modal_form_publicidades').modal('show');
    },
    'click .clear': function (e, value, row, index) {
        $('.modal-title').text('Quitar publicidad ' + row.nombre);
        url = 'update.php';
        $('#frmPublicidadLimpiar').find('#Id').val(row.Id)
        $('#modal_form_limpiar_publicidades').modal('show');
    },
};

function actionFormatterNotasFijas(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="edit"  href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Editar</li>',
        '<li><a class="remove"  href="javascript:void(0)"><i class="fa fa-trash"></i> Eliminar</li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEventsNotasFijas = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar Nota fija');
        url = 'update.php';
        $.each(row, function (key, value) {
            console.log(key + ' - ' + value);
            $('#frmNotasFijas #' + key).val(value);
        });
        $('#modal_form_notas_fijas').modal('show');
    },
    'click .remove': function (e, value, row, index) {
        swal_procesando()
        $.ajax({
            async: true,
            url: 'update.php',
            type: 'POST',
            data: {
                action: 'limpiar_rss',
                id: row.Id
            },
            dataType: 'json',
            beforeSend: function (response) {
            },
            success: function (response) {
                $('.data-table-notas-fijas').bootstrapTable('refresh');
                swal.close()
            }
        });
    },
};

function queryParamsSuplementos() {
    var params = {};
    params["categoria"] = $('#suplemento_categoria').val();
    return params;
}

function autocompleteSuplementos() {
    $("#frmSuplementos #noticia_titulo").autocomplete("autocompletar.php?act=suplementos&categoria=" + $('#suplemento_categoria').val())
        .result(function (evt, data, formatted) {
            $("#frmSuplementos input[name=noticia]").val(data[1]);
        });
}

function autocompleteNotaFija() {
    $("#frmNotasFijas input[name=noticia_nombre]").autocomplete("autocompletar.php?act=fijos")
        .result(function (evt, data, formatted) {
            $("#frmNotasFijas input[name=nota]").val(data[1]);
            $("#frmNotasFijas input[name=tipo]").val(data[2]);
            $("#frmNotasFijas input[name=titulo]").val(data[3]);
        });
}