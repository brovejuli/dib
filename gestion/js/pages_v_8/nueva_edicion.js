var url = 'new.php';

$(document).ready(function () {

    if (parseInt($('#update').val()))
        url = 'update.php'
    else
        url = 'new.php'

    if ($('#frm #Id').val())
        getEdicionData($('#frm #Id').val())

    $('#cuerpo').summernote({
        height: 300,
    });

    $('#image-cropper').cropit({
        width: 540,
        height: 390,
        smallImage: 'stretch'
    });

    $('#image-cropper-pdf').cropit({
        width: 424,
        height: 577,
        smallImage: 'stretch'
    });

    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            category: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'The name is required and can\'t be empty'
                    }
                }
            },


        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');
        swal_procesando()
        $('#portada').val($('#image-cropper-pdf').cropit('export'));
        // Use Ajax to submit form data
        var file_data = $('#pdf-file').prop('files')[0];
        var file_data_byn = $('#pdf-file-byn').prop('files')[0];
        var form_data = new FormData();
        form_data.append('archivo', file_data);
        form_data.append('archivo_byn', file_data_byn);
        var other_data = $('#frm').serializeArray();
        $.each(other_data, function (key, input) {
            form_data.append(input.name, input.value);
        });
        $.ajax({
            url: url, // point to server-side PHP script
            dataType: 'json',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (result) {
                if (result.status) {
                    window.location.href = "main.php?page=ediciones&title=Ediciones";
                }
            }
        });
    });

    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

    $('.borrar-audio').click(function () {
        $('#pdf_cargado').val('')
        $('.audio-cargado').text('')
        $(this).addClass('hidden')
    })
    $('.borrar-audio-byn').click(function () {
        $('#pdf_cargado_byn').val('')
        $('.audio-cargado-byn').text('')
        $(this).addClass('hidden')
    })
});

function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doNew() {
    window.location.href = "main.php?page=fotos&title=Fotos";
}

function doSave() {
    var str = $("#frm").serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.status) {
                $('#modal_form').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });

    return false;
}


function doAlertRemove(id, name) {
    $(".modal-title").text('Eliminar usuario');
    $('#confirm-delete').modal('show');
    url = 'delete.php';
    str = {
        action: 'user',
        Id: id,
    }
}

function doRemove() {
    var str = $('#frmDelete').serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });
}

function doChangePass(id, name) {
    $(".modal-title-pass").text('Change Password ' + name);
    $('#modal_pass').modal('show');
    $('.clearval').val('')
    $('#frmPass input[name=Id]').val(id);
    url = 'update.php';
}

function doSavePass() {
    var str = $("#frmPass").serialize();
    var pass = $("input[name=pass]").val();
    var rpass = $("input[name=rpass]").val();
    if (pass == rpass) {
        $.ajax({
            async: true,
            url: url,
            type: "POST",
            data: str,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#modal_pass').modal('hide');
                    $('.data-table').bootstrapTable('refresh');
                }
                else
                    alertError(data.msg);

            }
        });
    } else {
        alertError('The password is no equals.');
    }

}

function autocompleteUserLevels() {
    $.post('autocompletar.php', {action: 'user_levels'}, function (result) {
        if (result) {
            $('#level').html(result)
        }
    }, 'json');
}

function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="view"  href="javascript:void(0)"><i class="fa fa-eye"></i> Visualizar</li>',
        '<li class="divider"></li>',
        '<li><a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i>Eliminar</a></li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar categoria ' + row.category);
        url = 'update.php';
        $.each(row, function (key, value) {
            console.log(key + ' - ' + value);
            $('#frm #' + key).val(value);
        });
        $('#modal_form').modal('show');
    },
    'click .view': function (e, value, row, index) {
        window.open("../obra.php?obra=" + row.id_obra);
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Eliminar la imagen ?');
        $('#confirm-delete').modal('show');
        url = 'delete.php';
        $('#frmDelete').find('#Id').val(row.id_imagen);
        console.log(value, row, index);
    }
};

function queryParams() {
    var params = {};
    params["obra"] = $('#filtro_obra').val();
    return params;
}

function getEdicionData(noticia_id) {
    $.ajax({
        async: true,
        url: 'listar.php',
        type: 'POST',
        data: {
            id: noticia_id,
            action: 'edicion'
        },
        dataType: 'json',
        beforeSend: function (response) {
        },
        success: function (response) {
            $.each(response.edicion, function (key, value) {
                if (key == 'cuerpo')
                    $("#cuerpo").summernote("code", value);
                else if (key == 'archivo' && value != 'undefined' && value != '') {
                    $('.audio-cargado').attr('href', 'archivos/suplementos/' + value).text(value)
                    $('#pdf_cargado').val(value)
                    $('.borrar-audio').removeClass('hidden')
                }
                else if (key == 'archivo_byn' && value != 'undefined' && value != '') {
                    $('.audio-cargado-byn').attr('href', 'archivos/suplementos/byn/' + value).text(value)
                    $('#pdf_cargado_byn').val(value)
                    $('.borrar-audio-byn').removeClass('hidden')
                }
                else if (key == 'portada')
                    $('#image-cropper-pdf').cropit('imageSrc', "images/blogmanagement/suplementos/big/" + value);
                else if (key != 'audio')
                    $('#' + key).val(value)
            })

        }
    });
}