var url = 'new.php';
var dropzoneFiles = [];
$(document).ready(function () {

    if (parseInt($('#update').val()))
        url = 'update.php'
    else
        url = 'new.php'

    if ($('#frm #Id').val())
        getNoticiaData($('#frm #Id').val())

    Dropzone.autoDiscover = false;
    try {
        var myDropzone = new Dropzone("#dropzone", {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 8, // MB
            url: 'upload.php',
            acceptedFiles: "image/jpeg,image/png",
            addRemoveLinks: true,
            dictDefaultMessage: '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Arrastrar imagenes</span> para subir \
                <span class="smaller-80 grey">(o hacer click)</span> <br /> \
                <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>'
            ,
            dictResponseError: 'Error while uploading file!',

            //change the previewTemplate to use Bootstrap progress bars
            previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-details\">\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n    <div class=\"dz-size\" data-dz-size></div>\n    <img data-dz-thumbnail />\n  </div>\n  <div class=\"progress progress-small progress-striped active\"><div class=\"progress-bar progress-bar-success\" data-dz-uploadprogress></div></div>\n  <div class=\"dz-success-mark\"><span></span></div>\n  <div class=\"dz-error-mark\"><span></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>"
        });
    } catch (e) {
        alert('Dropzone.js does not support older browsers!');
    }
    myDropzone.on("success", function (file, response) {
        dropzoneFiles.push({'status': file.status, 'name': response});
        // $('.galeria').append('<input type="hidden" name="imagen[]" class="' + file.name + '" id="' + file.name + '" value="' + file.name + '">')
    });
    myDropzone.on("removedfile", function (file) {
        // console.log($('.files').find('.' + file.name).val());
    });

    $('#cabecera').summernote({
        height: 100,
        fontNames: [
            'Source Sans Pro', 'Sans', 'Arial', 'Arial Black', 'Courier',
            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande'
        ],
        toolbar: [
            ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['picture', 'video', 'link']],
        ],
        lang: 'es-ES',
    });

    $('#cuerpo').summernote({
        height: 300,
        fontNames: [
            'Source Sans Pro', 'Sans', 'Arial', 'Arial Black', 'Courier',
            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande'
        ],
        toolbar: [
            ['style', ['style', 'bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['picture', 'video', 'link']],
        ],
        lang: 'es-ES',
    });

    $('#image-cropper').cropit({
        width: 540,
        height: 390,
    });

    $('.save').click(function () {
        $('#publicar').val($(this).data('publicar'))
    })

    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            categoria: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo obligatorio'
                    }
                }
            },
            titulo: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo obligatorio'
                    }
                }
            },
            fecha: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo obligatorio'
                    }
                }
            },
            copete: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo obligatorio'
                    }
                }
            },
            cuerpo: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo obligatorio'
                    }
                }
            },
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator')

        if (!$('#cuerpo').val()) {
            swal({
                title: 'El cuerpo de la noticia no puede estar vacio!',
                type: 'warning',
            })
            $('button[type=submit]').prop('disabled', false)
            return false
        }
        // Use Ajax to submit form data
        swal({
            title: 'Procesando solicitud...',
            text: '<span class="fa fa-refresh fa-spin fa-3x"></span>',
            html: true,
            type: 'warning',
            allowOutsideClick: false,
            showConfirmButton: false,
        })

        var file_data = $('#audio').prop('files')[0];
        var form_data = new FormData();
        form_data.append('audio', file_data);
        var other_data = $('#frm').serializeArray();
        $.each(other_data, function (key, input) {
            form_data.append(input.name, input.value);
        });

        $.ajax({
            async: true,
            url: url,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            data: form_data,
            dataType: 'json',
            beforeSend: function (response) {
            },
            success: function (response) {
                if (response.status)
                    window.location.href = "main.php?page=noticias_anteriores&title=Noticias"
                else {
                    swal(response.msg)
                    $('button[type=submit]').prop('disabled', false)
                }
            }
        });
    });

    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

    $('.imagenes-galeria').click(function () {
        $.each(dropzoneFiles, function (index, file) {
            if (file.status == 'success')
                $('.imagenes_galeria').append('<div class="image-container"><input type="hidden" name="imagen[]" class="' + file.name + '" id="' + file.name + '" value="' + file.name + '"><div class="col-md-3 ' + file.name + '" role="button" style="margin: 10px 0;"><img class="img-thumbnail" src="images/blogmanagement/noticias/thumb/' + file.name + '"><div class="col-md-12"><button type="button" class="btn btn-custom btn-flat predeterminar btn-block" data-imgpredeterminada="' + file.name + '">Predeterminar</button><button type="button" class="col-md-6 btn btn-custom btn-flat borrar-imagen btn-block">Borrar</button></div><input type="text" class="form-control" name="epigrafe[]" placeholder="Ep&iacute;grafe"></div></div>')
        })
        myDropzone.removeAllFiles();
        dropzoneFiles = [];
        $('#modal_galeria').modal('hide');
    })

    $('.imagenes_galeria').on('click', '.predeterminar', function () {
        $('.predeterminar').addClass('btn-custom');
        $(this).removeClass('btn-custom').addClass('btn-danger');
        $('#predeterminada').val($(this).data('imgpredeterminada'));
    })

    $('.borrar-audio').click(function () {
        $('#audio_cargado').val('')
        $('.audio-cargado').text('')
        $(this).addClass('hidden')
    })
});

$('.imagenes_galeria').on('click', '.borrar-imagen', function () {
    $(this).parent().parent().parent().remove();
})

function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doNew() {
    window.location.href = "main.php?page=fotos&title=Fotos";
}

function doSave() {
    var str = $("#frm").serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.status) {
                $('#modal_form').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });

    return false;
}


function doAlertRemove(id, name) {
    $(".modal-title").text('Eliminar usuario');
    $('#confirm-delete').modal('show');
    url = 'delete.php';
    str = {
        action: 'user',
        Id: id,
    }
}

function doRemove() {
    var str = $('#frmDelete').serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });
}

function doChangePass(id, name) {
    $(".modal-title-pass").text('Change Password ' + name);
    $('#modal_pass').modal('show');
    $('.clearval').val('')
    $('#frmPass input[name=Id]').val(id);
    url = 'update.php';
}

function doSavePass() {
    var str = $("#frmPass").serialize();
    var pass = $("input[name=pass]").val();
    var rpass = $("input[name=rpass]").val();
    if (pass == rpass) {
        $.ajax({
            async: true,
            url: url,
            type: "POST",
            data: str,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#modal_pass').modal('hide');
                    $('.data-table').bootstrapTable('refresh');
                }
                else
                    alertError(data.msg);

            }
        });
    } else {
        alertError('The password is no equals.');
    }

}

function autocompleteUserLevels() {
    $.post('autocompletar.php', {action: 'user_levels'}, function (result) {
        if (result) {
            $('#level').html(result)
        }
    }, 'json');
}

function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="view"  href="javascript:void(0)"><i class="fa fa-eye"></i> Visualizar</li>',
        '<li class="divider"></li>',
        '<li><a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i>Eliminar</a></li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar categoria ' + row.category);
        url = 'update.php';
        $.each(row, function (key, value) {
            console.log(key + ' - ' + value);
            $('#frm #' + key).val(value);
        });
        $('#modal_form').modal('show');
    },
    'click .view': function (e, value, row, index) {
        window.open("../obra.php?obra=" + row.id_obra);
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Eliminar la imagen ?');
        $('#confirm-delete').modal('show');
        url = 'delete.php';
        $('#frmDelete').find('#Id').val(row.id_imagen);
        console.log(value, row, index);
    }
};

function queryParams() {
    var params = {};
    params["obra"] = $('#filtro_obra').val();
    return params;
}

function getNoticiaData(noticia_id) {
    $.ajax({
        async: true,
        url: 'listar.php',
        type: 'POST',
        data: {
            id: noticia_id,
            action: 'noticia_anterior'
        },
        dataType: 'json',
        beforeSend: function (response) {
        },
        success: function (response) {
            $.each(response.noticia, function (key, value) {
                if (key == 'cuerpo' || key == 'cabecera')
                    $("#" + key).summernote("code", value);
                else if (key == 'audio' && value != 'undefined' && value != '') {
                    $('.audio-cargado').attr('href', 'archivos/noticias/' + value).text(value)
                    $('#audio_cargado').val(value)
                    $('.borrar-audio').removeClass('hidden')
                }
                else if (key != 'audio')
                    $('#' + key).val(value)
            })
            $.each(response.imagenes, function (key, value) {
                if (parseInt(value.predeterminar)) {
                    $('.imagenes_galeria').append('<div class="image-container"><input type="hidden" name="imagen[]" class="' + value.imagen + '" id="' + value.imagen + '" value="' + value.imagen + '"><div class="col-md-3 ' + value.imagen + '" role="button" style="margin: 10px 0;"><img class="img-thumbnail" src="images/blogmanagement/noticias/thumb/' + value.imagen + '"><div class="col-md-12"><button type="button" class="btn btn-flat predeterminar btn-block btn-danger" data-imgpredeterminada="' + value.imagen + '">Predeterminar</button><button type="button" class="col-md-6 btn btn-custom btn-flat borrar-imagen btn-block">Borrar</button></div><input type="text" class="form-control" name="epigrafe[]" placeholder="Ep&iacute;grafe" value=\'' + value.epigrafe + '\'></div></div>')
                    $('#predeterminada').val(value.imagen)
                }
                else
                    $('.imagenes_galeria').append('<div class="image-container"><input type="hidden" name="imagen[]" class="' + value.imagen + '" id="' + value.imagen + '" value="' + value.imagen + '"><div class="col-md-3 ' + value.imagen + '" role="button" style="margin: 10px 0;"><img class="img-thumbnail" src="images/blogmanagement/noticias/thumb/' + value.imagen + '"><div class="col-md-12"><button type="button" class="btn btn-custom btn-flat predeterminar btn-block" data-imgpredeterminada="' + value.imagen + '">Predeterminar</button><button type="button" class="col-md-6 btn btn-custom btn-flat borrar-imagen btn-block">Borrar</button></div><input type="text" class="form-control" name="epigrafe[]" placeholder="Ep&iacute;grafe" value=\'' + value.epigrafe + '\'></div></div>')
            })
            $('#seccion').val(response.seccion['seccion'])
        }
    });
}