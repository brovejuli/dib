var q = '';
var campo = '';
var srt = '';
var order = '';
var page = '1';
var url;

$(document).ready(function () {

    autocompleteUserLevels();

    $('.fecha_input').datepicker({
        format: 'mm/dd/yyyy',
        language: 'en'
    });

    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            categoria: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Campo requerido!'
                    }
                }
            },


        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $('#portada').val($('#image-cropper-pdf').cropit('export'));
        // Use Ajax to submit form data
        var file_data = $('#archivo').prop('files')[0];
        var form_data = new FormData();
        form_data.append('archivo', file_data);
        var other_data = $('#frm').serializeArray();
        $.each(other_data, function (key, input) {
            form_data.append(input.name, input.value);
        });
        swal_procesando()
        $.ajax({
            url: 'update.php', // point to server-side PHP script
            dataType: 'json',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (result) {
                if (result.status) {
                    location.reload();
                }
            }
        });
    });

    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

    $('#image-cropper-pdf').cropit({
        width: 424,
        height: 577,
    });

});


function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doNew(action) {
    window.location.href = "main.php?page=diario_extra&title=Nuevo Diario Extra";
}

function doSave() {
    var str = $("#frm").serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.status) {
                $('#modal_form').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });

    return false;
}


function doAlertRemove(id, name) {
    $(".modal-title").text('Eliminar usuario');
    $('#confirm-delete').modal('show');
    url = 'delete.php';
    str = {
        action: 'user',
        Id: id,
    }
}

function doRemove() {
    var str = $('#frmDelete').serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });
}

function doChangePass(id, name) {
    $(".modal-title-pass").text('Change Password ' + name);
    $('#modal_pass').modal('show');
    $('.clearval').val('')
    $('#frmPass input[name=Id]').val(id);
    url = 'update.php';
}

function doSavePass() {
    var str = $("#frmPass").serialize();
    var pass = $("input[name=pass]").val();
    var rpass = $("input[name=rpass]").val();
    if (pass == rpass) {
        $.ajax({
            async: true,
            url: url,
            type: "POST",
            data: str,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#modal_pass').modal('hide');
                    $('.data-table').bootstrapTable('refresh');
                }
                else
                    alertError(data.msg);

            }
        });
    } else {
        alertError('The password is no equals.');
    }

}

function autocompleteUserLevels() {
    $.post('autocompletar.php', {action: 'user_levels'}, function (result) {
        if (result) {
            $('#level').html(result)
        }
    }, 'json');
}

function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="edit"  href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> Editar</li>',
        '<li class="divider"></li>',
        '<li><a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i>Limpiar</a></li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar diario extra');
        url = 'update.php';
        $('#modal_form').find('#Id').val(row.Id);
        $('#modal_form').modal('show');
    },

    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Eliminar diario ' + row.Id);
        $('#confirm-delete').modal('show');
        url = 'delete.php';
        $('#frmDelete').find('#Id').val(row.Id);
        console.log(value, row, index);
    }
};