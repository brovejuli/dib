var url = 'new.php';

$(document).ready(function () {

    if (parseInt($('#update').val()))
        url = 'update.php'
    else
        url = 'new.php'

    if ($('#frm #Id').val())
        getNotaData($('#frm #Id').val())

    $('#cuerpo').summernote({
        height: 300,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['style','bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['picture', 'video', 'link']],
        ],
        lang: 'es-ES',
    });

    $('#image-cropper').cropit({
        width: 540,
        height: 390,
        smallImage: 'stretch'
    });

    $('.save').click(function () {
        $('#publicar').val($(this).data('publicar'))
    })

    $('#frm').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            category: {
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'The name is required and can\'t be empty'
                    }
                }
            },
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        $('#portada').val($('#image-cropper').cropit('export'));
        // Use Ajax to submit form data
        swal_procesando()

        var file_data = $('#audio').prop('files')[0];
        var form_data = new FormData();
        form_data.append('audio', file_data);
        var other_data = $('#frm').serializeArray();
        $.each(other_data, function (key, input) {
            form_data.append(input.name, input.value);
        });
        $.ajax({
            async: true,
            url: url,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            data: form_data,
            dataType: 'json',
            beforeSend: function (response) {
            },
            success: function (response) {
                if (response.status)
                    window.location.href = "main.php?page=notas&title=Notas"
                else {
                    swal(response.msg)
                    $('button[type=submit]').prop('disabled', false)
                }
            }
        });

    });

    $('.modal-form').css('width', '780px');
    $('.modal-form').css('margin', '100px auto 100px auto');
    $('.alert_form').css('display', 'none');

    $('.borrar-audio').click(function () {
        $('#audio_cargado').val('')
        $('.audio-cargado').text('')
        $(this).addClass('hidden')
    })

});


function alertError(t) {
    $('.alert_form').css('display', '');
    $(".alert_form").text(t);
    $(".alert_form").delay(2000).fadeOut('fast', function () {
    });
}

function doNew() {
    window.location.href = "main.php?page=fotos&title=Fotos";
}

function doSave() {
    var str = $("#frm").serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.status) {
                $('#modal_form').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });

    return false;
}


function doAlertRemove(id, name) {
    $(".modal-title").text('Eliminar usuario');
    $('#confirm-delete').modal('show');
    url = 'delete.php';
    str = {
        action: 'user',
        Id: id,
    }
}

function doRemove() {
    var str = $('#frmDelete').serialize();
    $.ajax({
        async: true,
        url: url,
        type: "POST",
        data: str,
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                $('#confirm-delete').modal('hide');
                $('.data-table').bootstrapTable('refresh');
            }
            else
                alertError(data.msg);

        }
    });
}

function doChangePass(id, name) {
    $(".modal-title-pass").text('Change Password ' + name);
    $('#modal_pass').modal('show');
    $('.clearval').val('')
    $('#frmPass input[name=Id]').val(id);
    url = 'update.php';
}

function doSavePass() {
    var str = $("#frmPass").serialize();
    var pass = $("input[name=pass]").val();
    var rpass = $("input[name=rpass]").val();
    if (pass == rpass) {
        $.ajax({
            async: true,
            url: url,
            type: "POST",
            data: str,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#modal_pass').modal('hide');
                    $('.data-table').bootstrapTable('refresh');
                }
                else
                    alertError(data.msg);

            }
        });
    } else {
        alertError('The password is no equals.');
    }

}

function autocompleteUserLevels() {
    $.post('autocompletar.php', {action: 'user_levels'}, function (result) {
        if (result) {
            $('#level').html(result)
        }
    }, 'json');
}

function actionFormatter(value, row, index) {
    return [
        '<div class="btn-group">',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>',
        '<button type="button" class="btn btn-custom dropdown-toggle" data-toggle="dropdown" aria-expanded="false">',
        '<span class="caret"></span>',
        '<span class="sr-only">Toggle Dropdown</span>',
        '</button>',
        '<ul class="dropdown-menu" role="menu">',
        '<li><a class="view"  href="javascript:void(0)"><i class="fa fa-eye"></i> Visualizar</li>',
        '<li class="divider"></li>',
        '<li><a class="remove" href="javascript:void(0)"><i class="fa fa-times"></i>Eliminar</a></li>',
        '</ul>',
        '</div>',
    ].join('');
}

window.actionEvents = {

    'click .edit': function (e, value, row, index) {
        $('.modal-title').text('Editar categoria ' + row.category);
        url = 'update.php';
        $.each(row, function (key, value) {
            console.log(key + ' - ' + value);
            $('#frm #' + key).val(value);
        });
        $('#modal_form').modal('show');
    },
    'click .view': function (e, value, row, index) {
        window.open("../obra.php?obra=" + row.id_obra);
    },
    'click .remove': function (e, value, row, index) {
        $(".modal-title").text('Eliminar la imagen ?');
        $('#confirm-delete').modal('show');
        url = 'delete.php';
        $('#frmDelete').find('#Id').val(row.id_imagen);
        console.log(value, row, index);
    }
};

function queryParams() {
    var params = {};
    params["obra"] = $('#filtro_obra').val();
    return params;
}

function getNotaData(nota_id) {
    $.ajax({
        async: true,
        url: 'listar.php',
        type: 'POST',
        data: {
            id: nota_id,
            action: 'nota'
        },
        dataType: 'json',
        beforeSend: function (response) {
        },
        success: function (response) {
            $.each(response.nota, function (key, value) {
                if (key == 'cuerpo')
                    $("#cuerpo").summernote("code", value);
                else if (key == 'audio' && value != 'undefined' && value != '') {
                    $('.audio-cargado').attr('href', 'archivos/notas/audio/' + value).text(value)
                    $('#audio_cargado').val(value)
                    $('.borrar-audio').removeClass('hidden')
                }
                else if (key != 'audio')
                    $('#' + key).val(value)
            })
            $('#image-cropper').cropit('imageSrc', "images/blogmanagement/notas/big/" + response.imagen['imagen']);
        }
    });
}