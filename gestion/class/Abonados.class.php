<?php
define('__ROOT__', dirname(dirname(__FILE__)));
include_once(__ROOT__ . "/class/Connection.class.php");
require_once(__ROOT__ . "/class/Main.class.php");

class Abonados extends Main
{
    //constructor
    var $dbConnection;
    var $table = TABLE_ABONADOS;

    function __construct()
    {
        $this->dbConnection = new DBManager;
    }

    public function select($offset, $limit,$search)
    {
        if ($connection = $this->dbConnection->connect()) {
            $and = $search ? " AND (nombre LIKE '%$search%' OR user LIKE '%$search%' OR medio LIKE '%$search%') " : '';

            $query = $connection->query("SELECT * FROM " . $this->table . " 
                                WHERE borrar = 0
                                $and
                                LIMIT $offset,$limit
                                ");
            if ($query)
                return $query->fetchAll(PDO::FETCH_ASSOC);
            else
                return false;
        }
    }

    public function select_total($search)
    {
        if ($connection = $this->dbConnection->connect()) {
            $and = $search ? " AND (nombre LIKE '%$search%' OR user LIKE '%$search%' OR medio LIKE '%$search%') " : '';

            $query = $connection->query("
                                SELECT COUNT(*) 
                                FROM " . $this->table . " 
                                $and
                                WHERE borrar = 0
                                ");
            if ($query)
                return $query->fetch()[0];
            else
                return false;
        }
    }

    public function actualizar_pass($pass, $id)
    {

        if ($connection = $this->dbConnection->connect()) {
            return $connection->query("UPDATE " . $this->table . " SET pass= '" . $pass . "' WHERE Id = " . $id);
        }

    }

}

?>