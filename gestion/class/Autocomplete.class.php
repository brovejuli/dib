<?php
define('__ROOT__', dirname(dirname(__FILE__)));
include_once(__ROOT__ . "/class/Connection.class.php");
require_once(__ROOT__ . "/class/Main.class.php");

class Autocomplete extends Main
{

    //Variables
    var $dbConnection;

    function __construct()
    {
        $this->dbConnection = new DBManager;
    }

    public function provinces()
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT *
                                FROM " . TABLE_PROVINCES . "
                                ");
            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function cities($province)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT *
                                FROM " . TABLE_CITIES . "
                                WHERE province = $province
                                ");
            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function property($id)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT *
                                FROM " . TABLE_PROPERTIES . " tp
                                WHERE Id = $id
                                ") or die(var_dump($connection->errorInfo()));
            return $query->fetch(PDO::FETCH_ASSOC);
        }
    }

    public function suplementos($q, $suplemento)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT *
                                FROM " . TABLE_SUPLEMENTOS . "
                                WHERE titulo LIKE '%$q%'
                                AND categoria = $suplemento
                                ");
            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function noticias($q)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT *
                                FROM " . TABLE_NOTICIAS . "
                                WHERE titulo LIKE '%$q%'
                                ");
            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function fijas($q)
    {
        if ($connection = $this->dbConnection->connect()) {
            $q = utf8_decode($q);
            $query = $connection->query("
            (SELECT 
                titulo,
                Id,
                'noticia' as tipo 
            FROM " . TABLE_NOTICIAS . " 
            WHERE titulo LIKE '%$q%'
            AND borrar = 0
            AND publicar = 1) 
           UNION
           (SELECT 
                titulo,
                Id,
                'nota' as tipo 
            FROM " . TABLE_NOTAS . " 
            WHERE titulo LIKE '%$q%'
            AND borrar = 0)
            UNION
            (SELECT 
                titulo,
                Id,
                'suplemento' as tipo 
            FROM " . TABLE_SUPLEMENTOS . " 
            WHERE titulo LIKE '%$q%')");

            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

}

?>