<?php
define('__ROOT__', dirname(dirname(__FILE__)));
include_once(__ROOT__ . '/class/Connection.class.php');
require_once(__ROOT__ . '/includes/database_tables.php');

//include_once(__ROOT__ . '/class/securimage.class.php');

class Login
{
    //constructor
    var $dbConnection;
    var $user;
    var $pass;
    var $code;


    function __construct($user, $pass, $code)
    {
        $this->dbConnection = new DBManager;
        $this->user = $user;
        $this->pass = md5($pass);
        $this->code = $code;
    }

    public function verficarCnx()
    {
//        $img = new Securimage();
        /*if($img->check($this->code) == true) {*/
        $row = $this->checkUser();
        if (!empty($row['Id']) && $row['borrar'] == 0) {
            //Comparar Contraseñas
            if (strcmp($row['pass'], $this->pass) == 0) {
                $ar["success"] = "ok";
                //ahora seteo las sessiones necesarias
                $_SESSION['UserSystem'] = $row['user'];
                $_SESSION['IDUser'] = $row['Id'];
                $_SESSION['Name'] = $row['name'];
                $_SESSION['ACCESS'] = $row['level'];
            } else
                $ar["error"] = "Usuario o contraseña incorrecta.";
        } else
            $ar["error"] = "Usuario o contraseña incorrecta.";

        /*} else
            $ar["error"] = "Digito verficador incorrecto.";*/

        return $ar;
    }

    public function checkUser()
    {
        if ($connection = $this->dbConnection->connect()) {
            $sql = $connection->query("SELECT Id, name, user, pass, level,borrar FROM " . TABLE_USERS . " WHERE user='" . $this->user . "'");
            return $sql->fetch();
        }
    }

    public function checkUserAbonado()
    {
        if ($connection = $this->dbConnection->connect()) {
            $sql = $connection->query("SELECT 
                                          *
                                        FROM " . TABLE_ABONADOS . " 
                                        WHERE user='$this->user'
                                        AND pass = '$this->pass' ");
            if ($sql->rowCount())
                return $sql->fetch(PDO::FETCH_ASSOC);
            else
                return false;
        }
    }

    public function set_session_id($id, $session_id)
    {
        if ($connection = $this->dbConnection->connect()) {
            return $connection->query("INSERT INTO
                                      " . TABLE_ABONADOS_SESIONES . "
                                      (
                                      user,
                                      session_id,
                                      session_time
                                      )
                                      VALUES
                                      (
                                      $id,
                                      '$session_id',
                                      '" . date('Y-m-d H:i:s', strtotime('+24 hour'/*'+30 minute'*/, strtotime(date('Y-m-d H:i:s')))) . "'
                                      )");
        }
    }

    public function set_session_expire_time($id)
    {
        if ($connection = $this->dbConnection->connect()) {
            return $connection->query("UPDATE
                                      " . TABLE_ABONADOS_SESIONES . "
                                      SET session_time = '" . date('Y-m-d H:i:s', strtotime('+1 hour'/*'+30 minute'*/, strtotime(date('Y-m-d H:i:s')))) . "'
                                      WHERE session_id = $id");
        }
    }

    public function remove_timed_out_session_id()
    {
        if ($connection = $this->dbConnection->connect()) {
            return $connection->query("DELETE FROM " . TABLE_ABONADOS_SESIONES . " WHERE session_time < '" . date('Y-m-d H:i:s') . "'");
        }
    }

    public function check_session_time($id)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT session_time FROM " . TABLE_ABONADOS_SESIONES . " WHERE session_id = '$id'");
            if ($query->fetch()[0] < date('Y-m-d H:i:s'))
                return 'expired';
            else
                return 'active';
        }
    }

    public function remove_session_id($id)
    {
        if ($connection = $this->dbConnection->connect()) {
            return $connection->query("DELETE FROM " . TABLE_ABONADOS_SESIONES . " WHERE session_id = '$id'");
        }
    }

    public function check_session_count($id)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("SELECT * FROM " . TABLE_ABONADOS_SESIONES . " WHERE user = $id");
            return $query->rowCount();

        }
    }
}

?>