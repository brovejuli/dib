<?php
define('__ROOT__', dirname(dirname(__FILE__)));
include_once(__ROOT__ . "/class/Connection.class.php");
require_once(__ROOT__ . "/class/Main.class.php");
require_once(__ROOT__ . "/validacion.php");

class Ediciones extends Main
{
    //constructor
    var $dbConnection;
    var $table = TABLE_SUPLEMENTOS_EDICIONES;

    function __construct()
    {
        $this->dbConnection = new DBManager;
    }


    public function seccion_ocupada($fecha, $seccion)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("
                                SELECT count(*)
                                FROM " . TABLE_NOTICIAS_SECCIONES . "
                                WHERE seccion = $seccion
                                AND fecha = '" . $this->changeDNormalToSql($fecha) . "'
                                ");

            if ($query->fetch()[0])
                return true;
            else
                return false;
        }
    }

    public function select($user = null)
    {
        if ($connection = $this->dbConnection->connect()) {
            $and = $user ? " AND n.usuario = $user " : '';
            $query = $connection->query("
                                SELECT n.*, u.user as usuario 
                                FROM " . TABLE_NOTAS . " n
                                JOIN " . TABLE_USERS . " u ON u.Id = n.usuario
                                WHERE n.borrar = 0
                                $and
                                ORDER BY n.fecha DESC
                                ");

            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    public function select_nota($id)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("
                                SELECT *,
                                      DATE_FORMAT(fecha_desde, '%d/%m/%Y') as fecha_desde,
                                      DATE_FORMAT(fecha_hasta, '%d/%m/%Y') as fecha_hasta
                                FROM " . TABLE_SUPLEMENTOS_EDICIONES . "
                                WHERE Id = $id
                                ");

            return $query->fetch(PDO::FETCH_ASSOC);
        }
    }

    public function select_imagen($id)
    {
        if ($connection = $this->dbConnection->connect()) {
            $query = $connection->query("
                                SELECT *
                                FROM " . TABLE_NOTAS_IMAGENES . "
                                WHERE nota = $id
                                ");

            return $query->fetch(PDO::FETCH_ASSOC);
        }
    }


}

?>