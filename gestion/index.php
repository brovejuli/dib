<?php
include_once("includes/configure.php");
//if ($_SERVER['HTTP_HOST'] != 'dib.com.ar')
//    header('Location:' . HTTP_SERVER);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= TITLE ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" type="image/x-icon" href="<?= HTTP_SERVER ?>favicon.ico">
    <link rel="stylesheet" type="text/css" media="screen" href="<?= DIR_WS_CSS ?>style.css"/>

</head>

<body>
<?php include_once("pages/login.php"); ?>
</body>

<script src="<?= DIR_WS_BOWER_COMPONENTS ?>jquery/dist/jquery.min.js"></script>
<script src="<?= DIR_WS_JS ?>jquery.validate.js"></script>
<script src="<?= DIR_WS_JS ?>jquery.login.js"></script>
</html>
