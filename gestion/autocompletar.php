<?php
require_once("class/Autocomplete.class.php");
require_once("class/Main.class.php");
require_once("includes/database_tables.php");
$obj = new Autocomplete();
if (!empty($_REQUEST["act"])) {

    $search_val = strtolower($_REQUEST["term"]);

    switch ($_REQUEST["act"]) {

        case "suplementos" :
            $consulta = $obj->suplementos($_REQUEST["q"], $_REQUEST["categoria"]);
            foreach ($consulta as $row) {
                echo utf8_encode($row["titulo"]) . "|" . $row["Id"] . "\n";
            }
            break;

        case "noticias" :
            $consulta = $obj->noticias($_REQUEST["term"]);
            foreach ($consulta as $row) {
                echo utf8_encode($row["titulo"]) . "|" . $row["Id"] . "\n";
            }
            break;

        case "fijos" :
            $consulta = $obj->fijas($_REQUEST["q"]);
            foreach ($consulta as $row) {
                echo utf8_encode($row["tipo"] . ' - ' . $row["titulo"]) . "|" . $row["Id"] . "|" . strtoupper($row["tipo"]) . "|" . utf8_encode($row["titulo"]) . "\n";
            }
            break;

        default:
            echo "Ocurrio un error, intentelo nuevamente";
    }
}
?>