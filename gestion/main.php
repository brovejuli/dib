<?php
ini_set('date.timezone', 'America/Argentina/Buenos_Aires');
include_once("validacion.php");
include_once("includes/configure.php");
define('__ROOTWEB__', dirname(__FILE__));
require_once(__ROOTWEB__ . "/class/Main.class.php");
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');

$obj = new Main();
$dbManager = new DatabaseManager();

include(DIR_WS_COMMON . "doctype-and-head.php");
include(DIR_WS_COMMON . "nav-and-title.php");

if (!empty($_GET['page'])) {

    $folder = "pages/";
    $FILE = $folder . $_GET['page'] . ".php";
    $blank = $folder . "notes.php";
    $exeptions = [
        'nueva_nota',
        'nuevo_suplemento',
        'nueva_noticia',
        'nueva_edicion',
        'diario_extra',
        'noticias_anteriores',
        'editar_noticia_anterior',
    ];

    if (file_exists($FILE) && ($obj->level_file_level($_GET["page"], $ACCESS) || in_array($_GET["page"],$exeptions)))
        include($FILE);
    else
        include($blank);

}


include("common/footer.php");
?>


<!--==============================FOOTER=================================-->
