<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="row">
            <div id="tab_listado">
                <div class="col-md-12">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3><i class="fa fa-list"></i> <?= $_GET["title"] ?></h3>
                        </div>
                        <form id="frm">
                            <input type="hidden" name="action" id="action" value="diario_extra">
                            <input type="hidden" name="fecha" id="fecha" value="<?= date('d/m/Y') ?>">
                            <div class="files"></div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label for="pdf">Archivo PDF</label>
                                        <input id="pdf-file" type="file" name="pdf" required>
                                    </div>
                                    <div class="clearfix"></div>
                                    <br>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="hidden" name="portada" id="portada" required>
                                            <label for="titulo">Portada - PDF</label>
                                            <div id="image-cropper-pdf">
                                                <input type="file" class="cropit-image-input" required>
                                                <br>
                                                <div class="cropit-preview img-thumbnail text-center" role="button">
                                                </div>
                                                <br>
                                                <input type="range" class="cropit-image-zoom-input"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <div class="col-md-12">
                                    <div class="col-md-2 pull-right">
                                        <button type="submit" class="btn btn-lg btn-flat btn-custom btn-block">Guardar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

<!-- Modal New / Update-->
<div class="modal fade modal-add" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>

            </div>
            <form id="frm" method="post">
                <input type="hidden" name="action" id="action" value="obra"/>
                <input type="hidden" name="Id" id="Id"/>
                <div class="hidden-update-data"></div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Nombre</label>
                                <input type="text" id="nombre" name="nombre" class="form-control clearval"
                                       placeholder="Nombre" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Porcentaje</label>
                                <input type="text" id="porcentaje" name="porcentaje" class="form-control clearval"
                                       placeholder="Porcentaje" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Lugar</label>
                                <input type="text" id="lugar" name="lugar" class="form-control clearval"
                                       placeholder="Lugar" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Email</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <select type="text" id="email" name="email[]"
                                                class="form-control email clearval"
                                                placeholder="Email" multiple="multiple" style="width: 100%;">
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="alert alert-danger alert_form" role="alert"></div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Password-->

<!-- Modal Delete !-->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">
                <p>¿Seguro que desea eliminar el diario?</p>
                <p class="debug-url"></p>
                <form id="frmDelete" method="post">
                    <input type="hidden" name="action" value="diario_extra">
                    <input type="hidden" name="Id" id="Id" value="">
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger btn-ok" onclick="doRemove()">Aceptar</a>
            </div>
        </div>
    </div>
</div>