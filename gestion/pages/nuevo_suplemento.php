<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="row">
            <!--<form id="pdf-file">-->
            <!--            -->
            <!--            <button id="upload">Upload</button>-->
            <!--</form>-->
            <div id="tab_listado">
                <div class="col-md-12">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3><i class="fa fa-list"></i> <?= $_GET["title"] ?></h3>
                        </div>
                        <form id="frm">
                            <input type="hidden" name="action" id="action" value="suplemento">
                            <input type="hidden" name="publicar" id="publicar" value="1">
                            <input type="hidden" name="Id" id="Id" value="<?= $_REQUEST['suplemento'] ?>">
                            <input type="hidden" id="update" value="<?= $_REQUEST['suplemento'] ? 1 : 0 ?>">
                            <div class="files"></div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="fecha">Fecha</label>
                                            <input type="text" name="fecha" id="fecha"
                                                   class="form-control datepicker"
                                                   required value="<?= date('d/m/Y') ?>" required>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="titulo">T&iacute;tulo</label>
                                            <input type="text" name="titulo" id="titulo" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="categoria">Secci&oacute;n</label>
                                            <select name="categoria" id="categoria" class="form-control" required>
                                                <option value="">Seleccionar</option>
                                                <?php foreach ($dbManager->select(TABLE_CATEGORIAS_SUPLEMENTOS, 'concepto', 'ASC') as $categoria): ?>
                                                    <option
                                                        value="<?= $categoria['Id'] ?>"><?= utf8_encode($categoria['concepto']) ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="seccion">Ubicaci&oacute;n</label>
                                            <select name="seccion" id="seccion" class="form-control">
                                                <option value="">Seleccionar</option>
                                                <?php $ubicaciones = [1 => 'Principal', 2 => 'Izquierda', 3 => 'Derecha']; ?>
                                                <?php foreach ($dbManager->select(TABLE_SECCIONES, 'Id', 'ASC') as $seccion): ?>
                                                    <option
                                                        value="<?= $seccion['Id'] ?>"><?= utf8_encode($ubicaciones[$seccion['Id']]) ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="hidden" name="portada" id="portada" required>
                                            <label for="titulo">Portada</label>
                                            <div id="image-cropper">
                                                <input type="file" class="cropit-image-input" required>
                                                <br>
                                                <div class="cropit-preview img-thumbnail text-center" role="button">
                                                </div>
                                                <br>
                                                <input type="range" class="cropit-image-zoom-input"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="cuerpo">Cuerpo</label>
                                            <textarea name="cuerpo" id="cuerpo" class="form-control" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="video" class="text-uppercase">Video</label>
                                            <input type="text" name="video" id="video" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="audio" class="text-uppercase">Audio</label>
                                            <div class="clearfix"></div>
                                            <a class="audio-cargado" target="_blank"></a>
                                            <button type="button" class="btn btn-flat hidden borrar-audio"><span
                                                    class="fa fa-times "></span>
                                            </button>
                                            <input type="hidden" name="audio_cargado" id="audio_cargado">
                                            <input id="audio" type="file" name="audio">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <div class="col-md-12">
                                    <div class="col-md-2 pull-right">
                                        <button type="submit" class="btn btn-lg btn-flat btn-custom btn-block save"
                                                data-publicar="1">Publicar
                                        </button>
                                    </div>
                                    <div class="col-md-2 pull-right">
                                        <button type="submit" class="btn btn-lg btn-flat btn-warning btn-block save"
                                                data-publicar="0">Guardar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

<!-- Modal New / Update-->
<div class="modal fade modal-add" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>

            </div>
            <form id="frm" method="post">
                <input type="hidden" name="action" id="action" value="obra"/>
                <input type="hidden" name="Id" id="Id"/>
                <div class="hidden-update-data"></div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Nombre</label>
                                <input type="text" id="nombre" name="nombre" class="form-control clearval"
                                       placeholder="Nombre" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Porcentaje</label>
                                <input type="text" id="porcentaje" name="porcentaje" class="form-control clearval"
                                       placeholder="Porcentaje" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Lugar</label>
                                <input type="text" id="lugar" name="lugar" class="form-control clearval"
                                       placeholder="Lugar" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Email</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <select type="text" id="email" name="email[]"
                                                class="form-control email clearval"
                                                placeholder="Email" multiple="multiple" style="width: 100%;">
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="alert alert-danger alert_form" role="alert"></div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Password-->

<!-- Modal Delete !-->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">
                <p>¿Seguro que desea eliminar la categoria?</p>
                <p class="debug-url"></p>
                <form id="frmDelete" method="post">
                    <input type="hidden" name="action" value="obra">
                    <input type="hidden" name="Id" id="Id" value="">
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger btn-ok" onclick="doRemove()">Aceptar</a>
            </div>
        </div>
    </div>
</div>