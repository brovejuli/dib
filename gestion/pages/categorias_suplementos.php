<!--==============================CONTENT=================================-->
<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="row">

            <div id="tab_listado">
                <div class="col-md-12">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3><i class="fa fa-list"></i> <?= $_GET["title"] ?></h3>
                        </div>
                        <div>
                            <div class="box-body table-responsive table-warning">
                                <div id="toolbar_categorias" class="btn-group">
                                    <button type="button" class="btn btn-default"
                                            onclick="doNew('categoria_suplemento')"
                                            data-toggle="modal" data-target="#modal_form" data-whatever="category_news">
                                        <i class="glyphicon glyphicon-plus"></i> Nuevo
                                    </button>
                                </div>
                                <table class="table table-hover table-striped data-table"
                                       data-toggle="table"
                                       data-url="listar.php?action=categorias_suplementos"
                                       data-query-params="queryParams"
                                       data-pagination="true"
                                       data-search="true"
                                       data-height="600"
                                       data-click-to-select="true"
                                       data-show-refresh="true"
                                       data-show-toggle="true"
                                       data-show-columns="true"
                                       data-toolbar="#toolbar_categorias">
                                    <thead>
                                    <tr>
                                        <th data-field="concepto" data-sortable="true">Categor&iacute;a</th>
                                        <th data-field="resenia" data-sortable="true">Rese&ntilde;a</th>
                                        <th data-field="action" data-formatter="actionFormatter"
                                            data-events="actionEvents"
                                            class="col-md-2 text-center">
                                            Acciones
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="box-footer clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

<!-- Modal New / Update-->
<div class="modal fade modal-add" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>

            </div>
            <form id="frm" method="post">
                <input type="hidden" name="action" id="action" value="categoria_suplemento"/>
                <input type="hidden" name="Id" id="Id"/>
                <div class="hidden-update-data"></div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="concepto" class="col-sm-3 control-label">Categor&iacute;a</label>
                        <input type="text" id="concepto" name="concepto" class="form-control clearval">
                    </div>
                    <div class="form-group">
                        <label for="resenia" class="col-sm-3 control-label">Rese&ntilde;a</label>
                        <textarea type="text" id="resenia" name="resenia" class="form-control" maxlength="150"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Password-->

<!-- Modal Delete !-->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">
                <p>¿Seguro que desea eliminar la categoria?</p>
                <p class="debug-url"></p>
                <form id="frmDelete" method="post">
                    <input type="hidden" name="action" value="categoria_suplemento">
                    <input type="hidden" name="Id" id="Id" value="">
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger btn-ok" onclick="doRemove()">Aceptar</a>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete Edicion !-->
<div class="modal fade" id="confirm-delete-edicion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">
                <p>¿Seguro que desea eliminar la edicion?</p>
                <p class="debug-url"></p>
                <form id="frmDeleteEdicion" method="post">
                    <input type="hidden" name="action" value="suplemento_edicion">
                    <input type="hidden" name="Id" id="Id" class="Id" value="">
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a class="btn btn-danger btn-ok" onclick="doRemoveEdicion()">Aceptar</a>
            </div>
        </div>
    </div>
</div>