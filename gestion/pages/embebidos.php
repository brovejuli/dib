<div class="box-body table-responsive table-warning">
    <table class="table table-hover table-striped data-table-notas-fijas"
           data-toggle="table"
           data-url="listar.php?action=notas_rss"
           data-query-params="queryParamsNotasFijas"
           data-pagination="true"
           data-search="true"
           data-height="600"
           data-click-to-select="true"
           data-show-refresh="true"
           data-show-toggle="true"
           data-show-columns="true"
           data-toolbar="#toolbar_fijos">
        <thead>
        <tr>
            <th data-field="Id" data-sortable="true">Id</th>
            <th data-field="titulo" data-sortable="true">Noticia</th>
            <th data-field="tipo" data-sortable="true">Tipo</th>
            <th data-field="action" data-formatter="actionFormatterNotasFijas"
                data-events="actionEventsNotasFijas"
                class="col-md-2 text-center">
                Acciones
            </th>
        </tr>
        </thead>
    </table>
</div>

<!-- Modal New / Update-->
<div class="modal fade modal-add" id="modal_form_notas_fijas" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form id="frmNotasFijas" method="post">
                <input type="hidden" name="action" id="action" value="nota_rss"/>
                <input type="hidden" name="Id" id="Id">
                <div class="hidden-update-data"></div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="seccion_nombre" class="col-sm-3 control-label">Noticia</label>
                        <input type="text" id="noticia_nombre" name="noticia_nombre" class="form-control clearval">
                        <input type="hidden" id="nota" name="nota" class="form-control clearval">
                        <input type="hidden" id="tipo" name="tipo" class="form-control clearval">
                        <input type="hidden" id="titulo" name="titulo" class="form-control clearval">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>
