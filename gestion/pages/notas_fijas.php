<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="row">
            <div id="tab_listado">
                <div class="col-md-12">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3><i class="fa fa-list"></i> <?= $_GET["title"] ?></h3>
                        </div>
                        <div class="box-body table-responsive table-warning">
                            <table class="table table-hover table-striped data-table-notas-fijas"
                                   data-toggle="table"
                                   data-url="listar.php?action=notas_fijas"
                                   data-query-params="queryParamsNotasFijas"
                                   data-pagination="true"
                                   data-search="true"
                                   data-height="600"
                                   data-click-to-select="true"
                                   data-show-refresh="true"
                                   data-show-toggle="true"
                                   data-show-columns="true"
                                   data-toolbar="#toolbar_fijos">
                                <thead>
                                <tr>
                                    <th data-field="Id" data-sortable="true">Id</th>
                                    <th data-field="titulo" data-sortable="true">Noticia</th>
                                    <th data-field="tipo" data-sortable="true">Tipo</th>
                                    <th data-field="action" data-formatter="actionFormatterNotasFijas"
                                        data-events="actionEventsNotasFijas"
                                        class="col-md-2 text-center">
                                        Acciones
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>

                        <!-- Modal New / Update-->
                        <div class="modal fade modal-add" id="modal_form_notas_fijas" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel">
                            <div class="modal-form" role="document">
                                <div class="modal-content modal-solid">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"></h4>
                                    </div>
                                    <form id="frmNotasFijas" method="post">
                                        <input type="hidden" name="action" id="action" value="nota_fija"/>
                                        <input type="hidden" name="Id" id="Id">
                                        <div class="hidden-update-data"></div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="seccion_nombre"
                                                       class="col-sm-3 control-label">Noticia</label>
                                                <input type="text" id="noticia_nombre" name="noticia_nombre"
                                                       class="form-control clearval">
                                                <input type="hidden" id="nota" name="nota"
                                                       class="form-control clearval">
                                                <input type="hidden" id="tipo" name="tipo"
                                                       class="form-control clearval">
                                                <input type="hidden" id="titulo" name="titulo"
                                                       class="form-control clearval">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Cancelar
                                            </button>
                                            <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

<!-- Modal New / Update-->
<div class="modal fade modal-add" id="modal_form_suplementos" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form id="frmSuplementos" method="post">
                <input type="hidden" name="action" id="action" value="seccion_suplemento"/>
                <input type="hidden" name="Id" id="Id" value="seccion_suplemento"/>
                <div class="hidden-update-data"></div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="seccion_nombre" class="col-sm-3 control-label">Seccion</label>
                        <input type="text" id="seccion_nombre" name="seccion_nombre" class="form-control clearval"
                               readonly>
                        <input type="hidden" id="seccion_id" name="seccion_id" class="clearval">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="noticia_titulo" class="col-sm-3 control-label">Suplemento</label>
                        <input type="text" id="noticia_titulo" name="noticia_titulo" class="form-control clearval">
                        <input type="hidden" id="noticia" name="noticia" class="clearval">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal New / Update-->
<div class="modal fade modal-add" id="modal_form_noticias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form id="frm" method="post">
                <input type="hidden" name="action" id="action" value="seccion_noticia"/>
                <input type="hidden" name="Id" id="Id">
                <div class="hidden-update-data"></div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="seccion_nombre" class="col-sm-3 control-label">Seccion</label>
                        <input type="text" id="seccion_nombre" name="seccion_nombre" class="form-control clearval"
                               readonly>
                        <input type="hidden" id="seccion_id" name="seccion_id" class="clearval">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="noticia_titulo" class="col-sm-3 control-label">Noticia</label>
                        <input type="text" id="noticia_titulo" name="noticia_titulo" class="form-control clearval">
                        <input type="hidden" id="noticia" name="noticia" class="clearval">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>