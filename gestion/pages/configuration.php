<!--==============================CONTENT=================================-->
<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">

    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="container-fluid">
            <div id="tab_listado">
                <div class="col-md-12">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3><i class="fa fa-cogs"></i> <?= $_GET["title"] ?></h3>
                        </div>
                        <div class="box-body table-responsive table-warning">
                            <table class="table table-hover table-striped data-table"
                                   data-toggle="table"
                                   data-url="listar.php?action=configuration"
                                   data-query-params="queryParams"
                                   data-pagination="true"
                                   data-search="true"
                                   data-height="600"
                                   data-click-to-select="true"
                                   data-show-refresh="true"
                                   data-show-toggle="true"
                                   data-show-columns="true"
                                   data-toolbar="#toolbar">
                                <thead>
                                <tr>
                                    <th data-field="variable" data-sortable="true">Variable</th>
                                    <th data-field="value" data-sortable="true">Valor</th>
                                    <th data-field="action" data-formatter="actionFormatter" data-events="actionEvents"
                                        class="text-center col-md-2">
                                        Acciones
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="box-footer clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

<!-- Modal New / Update-->
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">
                <form id="frm" method="post" class="form-horizontal">
                    <input type="hidden" name="action" value="configuration"/>
                    <input type="hidden" id="Id" id="Id"/>
                    <div class="hidden-update-data"></div>
                    <div class="border row">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Variable</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span
                                            class="glyphicon glyphicon-pencil"></span></span>
                                        <input type="text" id="variable" name="variable"
                                               class="disabled form-control clearval"
                                               placeholder="Input name" aria-describedby="basic-addon1" readonly>
                                        <input type="hidden" id="Id" name="Id"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="user" class="col-sm-3 control-label">Valor</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"
                                                                                         aria-hidden="true"></i></span>
                                        <input type="text" id="value" name="value" class="form-control clearval"
                                               placeholder="Input user" aria-describedby="basic-addon1"/>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="alert alert-danger alert_form" role="alert"></div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Delete !-->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">
                <p>You are about to delete one register, this procedure is irreversible.</p>
                <p>Do you want to proceed?</p>
                <p class="debug-url"></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok" onclick="doRemove()">Delete</a>
            </div>
        </div>
    </div>
</div>