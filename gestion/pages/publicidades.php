<div class="box-body table-responsive table-warning">
    <table class="table table-hover table-striped data-table-publicidades"
           data-toggle="table"
           data-url="listar.php?action=publicidades"
           data-query-params="queryParamsPublicidades"
           data-pagination="true"
           data-search="true"
           data-height="600"
           data-click-to-select="true"
           data-show-refresh="true"
           data-show-toggle="true"
           data-show-columns="true"
           data-toolbar="#toolbar_publicidades">
        <thead>
        <tr>
            <th data-field="Id" data-sortable="true">Id</th>
            <th data-field="nombre" data-sortable="true">Nombre</th>
            <th data-field="archivo" data-sortable="true">Archivo</th>
            <th data-field="action" data-formatter="actionFormatterPublicidades"
                data-events="actionEventsPublicidades"
                class="col-md-2 text-center">
                Acciones
            </th>
        </tr>
        </thead>
    </table>
</div>

<!-- Modal New / Update-->
<div class="modal fade modal-add" id="modal_form_publicidades" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form id="frmPublicidad" method="post">
                <input type="hidden" name="action" id="action" value="publicidad"/>
                <input type="hidden" name="Id" id="Id">
                <div class="hidden-update-data"></div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="seccion_nombre" class="col-sm-3 control-label">Archivo</label>
                        <input type="file" id="archivo" name="archivo" class="form-control clearval">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal limpiar-->
<div class="modal fade modal-add" id="modal_form_limpiar_publicidades" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-form modal-sm" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form id="frmPublicidadLimpiar" method="post">
                <input type="hidden" name="action" id="action" value="limpiar_publicidad"/>
                <input type="hidden" name="Id" id="Id">
                <div class="hidden-update-data"></div>
                <div class="modal-body">
                    <p>¿Seguro desea quitar la publicidad?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Aceptar</button>
                </div>
            </form>
        </div>
    </div>
</div>