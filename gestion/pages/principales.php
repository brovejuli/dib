<!--==============================CONTENT=================================-->
<!--WRAPPER PRINCIPAL-->
<div class="content-wrapper">
    <!--WRAPPER DEL CONTENIDO-->
    <section class="content">
        <div class="row">

            <div id="tab_listado">
                <div class="col-md-12">
                    <div class="box box-custom box-solid">
                        <div class="box-header">
                            <h3><i class="fa fa-list"></i> <?= $_GET["title"] ?></h3>
                        </div>
                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#noticias" aria-controls="noticias"
                                                                          role="tab" data-toggle="tab">Noticias</a></li>
                                <li role="presentation"><a href="#suplementos" aria-controls="suplementos" role="tab"
                                                           data-toggle="tab">Suplementos</a></li>
                                <li role="presentation"><a href="#publicidades" aria-controls="publicidades" role="tab"
                                                           data-toggle="tab">Publicidades</a></li>
                                <li role="presentation"><a href="#fijos" aria-controls="fijos" role="tab"
                                                           data-toggle="tab">Notas embebidas</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="noticias">
                                    <div class="box-body table-responsive table-warning">
                                        <table class="table table-hover table-striped data-table"
                                               data-toggle="table"
                                               data-url="listar.php?action=noticias_secciones"
                                               data-query-params="queryParams"
                                               data-pagination="true"
                                               data-search="true"
                                               data-height="600"
                                               data-click-to-select="true"
                                               data-show-refresh="true"
                                               data-show-toggle="true"
                                               data-show-columns="true"
                                               data-toolbar="#toolbar_categorias">
                                            <thead>
                                            <tr>
                                                <th data-field="noticia" data-sortable="true">Id</th>
                                                <th data-field="noticia_titulo" data-sortable="true">Noticia</th>
                                                <th data-field="seccion_nombre" data-sortable="true">Seccion</th>
                                                <th data-field="action" data-formatter="actionFormatter"
                                                    data-events="actionEvents"
                                                    class="col-md-2 text-center">
                                                    Acciones
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="suplementos">
                                    <div class="box-body table-responsive table-warning">
                                        <div id="toolbar_news" class="btn-group">
                                            <div class="form-group">
                                                <select class="form-control" name="suplemento_categoria"
                                                        id="suplemento_categoria">
                                                    <?php foreach ($dbManager->select(TABLE_CATEGORIAS_SUPLEMENTOS, 'concepto', 'ASC', 1) as $categoria): ?>
                                                        <option
                                                            value="<?= $categoria['Id'] ?>"><?= utf8_encode($categoria['concepto']) ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <table class="table table-hover table-striped data-table-suplementos"
                                               data-toggle="table"
                                               data-url="listar.php?action=suplementos_secciones"
                                               data-query-params="queryParamsSuplementos"
                                               data-pagination="true"
                                               data-search="true"
                                               data-height="600"
                                               data-click-to-select="true"
                                               data-show-refresh="true"
                                               data-show-toggle="true"
                                               data-show-columns="true"
                                               data-toolbar="#toolbar_news">
                                            <thead>
                                            <tr>
                                                <th data-field="noticia" data-sortable="true">Id</th>
                                                <th data-field="noticia_titulo" data-sortable="true">Noticia</th>
                                                <th data-field="seccion_nombre" data-sortable="true">Seccion</th>
                                                <th data-field="action" data-formatter="actionFormatterSuplementos"
                                                    data-events="actionEventsSuplementos"
                                                    class="col-md-2 text-center">
                                                    Acciones
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="publicidades">
                                    <?php include_once 'pages/publicidades.php' ?>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="fijos">
                                    <?php include_once 'pages/embebidos.php' ?>
                                </div>
                            </div>

                        </div>

                        <div class="box-footer clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

<!-- Modal New / Update-->
<div class="modal fade modal-add" id="modal_form_suplementos" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form id="frmSuplementos" method="post">
                <input type="hidden" name="action" id="action" value="seccion_suplemento"/>
                <input type="hidden" name="Id" id="Id" value="seccion_suplemento"/>
                <div class="hidden-update-data"></div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="seccion_nombre" class="col-sm-3 control-label">Seccion</label>
                        <input type="text" id="seccion_nombre" name="seccion_nombre" class="form-control clearval"
                               readonly>
                        <input type="hidden" id="seccion_id" name="seccion_id" class="clearval">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="noticia_titulo" class="col-sm-3 control-label">Suplemento</label>
                        <input type="text" id="noticia_titulo" name="noticia_titulo" class="form-control clearval">
                        <input type="hidden" id="noticia" name="noticia" class="clearval">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal New / Update-->
<div class="modal fade modal-add" id="modal_form_noticias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-form" role="document">
        <div class="modal-content modal-solid">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <form id="frm" method="post">
                <input type="hidden" name="action" id="action" value="seccion_noticia"/>
                <input type="hidden" name="Id" id="Id">
                <div class="hidden-update-data"></div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="seccion_nombre" class="col-sm-3 control-label">Seccion</label>
                        <input type="text" id="seccion_nombre" name="seccion_nombre" class="form-control clearval"
                               readonly>
                        <input type="hidden" id="seccion_id" name="seccion_id" class="clearval">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="noticia_titulo" class="col-sm-3 control-label">Noticia</label>
                        <input type="text" id="noticia_titulo" name="noticia_titulo" class="form-control clearval">
                        <input type="hidden" id="noticia" name="noticia" class="clearval">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-custom btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>