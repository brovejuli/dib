<?php
define('__ROOTWEB__', dirname(__FILE__));
require_once(__ROOTWEB__ . '/validacion.php');
require_once(__ROOTWEB__ . '/class/Configuration.class.php');
require_once(__ROOTWEB__ . '/class/DatabaseManager.class.php');
require_once(__ROOTWEB__ . '/class/Main.class.php');

$logic = array(0 => "NO", 1 => "SI");
$databaseManager = new DatabaseManager();

if (!empty($_REQUEST["action"])) {

    switch ($_REQUEST["action"]) {

        case "configuration":

            $query = $databaseManager->select(TABLE_CONFIGURATION, 'variable', 'ASC');
            echo json_encode(Main::utf8_converter($query));

            break;

        case "users":

            require_once(__ROOTWEB__ . '/class/Users.class.php');

            $users = new Users();

            $page = !empty($_POST['page']) ? intval($_POST['page']) : 1;
            $rows = !empty($_POST['rows']) ? intval($_POST['rows']) : 25;
            $srt = !empty($_POST['srt']) ? strval($_POST['srt']) : 'user';
            $order = !empty($_POST['order']) ? strval($_POST['order']) : 'DESC';
            $offset = ($page - 1) * $rows;

            $search = !empty($_POST['q']) ? $_POST['q'] : '';
            $field = !empty($_POST['campo']) ? $_POST['campo'] : '';
            $source = !empty($_POST['source']) ? $_POST['source'] : '';

            $table = TABLE_USERS;
            $query = $users->select($rows, $srt, $order, $offset, $field, $search);

            echo json_encode(Main::utf8_converter($query));

            break;

        case "categorias":

            $query = $databaseManager->select(TABLE_CATEGORIAS, 'categoria', 'ASC');

            echo json_encode(Main::query_converter($query));

            break;

        case "categorias_suplementos":

            $query = $databaseManager->select(TABLE_CATEGORIAS_SUPLEMENTOS, 'concepto', 'ASC');

            echo json_encode(Main::query_converter($query));

            break;

        case "notas":

            include_once 'class/Notas.class.php';

            $obj = new Notas();
            $user = $ACCESS == 2 ? $IDUSER : '';
            $query = $obj->select($user, $_REQUEST['offset'], $_REQUEST['limit'], utf8_decode($_REQUEST['search']));
            $total = $obj->select_total($user, utf8_decode($_REQUEST['search']));

            echo json_encode(["total" => $total, "rows" => Main::query_converter($query)]);

            break;

        case "noticias":

            include_once 'class/Noticias.class.php';

            $obj = new Noticias();

            $user = $ACCESS == 2 ? $IDUSER : '';
            $query = $obj->select(null, $user, $_REQUEST['offset'], $_REQUEST['limit'], utf8_decode($_REQUEST['search']));
            $total = $obj->select_total(null, $user, utf8_decode($_REQUEST['search']));

            echo json_encode(["total" => $total, "rows" => Main::query_converter($query)]);

            break;

        case "suplementos":

            include_once 'class/Suplementos.class.php';

            $obj = new Suplementos();
            $user = $ACCESS == 2 ? $IDUSER : '';
            $query = $obj->select($_REQUEST['categoria'], $user, $_REQUEST['offset'], $_REQUEST['limit'], utf8_decode($_REQUEST['search']));
            $total = $obj->select_total($_REQUEST['categoria'], $user, utf8_decode($_REQUEST['search']));

            echo json_encode(["total" => $total, "rows" => Main::query_converter($query)]);

            break;

        case "abonados":

            require_once(__ROOTWEB__ . '/class/Abonados.class.php');

            $users = new Abonados();

            $query = $users->select($_REQUEST['offset'], $_REQUEST['limit'],$_REQUEST['search']);
            $total = $users->select_total($_REQUEST['search']);

            echo json_encode(["total" => $total, "rows" => Main::query_converter($query)]);

            break;

        case "suplemento":

            include_once 'class/Suplementos.class.php';

            $obj = new Suplementos();

            $query_suplemento = $obj->select_suplemento($_REQUEST['id']);
            $query_imagen = $obj->select_imagen($_REQUEST['id']);
            $query_seccion = $obj->select_seccion($_REQUEST['id'], $query_suplemento['categoria']);

            echo json_encode([
                'suplemento' => Main::query_converter_edit($query_suplemento),
                'imagen' => Main::query_converter_edit($query_imagen),
                'seccion' => $query_seccion,
            ]);

            break;

        case "nota":

            include_once 'class/Notas.class.php';

            $obj = new Notas();

            $query_suplemento = $obj->select_nota($_REQUEST['id']);
            $query_imagen = $obj->select_imagen($_REQUEST['id']);

            echo json_encode([
                'nota' => Main::query_converter_edit($query_suplemento),
                'imagen' => Main::query_converter_edit($query_imagen),
            ]);

            break;

        case "suplementos_ediciones":

            include_once 'class/Suplementos.class.php';

            $obj = new Suplementos();

            $query = $obj->select_ediciones($_REQUEST['offset'], $_REQUEST['limit']);
            $total = $obj->select_ediciones_total();

            echo json_encode(["total" => $total, "rows" => Main::query_converter($query)]);

            break;

        case "diario_extra":

            $query = $databaseManager->select(TABLE_DIARIO_EXTRA, 'Id', 'DESC', 1);

            echo json_encode(Main::query_converter($query));

            break;

        case "noticias_secciones":

            include_once 'class/Noticias.class.php';

            $obj = new Noticias();

            $result[] = $obj->getNoticiasSecciones(1);
            $result[] = $obj->getNoticiasSecciones(2);
            $result[] = $obj->getNoticiasSecciones(3);

            echo json_encode(Main::query_converter_edit($result));

            break;

        case "suplementos_secciones":

            include_once 'class/Suplementos.class.php';

            $obj = new Suplementos();

            $result[] = $obj->getSuplementosSecciones(1, $_REQUEST['categoria']);
            $result[] = $obj->getSuplementosSecciones(2, $_REQUEST['categoria']);
            $result[] = $obj->getSuplementosSecciones(3, $_REQUEST['categoria']);

            echo json_encode(Main::query_converter_edit($result));

            break;

        case "diarios":

            $query = $databaseManager->select(TABLE_DIARIOS, 'nombre', 'ASC');

            echo json_encode(Main::query_converter($query));

            break;

        case "publicidades":

            $query = $databaseManager->select(TABLE_PUBLICIDADES, 'Id', 'ASC', 1);

            echo json_encode(Main::query_converter($query));

            break;

        case "notas_fijas":

            $query = $databaseManager->select(TABLE_NOTAS_FIJAS, 'Id', 'ASC');

            echo json_encode(Main::query_converter($query));

            break;

        case "edicion":

            include_once 'class/Ediciones.class.php';

            $obj = new Ediciones();

            $query_suplemento = $obj->select_nota($_REQUEST['id']);
//            $query_imagen = $obj->select_imagen($_REQUEST['id']);

            echo json_encode([
                'edicion' => Main::query_converter_edit($query_suplemento),
//                'imagen' => Main::query_converter_edit($query_imagen),
            ]);

            break;

        case "notas_rss":

            $query = $databaseManager->select(TABLE_NOTAS_RSS, 'Id', 'ASC');

            echo json_encode(Main::query_converter($query));

            break;

        case "noticias_anteriores":

            include_once 'class/Noticias.class.php';

            $obj = new Noticias();

            $user = $ACCESS == 2 ? $IDUSER : '';
            $query = $obj->select_anteriores(null, $user, $_REQUEST['offset'], $_REQUEST['limit']);
            $total = $obj->select_anteriores_total(null, $user);
            echo json_encode(["total" => $total, "rows" => Main::query_converter($query)]);

            break;

        case "noticia_anterior":

            include_once 'class/Noticias.class.php';

            $obj = new Noticias();

            $query_noticia = $obj->select_noticia_anterior($_REQUEST['id']);
            $query_imagenes = $obj->select_imagenes_anteriores($_REQUEST['id']);
            $query_seccion = $obj->select_seccion($_REQUEST['id']);

            echo json_encode([
                'noticia' => Main::query_converter_edit($query_noticia),
                'imagenes' => Main::query_converter_edit($query_imagenes),
                'seccion' => $query_seccion,
            ]);

            break;

        case "noticia":

            include_once 'class/Noticias.class.php';

            $obj = new Noticias();

            $query_noticia = $obj->select_noticia($_REQUEST['id']);
            $query_imagenes = $obj->select_imagenes($_REQUEST['id']);
            $query_seccion = $obj->select_seccion($_REQUEST['id']);

            echo json_encode([
                'noticia' => Main::query_converter_edit($query_noticia),
                'imagenes' => Main::query_converter_edit($query_imagenes),
                'seccion' => $query_seccion,
            ]);

            break;

    }


}

?>