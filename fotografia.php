<?php include "common/doctype-and-head.php"; ?>
<?php $page_break = 21 ?>
<body>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->

<?php include("common/header.php"); ?>

<!-- bage header Start -->
<div class="container">
    <div class="page-header">
        <h1>
            <span class="ion-camera"></span>
            Fotografías </h1>
        <ol class="breadcrumb">
            <li><a href="index.php">Inicio</a></li>
            <li class="active">Fotografías</li>

        </ol>

    </div>

</div>
<!-- bage header End -->
<!-- data start -->

<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12 col-sm-11">
            <div class="row">
                <!-- business start -->
                <div class="col-md-15 business  wow fadeInDown animated" data-wow-delay="1s" data-wow-offset="50">
                    <?php $i = 0; ?>
                    <?php foreach ($dbManager->getFotografias($_REQUEST['page'] ? $_REQUEST['page'] : 1, $page_break) as $imagen): ?>
                            <div class="col-md-5">
                                <div class="item topic">
                                    <?php if ($_SESSION['abonado']): ?>
                                    <a class="popup-img"
                                       href="gestion/images/blogmanagement/noticias/big/<?= $imagen['imagen'] ?>">
                                        <div class="thumb-box"><span class="ion-arrow-expand"></span>
                                            <?php endif; ?>
                                            <img
                                                class="img-thumbnail"
                                                src="gestion/images/blogmanagement/noticias/thumb/<?= $imagen['imagen'] ?>"
                                                width="1600"
                                                height="972" alt=""/>
                                            <?php if ($_SESSION['abonado']): ?>
                                        </div>
                                    </a>
                                <?php endif; ?>
                                    <a href="#">
                                        <p><span
                                                class="ion-ios7-arrow-right icon"></span> <?= utf8_encode($imagen['epigrafe']) ?>
                                        </p>
                                    </a></div>
                            </div>
                            <?php $i++; ?>
                            <?php if ($i == 3): ?>
                                <div class="clearfix"></div>
                                <?php $i = 0; ?>
                            <?php endif ?>
                    <?php endforeach; ?>
                </div>
                <div class="col-md-1 columnafalsa" data-wow-delay="1s" data-wow-offset="50">
                </div>
                <!-- Pagination Start -->
                <div class="col-sm-16">
                    <?php $total = $dbManager->getTotalPagesFotografias($_REQUEST['user']) ?>
                    <?php if ($total > $page_break): ?>
                        <?php $offset = ceil($total / $page_break) ?>
                        <ul class="pagination">
                            <li class="<?= !isset($_REQUEST['page']) ? 'disabled' : '' ?>"><a
                                    href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= ($_REQUEST['page'] - 1) ?>">&laquo;</a>
                            </li>
                            <?php for ($i = 1; $i <= $offset; $i++): ?>
                                <?php
                                if ($i == $_REQUEST['page'] || (!isset($_REQUEST['page']) && $i == 1))
                                    $active = 'active';
                                else
                                    $active = '';
                                ?>
                                <li class="<?= $active ?>"><a
                                        href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= $i ?>"><?= $i ?></a>
                                </li>
                            <?php endfor; ?>
                            <li class="<?= !isset($_REQUEST['page']) || $offset == $_REQUEST['page'] ? 'disabled' : '' ?>">
                                <a href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= ($_REQUEST['page'] + 1) ?>">&raquo;</a>
                            </li>
                        </ul>
                    <?php endif; ?>
                </div>
                <!-- Pagination End -->


            </div>
        </div>
        <!-- left sec end -->
        <!-- redes -->
        <div class="col-sm-5 col-md-4 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>

        </div>
    </div>
</div>
<!-- data end -->
<?php include("common/footer.php"); ?>

</div>
<!-- wrapper end -->

</body>
</html>