<?php include("common/doctype-and-head.php"); ?>
<body>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '1370941839685660',
            xfbml: true,
            version: 'v2.9'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->

<?php include("common/header.php"); ?>

<!-- bage header start -->
<div class="container ">
    <div class="page-header">
        <h1>En primera persona </h1>
        <ol class="breadcrumb">
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Blog</a></li>
        </ol>
    </div>
</div>
<!-- bage header end -->
<!-- data start -->
<?php $nota = $dbManager->getNota($_REQUEST['nota']) ?>
<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4 col-sm-3">
                    <div class="col-md-16">
                        <?php if ($nota['foto_perfil']): ?>
                            <img class="img-thumbnail"
                                 src="gestion/images/blogmanagement/perfil/<?= utf8_encode($nota['foto_perfil']) ?>"
                                 width="128"
                                 height="128" alt=""/>
                        <?php else: ?>
                            <img class="img-thumbnail"
                                 src="gestion/images/default-user.png"
                                 width="128"
                                 height="128" alt=""/>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-16 blog_perfil2">
                        <h4><?= utf8_encode($nota['name']) ?></h4>
                        <ul class="list-inline">
                            <li><a href="<?= utf8_encode($nota['perfil_twitter']) ?>"><span
                                        class="ion-social-twitter"></span></a></li>
                            <li><a href="<?= utf8_encode($nota['perfil_facebook']) ?>"><span
                                        class="ion-social-facebook"></span></a></li>
                        </ul>
                        <p><?= utf8_encode($nota['descripcion']) ?></p>
                        <br><br>
                    </div>

                    <div>
                        <div class="table-responsive col-md-15">
                            <br>
                            <h6 style="text-align: left">Compartí la nota</h6>
                            <table class="table table-bordered social social2">
                                <tbody>

                                <tr>
                                    <td><a class="twitter2 " href="https://twitter.com/share?text=Diarios Bonaerenses"
                                           class=\"twitter"\
                                           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                            <p><span class="ion-social-twitter"></span>
                                            </p>
                                        </a></td>
                                    <td><a class="facebook2 facebook-post-share" href="#"
                                           data-link="<?= HTTP_WEBSITE ?>nota-blog.php?nota=<?= $nota['Id'] ?>"
                                           data-caption="<?= $nota['titulo'] ?>"
                                           data-name="<?= $nota['titulo'] ?>"
                                           data-description="<?= (strip_tags($nota['cuerpo'])) ?>"
                                           data-image="gestion/images/blogmanagement/notas/big/<?= $dbManager->getNotaImagen($nota['Id'])['imagen'] ?>">
                                            <p><span class=" ion-social-facebook"></span>
                                            </p>
                                        </a></td>
                                    <td><a class="correo" data-toggle="modal" data-target="#myModal" role="button">
                                            <p><span class="ion-android-mail"></span>
                                            </p>
                                        </a>
                                        <div class="modal fade" id="myModal" role="dialog">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close"
                                                                data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">
                                                            <small>Compartir por E-mail</small>
                                                        </h4>
                                                    </div>
                                                    <form id="email-share">
                                                        <div class="modal-body">
                                                            <input type="email" name="email" id="email"
                                                                   class="form-control" required>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Compartir
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tbody>
                            </table>
                            <br>
                        </div>
                    </div>

                </div>
                <div class="col-md-12 col-sm-13 ">
                    <div class="row">
                        <div class="sec-topic col-sm-16  wow fadeInDown animated author-box2" data-wow-delay="0.5s">
                            <div class="row">

                                <div class="col-sm-16 sec-info">
                                    <div class="text-danger sub-info-bordered">
                                        <div class="pull-right"><span> <?= utf8_encode($nota['volanta']) ?> </span>
                                        </div>
                                        <div class="time"><span
                                                class="ion-android-data icon"></span><?= $nota['fecha'] ?></div>
                                    </div>
                                    <div class="contenido-nota">
                                        <h3><?= utf8_encode($nota['titulo']) ?></h3>

                                        <p><?= ($nota['cuerpo']) ?></p>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-16 related">
                    <div class="main-title-outer pull-left ">
                        <div class="main-title">Últimas notas</div>
                    </div>
                    <div class="row author-box">
                        <?php foreach ($dbManager->getNotas($nota['usuario_id'], 1, 3) as $nota): ?>
                            <div class="item topic col-sm-5 col-xs-16">
                                <a href="nota-blog.php?nota=<?= $nota['Id'] ?>">
                                    <img class="img-thumbnail"
                                         src="gestion/images/blogmanagement/notas/big/<?= $dbManager->getNotaImagen($nota['Id'])['imagen'] ?>"
                                         width="1000" height="606" alt=""/>
                                    <div class="text-danger sub-info-bordered">
                                        <div class="pull-right"><span> <?= utf8_encode($nota['volanta']) ?> </span>
                                        </div>
                                        <div class="time"><span
                                                class="ion-android-data icon"></span><?= $nota['fecha'] ?></div>
                                    </div>
                                    <h3><?= utf8_encode($nota['titulo']) ?></h3>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-5 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>
        </div>
    </div>
    <!-- left sec end -->
    <!-- redes -->
</div>
<!-- data end -->
<?php include("common/footer.php"); ?>
</div>
<!-- wrapper end -->
</body>
</html>