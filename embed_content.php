
                    <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <meta http-equiv="refresh" content="600">
                        <title>Diarios Bonaerenses</title>
                        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
                        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
                        <!-- bootstrap styles-->
                        <link href="css/bootstrap.min.css" rel="stylesheet">
                        <!-- google font -->
                        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
                        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">     <!-- ionicons font -->
                        <link href="https://fonts.googleapis.com/css?family=Neucha" rel="stylesheet">
                        <link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
                        <!-- ionicons font -->
                        <link href="css/ionicons.min.css" rel="stylesheet">
                        <!-- animation styles -->
                        <link rel="stylesheet" href="css/animate.css" />
                        <!-- custom styles -->
                        <link href="css/custom-blue.css" rel="stylesheet" id="style">
                        <link href="css/custom-notes.css" rel="stylesheet" id="style">
                        <!-- owl carousel styles-->
                        <link rel="stylesheet" href="css/owl.carousel.css">
                        <link rel="stylesheet" href="css/owl.transitions.css">
                        <!-- magnific popup styles -->
                        <link rel="stylesheet" href="css/magnific-popup.css">
                        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                        <!--[if lt IE 9]>
                        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                        <![endif]-->
                    </head>
                    <body>
                    
                            <div class="col-xs-16 banner-outer-thumb  pull-left  wow fadeInLeft animated animated" data-wow-delay="0.9s" data-wow-offset="50" style="visibility: visible; animation-delay: 0.9s; animation-name: fadeInLeft;">
                            <div class="sec-topic col-sm-16 wow fadeInDown animated border animated" data-wow-delay="0.5s"
                                 style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <img width="1000" height="606" alt="" src="http://dib.com.ar/gestion/images/blogmanagement/noticias/thumb/


59807760e09691501591392.jpg"
                                             class="img-thumbnail">
                                    </div>
                                    <div class="col-sm-11">
                                        <a href="#">
                                            <div class="sec-info">
                                                <h5>El Gobierno y las entidades del campo ya discuten la reforma impositiva de 2018 </h5>
                            
                                            </div>
                                        </a>
                                        <p>La Plata, ago 1 (DIB).- El Gobierno bonaerense y las
entidades del campo iniciaron ya la discusión en torno a la ley impositiva del
año próximo, y desde el sector agrario renovaron sus reclamos por la
eliminación de algunos tributos que consid </p>
                                        <a href="noticia.php?noticia=1735" target="_blank">
                                            Leer m&aacute;s &gt; </a>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-xs-16 banner-outer-thumb  pull-left  wow fadeInLeft animated animated" data-wow-delay="0.9s" data-wow-offset="50" style="visibility: visible; animation-delay: 0.9s; animation-name: fadeInLeft;">
                            <div class="sec-topic col-sm-16 wow fadeInDown animated border animated" data-wow-delay="0.5s"
                                 style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <img width="1000" height="606" alt="" src="http://dib.com.ar/gestion/images/blogmanagement/noticias/thumb/


597fc5d41ea1f1501545940.jpg"
                                             class="img-thumbnail">
                                    </div>
                                    <div class="col-sm-11">
                                        <a href="#">
                                            <div class="sec-info">
                                                <h5>El Gobierno ofreció un "27 por ciento" a los médicos; el sábado responden  </h5>
                            
                                            </div>
                                        </a>
                                        <p>La Plata, jul 31
(DIB).- El Gobierno provincial les ofreció a los médicos un aumento del “27 por
ciento de bolsillo” y arrimó posiciones con la Asociación Sindical de Profesionales
de la Salud de la Provincia de Buenos Aires (Cicop), que a </p>
                                        <a href="noticia.php?noticia=1727" target="_blank">
                                            Leer m&aacute;s &gt; </a>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="col-xs-16 banner-outer-thumb  pull-left  wow fadeInLeft animated animated" data-wow-delay="0.9s" data-wow-offset="50" style="visibility: visible; animation-delay: 0.9s; animation-name: fadeInLeft;">
                            <div class="sec-topic col-sm-16 wow fadeInDown animated border animated" data-wow-delay="0.5s"
                                 style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <img width="1000" height="606" alt="" src="http://dib.com.ar/gestion/images/blogmanagement/suplementos/big/5978bea22bd111501085346.jpg"
                                             class="img-thumbnail">
                                    </div>
                                    <div class="col-sm-11">
                                        <a href="#">
                                            <div class="sec-info">
                                                <h5>San Andrés, el paraíso cristalino </h5>
                            
                                            </div>
                                        </a>
                                        <p>El archipiélago de San Andrés, un paradisiaco
remanso de paz en el Caribe, es uno de esos lugares que merecen la pena ser
explorados, entre días de playas y tardes de compras. Esta pequeña isla de
Colombia de 60 mil habitantes, a 750 kilómetr </p>
                                        <a href="nota-suplemento.php?suplemento=123" target="_blank">
                                            Leer m&aacute;s &gt; </a>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </body>
                        </html>