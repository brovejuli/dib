<?php include("common/doctype-and-head.php"); ?>
<?php $page_break = 4 ?>
<body>
<!-- preloader start -->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- preloader end -->
<?php include("common/header.php"); ?>
<!-- bage header start -->
<div class="container ">
    <div class="page-header">
        <h1>En primera persona</h1>
        <ol class="breadcrumb">
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Blog</a></li>
        </ol>
    </div>
</div>
<!-- bage header end -->
<!-- data start -->

<div class="container ">
    <div class="row ">
        <!-- left sec start -->
        <div class="col-md-12 col-sm-11">
            <div class="row">
                <!-- left sec start -->
                <div class="col-md-16 col-sm-16">
                    <div class="row">
                        <div class="col-md-16 col-sm-16">
                            <div class="row">
                                <?php $usuario = $dbManager->select(TABLE_USERS, 'Id', 'ASC', null, $_REQUEST['user'])[0] ?>
                                <div class=" col-xs-16 col-sm-3">
                                    <?php if($usuario['foto_perfil']): ?>
                                    <img width="128" height="128" alt=""
                                         src="gestion/images/blogmanagement/perfil/<?= utf8_encode($usuario['foto_perfil']) ?>"
                                         class="img-thumbnail">
                                    <?php else:?>
                                        <img width="128" height="128" alt=""
                                             src="gestion/images/default-user.png"
                                             class="img-thumbnail">
                                    <?php endif;?>
                                </div>
                                <div class="col-xs-16 col-sm-13 blog_perfil">
                                    <h4><?= utf8_encode($usuario['name']) ?></h4>
                                    <p><?= utf8_encode($usuario['descripcion']) ?></p>
                                    <ul class="list-inline">
                                        <li><a href="<?= utf8_encode($usuario['perfil_twitter']) ?>"
                                               target="_blank"><span
                                                    class="ion-social-twitter"></span></a></li>
                                        <li><a href="<?= utf8_encode($usuario['perfil_facebook']) ?>"
                                               target="_blank"><span
                                                    class="ion-social-facebook"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-16 wow fadeInDown animated " data-wow-delay="0.5s">
                            <div class="main-title-outer pull-left">
                                <div class="main-title">Notas</div>
                            </div>
                        </div>
                        <?php foreach ($dbManager->getNotas($_REQUEST['user'], $_REQUEST['page'] ? $_REQUEST['page'] : 1, $page_break) as $nota): ?>
                            <div class="sec-topic col-sm-8 col-md-8 col-xs-16 wow fadeInDown animated author-box "
                                 data-wow-delay="0.5s"><a href="#">
                                    <img width="1000" height="606" alt=""
                                         src="gestion/images/blogmanagement/notas/big/<?= $dbManager->getNotaImagen($nota['Id'])['imagen'] ?>"
                                         class="img-thumbnail">
                                    <div class="sec-info">
                                        <div class="text-danger sub-info-bordered">
                                            <div class="pull-right"><span> <?= utf8_encode($nota['volanta']) ?> </span>
                                            </div>
                                            <div class="time"><span
                                                    class="ion-android-data icon"></span><?= $nota['fecha'] ?>
                                            </div>
                                        </div>
                                        <a href="nota-blog.php?nota=<?= $nota['Id'] ?>">
                                            <h3><?= utf8_encode($nota['titulo']) ?></h3></a>
                                    </div>
                                </a>
                                <p><?= substr((strip_tags($nota['cuerpo'])), 0, 250) ?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- left sec end -->
                <!-- Pagination Start -->
                <div class="col-sm-16">
                    <?php $total = $dbManager->getTotalPagesBlog($_REQUEST['user']) ?>
                    <?php if ($total > $page_break): ?>
                        <?php $offset = ceil($total / $page_break) ?>
                        <ul class="pagination">
                            <li class="<?= !isset($_REQUEST['page']) ? 'disabled' : '' ?>"><a
                                    href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= ($_REQUEST['page'] - 1) ?>&user=<?= $_REQUEST['user'] ?>">&laquo;</a>
                            </li>
                            <?php for ($i = 1; $i <= $offset; $i++): ?>
                                <?php
                                if ($i == $_REQUEST['page'] || (!isset($_REQUEST['page']) && $i == 1))
                                    $active = 'active';
                                else
                                    $active = '';
                                ?>
                                <li class="<?= $active ?>"><a
                                        href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= $i ?>&user=<?= $_REQUEST['user'] ?>"><?= $i ?></a>
                                </li>
                            <?php endfor; ?>
                            <li class="<?= !isset($_REQUEST['page']) || $offset == $_REQUEST['page'] ? 'disabled' : '' ?>">
                                <a href="<?= $_SERVER['PHP_SELF'] ?>?page=<?= ($_REQUEST['page'] + 1) ?>&user=<?= $_REQUEST['user'] ?>">&raquo;</a>
                            </li>
                        </ul>
                    <?php endif; ?>
                </div>
                <!-- Pagination End -->

            </div>


        </div>
        <div class="col-md-4 col-sm-5 hidden-xs right-sec">
            <?php include("common/lateral.php"); ?>
        </div>
    </div>
    <!-- left sec end -->
    <!-- redes -->
</div>
</div>
<!-- data end -->
<?php include("common/footer.php"); ?>
</div>
<!-- wrapper end -->
</body>
</html>