<?php include("common/doctype-and-head.php");

$noticias[] = $dbManager->getNoticia(30);
$noticias[] = $dbManager->getNoticia(28);
$noticias[] = $dbManager->getNoticia(27);

$rss = '
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="600">
    <title>Diarios Bonaerenses</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- bootstrap styles-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- google font -->
    <link href=\'http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800\' rel=\'stylesheet\' type=\'text/css\'>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">     <!-- ionicons font -->
    <link href="https://fonts.googleapis.com/css?family=Neucha" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
    <!-- ionicons font -->
    <link href="css/ionicons.min.css" rel="stylesheet">
    <!-- animation styles -->
    <link rel="stylesheet" href="css/animate.css" />
    <!-- custom styles -->
    <link href="css/custom-blue.css" rel="stylesheet" id="style">
    <link href="css/custom-notes.css" rel="stylesheet" id="style">
    <!-- owl carousel styles-->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <!-- magnific popup styles -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
';
foreach ($noticias as $noticia) {
    $rss .= '
<div class="col-xs-16 banner-outer-thumb  pull-left  wow fadeInLeft animated animated" data-wow-delay="0.9s" data-wow-offset="50" style="visibility: visible; animation-delay: 0.9s; animation-name: fadeInLeft;">
<div class="sec-topic col-sm-16 wow fadeInDown animated border animated" data-wow-delay="0.5s"
     style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
    <div class="row">
        <div class="col-sm-5">
            <img width="1000" height="606" alt="" src="' . DIR_WS_IMAGES_RSS . $dbManager->getNoticiaImagenes($noticia['Id'])[0]['imagen'] . '"
                 class="img-thumbnail">
        </div>
        <div class="col-sm-11">
            <a href="#">
                <div class="sec-info">
                    <h5>' . utf8_encode($noticia['titulo']) . ' </h5>

                </div>
            </a>
            <p>' . substr(strip_tags($noticia['cuerpo']), 0, 250) . ' </p>
            <a href="noticia.php?noticia=' . $noticia['Id'] . '">
                Leer m&aacute;s &gt; </a>
        </div>
    </div>
</div>
</div>';
}
$rss .= '
</body>
</html>';
$myfile = fopen("test.php", "w") or die("Unable to open file!");
fwrite($myfile, $rss);
fclose($myfile);
?>